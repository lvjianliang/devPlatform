package org.g4studio.common.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.g4studio.core.mvc.xstruts.action.ActionForm;
import org.g4studio.core.mvc.xstruts.action.ActionForward;
import org.g4studio.core.mvc.xstruts.action.ActionMapping;

public interface IAppBaseAction {
	
	
	
	/**
	 * 页面初始化
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */	
	public  ActionForward init(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;

	
	/**
	 * 分页查询
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward query(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	
	
	/**
	 * 非分页查询
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward queryNoPage(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	
	/**
	 * 树信息查询
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward queryForTree(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	

	/**
	 * 树节点复制
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward treeCopy(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	


	/**
	 * 树节点删除
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward treeDelete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	
	
	/**
	 * 查询CoodeValue
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward queryCodeValue(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;	
	
	
	
	/**
	 * 保存参数信息
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public ActionForward insert(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;
	
	/**
	 * 删除参数信息
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
	
	/**
	 * 扩展事件
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward extend(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
	
	/**
	 * 修改参数信息
	 * 
	 * @param
	 * @return
	 * @throws Exception 
	 */
	public  ActionForward update(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;
	
	/**
	 * 批处理保存数据
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public  ActionForward batchModify(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;
	
	/**
	 * 数据导出
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public  ActionForward export(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;
	
	
	/**
	 * 打印
	 * @param mapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public  ActionForward print(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception ;
	
}
