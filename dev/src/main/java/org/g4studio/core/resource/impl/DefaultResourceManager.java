package org.g4studio.core.resource.impl;

import org.g4studio.core.resource.support.AbstractResourceManager;

/**
 * DefaultResourceManager
 * 
 * @author HuangYunHui|yangzhangheng
 * @since 2009-05-20
 */
public class DefaultResourceManager extends AbstractResourceManager {

	public DefaultResourceManager() {
		super();
	}

}
