package org.g4studio.core.web.report.excel;

import java.util.List;

import org.g4studio.core.metatype.Dto;

/**
 * Excel数据对象
 * 
 * @author yzh
 * @since 2015-4-12
 */
public class ExcelData {

	/**
	 * Excel参数元数据对象
	 */
	private Dto parametersDto;

	/**
	 * Excel集合元对象
	 */
	private List fieldsList;
	
	/**
	 * Excel集合元行标题集
	 */
	private List titleList;
	
	/**
	 * Excel集合元行标题对应字段集
	 */
	private List titleFieldsList;

	public List getTitleFieldsList() {
		return titleFieldsList;
	}

	public void setTitleFieldsList(List titleFieldsList) {
		this.titleFieldsList = titleFieldsList;
	}

	public List getTitleList() {
		return titleList;
	}

	public void setTitleList(List titleList) {
		this.titleList = titleList;
	}

	/**
	 * 构造函数
	 * 
	 * @param pDto
	 *            元参数对象
	 * @param pList
	 *            集合元对象
	 */
	public ExcelData(Dto pDto, List pList) {
		setParametersDto(pDto);
		setFieldsList(pList);
	}
	
	
	public Dto getParametersDto() {
		return parametersDto;
	}

	public void setParametersDto(Dto parametersDto) {
		this.parametersDto = parametersDto;
	}

	public List getFieldsList() {
		return fieldsList;
	}

	public void setFieldsList(List fieldsList) {
		this.fieldsList = fieldsList;
	}

}
