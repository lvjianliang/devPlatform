/**
 * 欢迎页面
 * 
 */
Ext.onReady(function() {
			new Ext.ux.TipWindow({
						title : '<span class=commoncss>提示</span>',
						html : '您有[0]条未读信息.',
						iconCls : 'commentsIcon'
					}).show(Ext.getBody());
		});

Ext.onReady(function() {

	Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

	var tools = [{
				id : 'maximize',
				handler : function(e, target, panel) {
				}
			}];

	var my_height1 = document.body.clientHeight - 20;
	var my_height = document.body.clientHeight - 20;
	var my_doc = '<div style="margin:10px"><a href="http://42.120.21.17:8888/g4studio/dev.zip">《DataWorld开发指南》下载</a></div>';
	var viewport = new Ext.Viewport({
		layout : 'border',
		items : [{
			xtype : 'portal',
			region : 'center',
			margins : '2 2 2 2',
			items : [{
						columnWidth : .6,
						style : 'padding:2px 0px 2px 2px',
						items : [{
									title : '信息栏',
									layout : 'fit',
									tools : tools,
									height : my_height1,
									html:'无'
								}]
					}, {
						columnWidth : .4,
						style : 'padding:2px 2px 2px 2px',
						items : [{
							title : '文档下载',
							html : my_doc
						}, {
							title : '联系方式',
							// tools : tools,
							html : '<div style=height:60px;line-height:25px class=commoncss>&nbsp;&nbsp;电子邮箱: 125116443@qq.com<br>&nbsp;&nbsp;QQ群：437480161</div>'
						},{
							title : '关于DataWorld',
							height : 100,
							html:'<div style=height:60px;line-height:25px class=commoncss><a href="http://www.dataworld.org" target="_blank">详细文档地址</a></div>'
							//contentEl:'sina_weibo'
						}, {
							title : '舍我其谁',
							// tools : tools,
							html : '<div style=height:155px;line-height:25px class=commoncss>&nbsp;&nbsp;如果DataWorld与你无缘, 那我推荐如下一些优秀的开发平台类产品. '
									+ ' <br>&nbsp;&nbsp;AppFuse：<a href="http://www.appfuse.org" target="_blank">www.appfuse.org</a>'
									+ ' <br>&nbsp;&nbsp;Primeton EOS：<a href="http://www.primeton.com/products/bap" target="_blank">www.primeton.com</a>'
									+ ' </div>'
						}]
					}]
		}]
	});
});
