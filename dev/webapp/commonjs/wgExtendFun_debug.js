/**
 * 自动生成页面逻辑
 * @param {} queryID
 * @param {} rows
 */
function autocreatepage(queryID,grid){
	var rows = grid.wgGrid().getSelectionModel().getSelections();	
	if(isEmpty(rows)){
		Ext.Msg.alert('操作提示', '请选择自动生成页面逻辑的子页面!');
		return ;
	}
	if(rows.length>1){
		Ext.Msg.alert('操作提示', '不能进行多个子页面处理!');
		return ;
	}
	var wgPageModel = new WgPageModel(queryID);    
	var wgWindow = new WgWindow('add',wgPageModel);
	 wgWindow.getWindow().show();
	 var editButton=Ext.getCmp(queryID+"editSave");
	 var editUrl=wgPageModel.wgRequestURL();
	 editButton.addListener('click',function(){ 	 	 
	      wgDataSave(wgWindow,queryID,editUrl);
	 },false);
	 var closeButton= Ext.getCmp(queryID+"editClose");
	 closeButton.addListener('click',function(){ 
	      wgWindowClose(wgWindow);
	 },false);
	 //设置默认信息
	 var defaultFields=wgPageModel.wgDefaultFields();        	  
	 for (var i =0;i<defaultFields.length;i++){
	 	var defaultField = defaultFields[i];    	     	
	 	var fieldComp= wgWindow.wgPanel().getForm().findField(defaultField.fieldName);        	     	
	 	if(defaultField.defaultType=="2"){       	     		
	 		fieldComp.setValue(grid.wgOutKeyValue()) ;     	    
	 	}else if(defaultField.defaultType=="3"){  	 			
	 		var fieldValue=rows[0].get(defaultField.defaultValue);  
	 		fieldComp.setValue(fieldValue);     
	 	}
	 	else{
	 		fieldComp.setValue(defaultField.defaultValue);     	     		
	 	}    	     	
	 }	 
	 //动态Code获取
	dynamicCode(wgWindow,wgPageModel);
}


//获取动态Code
 function dynamicCode(wgWindow,wgPageModel){
   	  //获取动态Code
	 var wgDynamicFields= wgPageModel.wgDynamicFields();
	 for (var i =0;i<wgDynamicFields.length;i++){
	 	var dynamicField = wgDynamicFields[i];
	 	var fieldRelate= wgWindow.wgPanel().getForm().findField(dynamicField.codeName);   
	 	var fieldRelateValue= fieldRelate.getValue();
	 	var fieldSelf= wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);   
	 	var fieldStore= fieldSelf.store;
	   	fieldStore.load({
					params : {	
						codeFlag : '1',
						tableCode :fieldRelateValue,
						reqCode : 'queryCodeValue'
							}							
						});
		     	
      }
  }
//编辑页面保存功能
function wgDataSave(wgWindow,queryID,editUrl){	
	var editPanel= wgWindow.getWindow().getComponent(queryID+'editForm');
	if (!editPanel.form.isValid()) {
	   return;
    }    
    //Ext.Msg.alert("操作提示1",editUrl);
    editUrl=editUrl+	"?reqCode=insert&queryID="+queryID;
     // Ext.Msg.alert("操作提示2",editUrl);
    editPanel.form.submit({
			url : editUrl,
			waitTitle : '保存提示',
			method : 'POST',
			waitMsg : '正在处理数据,请稍候...',
			success : function(form, action) {				
				   wgWindowClose(wgWindow);				
				
			},
			failure : function(form, action) {
				var msg = action.result.msg;
				Ext.MessageBox.alert('提示', msg);
			}
		});
}
 /**
  * 关闭功能
  */
function wgWindowClose(wgWindow){ 	
	wgWindow.getWindow().close();
}

/**
 * 编辑定阅规则
 * @param {} queryID
 * @param {} grid
 */
function subRuler(queryID,grid){
	var rows = grid.wgGrid().getSelectionModel().getSelections();	
	if(isEmpty(rows)){
		Ext.Msg.alert('操作提示', '请选择要编辑的订阅规则!');
		return ;
	}
	if(rows.length>1){
		Ext.Msg.alert('操作提示', '不能进行多个数据集的订阅数据集处理!');
		return ;
	}
	var condiFlag = rows[0].get("conditionflag");
	if(condiFlag=="0"){
		Ext.Msg.alert('操作提示', '非条件规则不能进行订阅规则定制!');
		return;
	}
	var subViewCode = rows[0].get("subviewcode");
	var wgPageModel = new WgPageModel(queryID);    
	var wgWindow = new WgGridWindow(wgPageModel);
    wgWindow.getWindow().show();
    var grid2= wgWindow.wgGrid();
    grid2.wgOutQueryKeyValue(subViewCode);
    grid2.refresh();
    
}


/**
 * 共享条件规则编辑
 * @param {} queryID
 * @param {} grid
 */
function shareDataCondition(queryID,grid){	
	var shareDataCode = grid.wgOutKeyValue();
	var wgPageModel = new WgPageModel(queryID);    
	var wgWindow = new WgGridWindow(wgPageModel);
    wgWindow.getWindow().show();
    var grid2 = wgWindow.wgGrid();
    grid2.wgOutQueryKeyValue(shareDataCode);
    grid2.refresh();
    
}
/**
 * 下载标准
 * @param {} queryID
 * @param {} grid
 */
function dLStandard(queryID,grid){		
    var downloadAdrr="http://"+window.location.host+"/datacenter/download/standard.zip";
    window.location=downloadAdrr;
    //var wgPageModel = new WgPageModel(queryID);   
    
}

/**
 * 代码匹配
 * @param {} queryID
 * @param {} grid
 */
function codeMatch(queryID,grid){
	var rows = grid.wgGrid().getSelectionModel().getSelections();	
	if(isEmpty(rows)){
		Ext.Msg.alert('操作提示', '请选中要代码匹配的数据元!');
		return ;
	}
	if(rows.length>1){
		Ext.Msg.alert('操作提示', '不能进行多个数据元代码匹配!');
		return ;
	}
	
	var codeConvertFlag = rows[0].get("codeconvertflag");
	if(codeConvertFlag!='1'){
		Ext.Msg.alert('操作提示', '非代码匹配的数据元，不需要进行代码匹配!');
		return;
	}	
	var outKey= grid.wgGetPageModel().wgOutQueryField();
	var innerKey= grid.wgGetPageModel().wgInnerKeyField();
	var outKeyValue = rows[0].get(outKey);
	outKeyValue= outKeyValue+'|'+rows[0].get(innerKey);
	var wgPageModel = new WgPageModel(queryID);    
	var wgWindow = new WgGridWindow(wgPageModel);
    wgWindow.getWindow().show();
    var grid2 = wgWindow.wgGrid();
    grid2.wgOutQueryKeyValue(outKeyValue);
    grid2.refresh();    
}


