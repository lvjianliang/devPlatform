    
    function WgCodeTable(url,tableCode,queryID,dependOtherField){
    	var codeTableStore;
    	this.getCode=function(){
	    	if(!isEmpty(codeTableStore)){
	    	  return codeTableStore;
	    	}	 
	      	codeTableStore = new Ext.data.ArrayStore({
                 url:url,
			        fields:['valueCode','valueName']
			    });	
			//不依赖别的字段的代码表
			if(dependOtherField=='2'){
				codeTableStore.load({
							params : {	
								codeFlag : '1',
								tableCode :tableCode,
								reqCode : 'queryCodeValue'
							}
						});
			}else{
			//动态代码表直接取数的
				if(tableCode!="dynamic"){
					codeTableStore.load({
								params : {
									queryID  :queryID,
									codeFlag : '2',
									tableCode :tableCode,
									dependOtherField:dependOtherField,
									reqCode : 'queryCodeValue'
								}
							});
				}
			}
			return codeTableStore;
    	}; 
    	
        var gridCode;
        this.gridCodeValue=function(queryID,dependOtherField){
        	if(!isEmpty(gridCode)){
	    	  return gridCode;
	    	}	
	    	gridCode= new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								url : url
							}),
					reader : new Ext.data.JsonReader({
								totalProperty : 'TOTALCOUNT',
								root : 'ROOT'
							}, [{
									name : 'valueCode'
								}, {
									name : 'valueName'
								}, {
									name : 'pym'
								}, {
									name : 'memo'
								}])
				});  
		   if(dependOtherField=='2'){
				gridCode.load({
							params : {						
								codeFlag : '2',
								tableCode :tableCode,
								reqCode : 'queryCodeValue'
							}
						});
		   }else{
			   if(tableCode!="dynamic"){
					gridCode.load({
								params : {	
									codeFlag : '2',
									tableCode :tableCode,
									dependOtherField:dependOtherField,
									reqCode : 'queryCodeValue'
								}
							});
				}
		   }
		   return gridCode;
        }; 
    }
    /***
     * 页面信息模型所需要信息
     * @param {} queryID
     */
    function WgPageModel(queryID){
    	this.wgQueryID =queryID;
    	var jsonQuery;
    	//表单信息
    	var queryResult;        	
    	this.wgQueryResult =function(){    		
	    	if(isEmpty(queryResult)){	    		
		    	//var jsonQuery =JSON.parse(tablesField);  		
	    		jsonQuery = Ext.util.JSON.decode(tablesField);
				var queryResultStr = "var queryResultTemp=jsonQuery."+this.wgQueryID;		
				eval(queryResultStr);
				queryResult=queryResultTemp;			
			    return queryResult;
	    	}else{	    		
	    		return queryResult;
	    	}
	    };	  
    	//得到服务端RUL
	    var requestURL;
    	this.wgRequestURL=function(){	  
		  if(isEmpty(requestURL)){
		  	  requestURL=this.wgQueryResult().URL;		  	  
	     	  return requestURL;
		  }else{
			  return requestURL;
		  }
	   };
	   //得到默认查询信息
	    var queryInfo;
    	this.wgQueryInfo=function(){		  
		  if(isEmpty(queryInfo)){
    	  	  queryInfo=this.wgQueryResult().queryInfo;
	     	  return queryInfo;
		  }else{
			  return queryInfo;
		  }
	   };
	   //选择框是否显示
	    var checkFlag;
    	this.wgCheckFlag=function(){		  
		  if(isEmpty(checkFlag)){
		  	  //alert(Ext.util.JSON.encode(this.wgQueryResult()));
		  	  checkFlag=this.wgQueryResult().checkFlag;
	     	  return checkFlag;
		  }else{
			  return checkFlag;
		  }
	   };
	   
	   //表格编辑标志
	   var editFlag;
	   this.wgEditFlag=function(){		  
		  if(isEmpty(editFlag)){
		  	  //alert(Ext.util.JSON.encode(this.wgQueryResult()));
		  	  editFlag=this.wgQueryResult().editFlag;
	     	  return editFlag;
		  }else{
			  return editFlag;
		  }
	   };
	   
	     //编辑标志，主要是应对双击事件是否触发后台操作
	   var dblTrigger;
	   this.wgDblTrigger=function(){		   	  
		  if(isEmpty(dblTrigger)){
		  	  //alert(Ext.util.JSON.encode(this.wgQueryResult()));
		  	  dblTrigger=this.wgQueryResult().dblTrigger;
	     	  return dblTrigger;
		  }else{
			  return dblTrigger;
		  }
	   };
	   
	   //表格是否展开
	    var expanderFlag;
    	this.wgExpanderFlag =  function(){	   	 
		  if(isEmpty(expanderFlag)){
		  	expanderFlag=this.wgQueryResult().expanderFlag;
	         return expanderFlag;
		  }else{
			 return expanderFlag;
		  }
	   };
	   //表格自动扩展列
	    var expandCol;
    	this.wgExpandCol =  function(){	   	  
		  if(isEmpty(expandCol)){
		  	this.wgQueryResult();
		  	expandCol=queryResult.expandCol;
	         return expandCol;
		  }else{
			 return expandCol;
		  }
	   };
	   //编辑时一行几列
	   var rowFields;
	   this.wgRowFields = function(){
	   	  if(isEmpty(rowFields)){
	   	  	 rowFields=this.wgQueryResult().rowFields;
	   	  	 return rowFields;
	   	  }else{
	   	  	return rowFields;
	   	  }
	   };
	   //编辑窗口高度
	   var pageWidth;
	   this.wgPageWidth = function(){
	   	   if(isEmpty(pageWidth)){
	   	   	   pageWidth= this.wgQueryResult().pageWidth;
	   	   	   return pageWidth;
	   	   }else{
	   	   	 return pageWidth;
	   	   }
	   };
	   //编辑窗口宽度
	   var pageHeigth;
	   this.wgPageHeight = function(){
	   	  if(isEmpty(pageHeigth)){
	   	  	 pageHeigth = this.wgQueryResult().pageHeight;
	   	  	 return pageHeigth;
	   	  }else{
	   	  	return pageHeigth;
	   	  }
	   };
	   //功能名称描述
	   var fuctionDesc;
	   this.wgFunctionDesc = function(){
	   	  if(isEmpty(fuctionDesc)){
	   	  	 fuctionDesc = this.wgQueryResult().functionDesc;
	   	  	 if(isEmpty(fuctionDesc)){
	   	  	 	fuctionDesc=this.wgQueryResult().queryDesc;
	   	  	 }
	   	  	 return fuctionDesc;
	   	  }else{
	   	  	return fuctionDesc;
	   	  }
	   };
	   
	   //功能类型
	   var queryType;
	   this.wgQueryType = function(){
	   	  if(isEmpty(queryType)){   	  	
	   		 queryType=this.wgQueryResult().queryType;	   	  	
	   	  	 return queryType;
	   	  }else{
	   	  	return queryType;
	   	  }
	   };
	   
	   //功能类型
	   var mainQueryFlag;
	   this.wgMainQueryFlag = function(){
	   	  if(isEmpty(mainQueryFlag)){   	  	
	   		mainQueryFlag=this.wgQueryResult().mainQueryFlag;	   	  	
	   	  	 return mainQueryFlag;
	   	  }else{
	   	  	return mainQueryFlag;
	   	  }
	   };
	   
       //查询是否分页
	   var pageFlag;
	   this.wgPageFlag = function(){
	   	  if(isEmpty(pageFlag)){
	   	  	 pageFlag = this.wgQueryResult().pageFlag;
	   	  	 return pageFlag;
	   	  }else{
	   	  	return pageFlag;
	   	  }
	   };
	   //增加外部查询Field
	   var outQueryField;
	   this.wgOutQueryField=function(){
	   	 if(isEmpty(outQueryField)){
	   	  	 outQueryField = this.wgQueryResult().outQueryField;
	   	  	 return outQueryField;
	   	  }else{
	   	  	return outQueryField;
	   	  }
	   };
	   //增加外部查询Field
	   var innerKeyField;
	   this.wgInnerKeyField=function(){
	   	 if(isEmpty(innerKeyField)){
	   	  	 innerKeyField = this.wgQueryResult().innerKeyField;
	   	  	 return innerKeyField;
	   	  }else{
	   	  	return innerKeyField;
	   	  }
	   };
	   //页面操作标志
	   var pageOperFlag= [];
	   this.wgPageOperFlag=function(){
	   	 if(isEmpty(pageOperFlag)){
	   	  	 var operFlag = this.wgQueryResult().pageOperFlag;
	   	  	   pageOperFlag.push(operFlag.substr(0,1)); //新增
	   	  	   pageOperFlag.push(operFlag.substr(1,1)); //修改
	   	  	   pageOperFlag.push(operFlag.substr(2,1)); //删除
	   	  	   pageOperFlag.push(operFlag.substr(3,1)); //复制
	   	  	   pageOperFlag.push(operFlag.substr(4,1)); //搜索
	   	  	   pageOperFlag.push(operFlag.substr(5,1)); //刷新
	   	  	   pageOperFlag.push(operFlag.substr(6,1)); //导出
	   	  	   pageOperFlag.push(operFlag.substr(7,1)); //打印
	   	  	   pageOperFlag.push(operFlag.substr(8,1)); //内部扩展功能
	   	  	   pageOperFlag.push(operFlag.substr(9,1)); //外部扩展功能
	   	  	 return pageOperFlag;
	   	  }else{
	   	  	return pageOperFlag;
	   	  }
	 
	   };
	   
	    //内部扩展功能名称
	   var extendName;
	   this.wgExtendName = function(){
	   	  if(isEmpty(extendName)){
	   	  	 extendName = this.wgQueryResult().extendName;
	   	  	 return extendName;
	   	  }else{
	   	  	return extendName;
	   	  }
	   };
	    //外部扩展功能名称
	   var extendName2;
	   this.wgExtendName2 = function(){
	   	  if(isEmpty(extendName2)){
	   	  	 extendName2 = this.wgQueryResult().extendName2;
	   	  	 return extendName2;
	   	  }else{
	   	  	return extendName2;
	   	  }
	   };
	   //外部扩展调用函数
	   var extendFun;
	   this.wgExtendFun = function(){
	   	  if(isEmpty(extendFun)){
	   	  	 extendFun = this.wgQueryResult().extendFun;
	   	  	 return extendFun;
	   	  }else{
	   	  	return extendFun;
	   	  }
	   };
	   
	   //外部扩展用到的QueryID
	   var extendQueryID;
	   this.wgExtendQueryID = function(){
	   	  if(isEmpty(extendQueryID)){
	   	  	 extendQueryID = this.wgQueryResult().extendQueryID;
	   	  	 return extendQueryID;
	   	  }else{
	   	  	return extendQueryID;
	   	  }
	   };
	    //得到复制页面操作模式
	    var copyPattern;
	    this.wgCopyPattern =function(){
	    	if(isEmpty(copyPattern)){	    	 
	    	  	copyPattern =this.wgQueryResult().copyPattern;	    	
		    	return copyPattern;
	    	}else{
	    		return copyPattern;
	    	}
	    };
	    //得到某个查询表单根目录
	    var queryRoot;
    	this.wgQueryRoot  =  function(){    	   	    	
	    	if(isEmpty(queryRoot)){
	    	    var queryResultTemp=this.wgQueryResult();	    	 
	    	  	queryRoot =queryResultTemp.queryFields;	    	
		    	return queryRoot;
	    	}else{
	    		return queryRoot;
	    	}
	    };
	      
	   
	    //得到表格头信息
	    var columns=[];
       	this.wgColumns= function(){	  
       		if(!isEmpty(columns)){
       			return columns;
       		}
	    	var fields=this.wgQueryRoot();
	    	var url=this.wgRequestURL();
		    //作一个提示
	       // alert(JSON.stringify(fields));
	      	for(var i=0;i<fields.length;i++){
				var field=fields[i];				
				var column ={};
				column.id =field.fieldName;
				column.dataIndex = field.fieldName;
				column.header = field.fieldDisp; 				
				column.width  = field.dispWidth;
				column.sortable=field.sortable;
				column.fixed  =field.fixed;
				column.menuDisabled   =field.menuDisabled;
				column.hidden =field.visibleFlag?false:true;
				column.tooltip=field.tooltip;	
				if(this.wgEditFlag()){
					//今后可能需要扩充
					if(field.editFlag){
					   //column.editor=new Ext.form.TextField({allowBlank: field.allowBlank});
						column.editor=getEditor(url,field);
					}
				}
				if(!isEmpty(field.renderer)){						
					column.renderer=codeToValue;
				}
				columns.push(column);
			}	
			return columns;
	    };
	    
	    function getEditor(url,field){
	    	var fieldObj = {};
	    	//文本框处理
	  		if(isEmpty(field.codeFlag)||field.codeFlag=="0"){				
				if(field.fieldType=="NUMBER"){
					fieldObj.allowBlank=field.allowBlank;
					if(isEmpty(field.fieldPrecision)||field.fieldPrecision===0){
				   	   fieldObj.allowDecimals=false;
				    }else{
				       fieldObj.allowDecimals=true;
				       fieldObj.decimalPrecision=wgFieldType.fieldPrecision;
				    }
		            return new Ext.form.NumberField(fieldObj);
				}
				else{
					fieldObj.allowBlank=field.allowBlank;
				    fieldObj.maxLength= field.fieldLength;								    
				    if(!isEmpty(field.regex)){	
				    	var regex;
				    	eval('regex=/'+field.regex+'/');
				    	fieldObj.regex= regex;							       
					    fieldObj.regexText =field.regexText;		
				    }
				    if(!isEmpty(field.maskre)){
				    	var maskre;
				    	eval('var maskre=field.maskRe');
					    fieldObj.maskRe=maskre;	
				    }
				    return new Ext.form.TextField(fieldObj);
				}			
			}else {
				fieldObj.allowBlank=field.allowBlank;
			    fieldObj.maxLength= field.fieldLength;	
			    return new Ext.form.TextField(fieldObj);   				
			}
	    }
	    
	    /**
	     * 代码转换
	     * @param {} value
	     * @param {} cellmeta
	     * @param {} record
	     * @param {} rowIndex
	     * @param {} columnIndex
	     * @param {} store
	     * @return {}
	     */
	    function codeToValue(value, cellmeta, record, rowIndex, columnIndex, store){
			var fields=queryRoot;	
			if(isEmpty(columnIndex)){
				return value;
			}		
			var field;
			if(checkFlag){			 
			   field=fields[columnIndex-2];	    	 
	    	}else{
	    	   field=fields[columnIndex-1];		    	 
	    	}	   
	    	var codeTable=field.codeName;	   
			var codeTables=queryResult.codeTables;		
			var codeValues;
			var i=0;
			for(i=0;i<codeTables.length;i++){				
				var codeTableObj=codeTables[i];
				if(codeTableObj.codeTableCode==codeTable){
				  codeValues=codeTableObj.codeValus;
				}
			}		
			for(i=0;i<codeValues.length;i++){
				var codeValueObj=codeValues[i];
				if(codeValueObj.valueCode==value){
					return codeValueObj.valueName;
				}				
			}	    	
	    	return value;
	    }

	    
	    //得到默认字段	
	    var defaultFields=[];
       	this.wgDefaultFields= function(){	  
       		if(!isEmpty(defaultFields)){
       			return defaultFields;
       		}
	    	var fields=this.wgQueryRoot();
		   	for(var i=0;i<fields.length;i++){
				var field=fields[i];				
				if(isEmpty(field.defaultType)||field.defaultType=="0"){
					continue;
				}
				var defaultField ={};	
				defaultField.fieldName=field.fieldName;
				if(this.wgOutQueryField()==field.fieldName){
				  defaultField.outQueryField="1";
				}else{
				  defaultField.outQueryField="0";	
				}				
				defaultField.defaultType =field.defaultType;				
				defaultField.tableCode = field.codeName;
				defaultField.codeFlag=field.codeFlag;
				defaultField.defaultValue = field.defaultValue;					 
				if(defaultField.defaultType=="1"){
				   if(defaultField.defaultValue=="opercode"){				   	 
				   	  defaultField.defaultValue=parent.userid;				   	 
				   }else if(defaultField.defaultValue=="deptcode"){
				   	  defaultField.defaultValue=parent.deptid;
				   }else if(defaultField.defaultValue=="sysdate"){				   	 
				   	  var curDate=new Date();					   	   
                      var dateStr=curDate.getFullYear()+"-"+(curDate.getMonth()+1)+"-"+curDate.getDate()+" "+curDate.getHours()+":"+ curDate.getMinutes() +":"+ curDate.getSeconds();
    			   	  defaultField.defaultValue=dateStr;
				   }
				}
				defaultFields.push(defaultField);
			}	
			return defaultFields;
	    };	    
	    
	    //得到查询条件字段
	    var conditionFields=[];
       	this.wgConditionFields= function(){	  
       		if(!isEmpty(conditionFields)){
       			return conditionFields;
       		}
       		var queryResultTemp=this.wgQueryResult();	       	
       		conditionFields=queryResultTemp.conditionFields;	 	   
			return conditionFields;
	    };	    
	    
	     //得到所有动态Code	
	    var dynamicFields=[];
       	this.wgDynamicFields= function(){	  
       		if(!isEmpty(dynamicFields)){
       			return dynamicFields;
       		}
	    	var fields=this.wgQueryRoot();
		   	for(var i=0;i<fields.length;i++){
				var field=fields[i];				
				if(isEmpty(field.codeFlag)||field.codeFlag!="4"){
					continue;
				}
				//排除掉直接取数的
				if(field.codeName==field.fieldName){
					continue;
				}
				var dynamicField ={};	
				dynamicField.fieldName=field.fieldName;			
				dynamicField.codeName= field.codeName;
				dynamicFields.push(dynamicField);
			}	
			return dynamicFields;
	    };	  
	      
	    //得到所有Code	
	    var codeFields=[];
       	this.wgCodeFields= function(){	  
       		if(!isEmpty(codeFields)){
       			return codeFields;
       		}
	    	var fields=this.wgQueryRoot();
		   	for(var i=0;i<fields.length;i++){
				var field=fields[i];				
				if(isEmpty(field.codeFlag)||field.codeFlag=='0'){
					continue;
				}				
				var codeField ={};	
				codeField.codeFlag =field.codeFlag;
				codeField.fieldName=field.fieldName;			
				codeField.codeName= field.codeName;
				codeField.allowBlank=field.allowBlank;
				codeFields.push(codeField);
			}	
			return codeFields;
	    };	  
	  
	  	//生成StoreRecord结构
	    var storeRecord=[];
	    this.wgStoreRecord =  function(){	  
    		if(!isEmpty(storeRecord)){
    			return storeRecord;
    		}
	    	var fields=this.wgQueryRoot();
		    //作一个提示
	       // alert(JSON.stringify(fields));	       
			for(var i=0;i<fields.length;i++){
				var field=fields[i];
				var storeField ={};
			    storeField.name=field.fieldName;
			    storeField.mapping= field.fieldName;
				storeRecord.push(storeField);
			}
			return storeRecord;
	    };
	    //生成Store信息
	    var store;
    	this.wgStore =  function(){	  
    		if(!isEmpty(store)){
    			return store;
    		}	    	
	        var wgStoreRecord =this.wgStoreRecord();			
			var url=this.wgRequestURL();		
			var pageFlag=this.wgPageFlag();			
			if(pageFlag){
			  url=url+"?reqCode=query&queryID="+this.wgQueryID;
			}else{
			  url=url+"?reqCode=queryNoPage&queryID="+this.wgQueryID;
			}					
			store=  new Ext.data.Store({
					proxy : new Ext.data.HttpProxy({
								url : url
							}),
					reader : new Ext.data.JsonReader({
								totalProperty : 'TOTALCOUNT',
								root : 'ROOT'
							}, wgStoreRecord)
				});  
			return store;
	    };

	    //创建一个FormPanel items
	    var panelItems=[];
	    this.wgPanelItems=function(){
	       var me=this;	   
	       if(!isEmpty(panelItems)){
       			return panelItems;
       		}    
       		var beginFlag=true;
       		var items=[];
	    	var fields=me.wgQueryRoot();
		    //作一个提示
	       // alert(JSON.stringify(fields));	
	    	var add=0;
	       	var step=0;
	       	var col1Items=[];
	       	var col2Items=[];
	      	for(var i=0;i<fields.length;i++){
				var field=fields[i];
				var wgFieldType ={};
				//先判断是否可见，不可见设置为Hidden类型	
				//wgFieldType.id=field.fieldName; 
		    	wgFieldType.name = field.fieldName; 
				if(!field.visibleFlag){		
					wgFieldType.id=field.fieldName; 
					wgFieldType.xtype="hidden";
					if(me.wgRowFields()==1){
						items.push(wgFieldType);
					}else{						
						if (add<=0){
							col1Items.push(wgFieldType);						
						}else{
							col2Items.push(wgFieldType);			
						}
					}
					continue;					
				}				
				//设置显示标签的名称
				wgFieldType.fieldLabel = field.fieldDisp;
				wgFieldType.labelWidth = me.wgPageWidth()/me.wgRowFields()/4/me.wgRowFields();				
				wgFieldType.readOnly =field.editFlag?false:true;
				wgFieldType.allowBlank= field.allowBlank;
				wgFieldType.labelAlign =field.align ;	
				wgFieldType.tabIndex =field.fieldOrder;
				
				//文本框处理
				var maskre;
				if(isEmpty(fields[i].codeFlag)||fields[i].codeFlag=="0"){					
					if(field.multi==1){	
						if(field.fieldType=="NUMBER"){
							wgFieldType.id=field.fieldName; 						
						    wgFieldType.xtype ="numberfield";
						    wgFieldType.anchor='95%';
						    if(isEmpty(field.fieldPrecision)||field.fieldPrecision===0){
						   	   wgFieldType.allowDecimals=false;
						    }else{
						       wgFieldType.allowDecimals=true;
						       wgFieldType.decimalPrecision=field.fieldPrecision;
						    }
						    if(!isEmpty(field.regex)){
							    wgFieldType.regex= new RegExp(field.regex);
							    wgFieldType.regexText =field.regexText;		
						    }
						    if(!isEmpty(field.maskre)){
							    maskre=new RegExp(field.maskRe);					
							    wgFieldType.maskRe=maskre;	
						    }
						}
						else{
							wgFieldType.id=field.fieldName; 
							//wgFieldType.name = field.fieldName; 
						    wgFieldType.xtype ="textfield";
						    wgFieldType.maxLength= field.fieldLength;
						    wgFieldType.anchor='95%';							    
						    if(!isEmpty(field.regex)){	
						    	var regex;
						    	eval('regex=/'+field.regex+'/');
						    	wgFieldType.regex= regex;							       
							    wgFieldType.regexText =field.regexText;		
						    }
						    if(!isEmpty(field.maskre)){						    	
						    	eval('var maskre=field.maskRe');
							    wgFieldType.maskRe=maskre;	
						    }
						}
					}else{						
						wgFieldType.xtype ="textarea";
						wgFieldType.maxLength= field.fieldLength;
						//wgFieldType.rows=field.multi;
						wgFieldType.anchor='95%';				    	
					}
				}else{
				  //选择框处理	
					//wgFieldType.name = field.fieldName;
					wgFieldType.hiddenName = field.fieldName;
					wgFieldType.xtype ="combo";
					wgFieldType.anchor="95%";					
					wgFieldType.emptyText='请选择....' ;
					wgFieldType.valueField="valueCode";
				    wgFieldType.displayField='valueName';					    
					var url=me.wgRequestURL();
					var codeTable=field.codeName;
					var dependOtherField='0';
					if(field.codeFlag=='4'){
						if(field.codeName==field.fieldName){
							dependOtherField='0';							
						}else{
							dependOtherField='1';
							codeTable='dynamic';
						}
					}else{
						dependOtherField='2';
					}	
					var queryID= me.wgQueryID;
					var wgCodeTable =new WgCodeTable(url,codeTable,queryID,dependOtherField);
					wgFieldType.store=wgCodeTable.getCode();				    
					wgFieldType.typeAhead=true;
	            	wgFieldType.triggerAction="all";
	            	wgFieldType.lazyRender=true;
	            	//wgFieldType.matchFieldWidth=true;
					wgFieldType.mode ="local";
					wgFieldType.editable = false;
					//wgFieldType.renderTo=Ext.getBody();				
				}
				
				if(field.multi<=3){				
					if(me.wgRowFields()==1){
						items.push(wgFieldType);
					}else{
						if (add<=0){
							col1Items.push(wgFieldType);						
						}else{
							col2Items.push(wgFieldType);			
						}
					}
					//步长计算
					if(add<=0){
						step=field.multi;					
					}else{
						step=-field.multi;
					}
					add=add+step;
				}else{
					if(beginFlag){					 
				      if(me.wgRowFields()==1){				
					    panelItems.push(items);	
				      }else{
					  	  var twoItems=[];					  	 
						  var items1={};
						  items1.items=col1Items;	
						  items1.layout="form";				
						  //items1.bodyStyle='border-width:1 0 0 0; background:transparent';	
						  items1.bodyStyle = 'background-color:#FFFFFF;padding:5px';//设置面板体的背景色  		
						  items1.frame=false;	
						  items1.border=false;
						  items1.width= me.wgPageWidth() /me.wgRowFields() -10 ;
						  var items2={};
						  items2.items=col2Items;
						  items2.layout="form";
						  //items2.bodyStyle='border-width:1 0 0 0; background:transparent';
						  items2.bodyStyle = 'background-color:#FFFFFF;padding:5px';//设置面板体的背景色  		
						  items2.frame=false;	
						  items2.border=false;
						  items2.width=me.wgPageWidth() /me.wgRowFields() -10 ;
						  twoItems.push(items1);
						  twoItems.push(items2);
					  	  panelItems.push(twoItems);
					  }
					  beginFlag=false;					
					}		
					wgFieldType.labelAlign="top";			
	    			wgFieldType.anchor="0 100%";	
	    			wgFieldType.hideLabel =true;
					panelItems.push(wgFieldType);					
				}
				
			}		
			if(panelItems.length===0){
				if(me.wgRowFields()==1){
				   panelItems.push(items);	
				}else{
					  var twoItems=[];					  	 
					  var items1={};
					  items1.items=col1Items;	
					  items1.layout="form";				
					  //items1.bodyStyle='border-width: 1 1 1 1; background:transparent';
					  items1.bodyStyle = 'background-color:#FFFFFF;padding:5px';//设置面板体的背景色  		            
					  items1.frame=false;	
					  items1.border=false;
					  items1.width=me.wgPageWidth() /me.wgRowFields() -10 ;
					  var items2={};
					  items2.items=col2Items;
					  items2.layout="form";
					  //items2.bodyStyle='border-width:1 1 1 1; background:transparent';
					  items2.bodyStyle = 'background-color:#FFFFFF;padding:5px';//设置面板体的背景色  							  
					  items2.frame=false;	
					  items2.border=false;
					  items2.width=me.wgPageWidth() /me.wgRowFields() -10 ;
					  twoItems.push(items1);
					  twoItems.push(items2);
				  	  panelItems.push(twoItems);
				}
			}
			return panelItems;
	    };
	   
	    //结束 
    }
    
    /***
     * 创建Gird上方工具条
     * @param {} wgPageModel
     */
    function WgTopBar(wgPageModel){
    	//grid上方的工具条
    	var tbar;
    	var queryID=wgPageModel.wgQueryID;
    	this.wgTbar=function(){
    		if (!isEmpty(tbar)){
    			return tbar;
    		}
    		var barComp =[];    		
    		var wgPageOperFlag=wgPageModel.wgPageOperFlag();
    		if (wgPageOperFlag[0]=="1"){
    			var compadd ={
    				        id:queryID+'add',
							text : '新增',
							iconCls : 'page_addIcon'
						};
			    
    			barComp.push(compadd);    			
    			var compAddSpit ="-";    		
    			barComp.push(compAddSpit);
    		 }
    		 if (wgPageOperFlag[1]=="1"){    		 	
    		 	if(!wgPageModel.wgEditFlag()){
    		 		var compEdit= {
    				        id:queryID+'modify',
							text : '修改',
							iconCls : 'page_edit_1Icon'
    		 		} ;  
    		 	   barComp.push(compEdit);    		
    			   var compEditSpit ="-";
    			   barComp.push(compEditSpit);    
    		 	}   					
    		 }	
    		 if (wgPageOperFlag[2]=="1"){
    			var delComp = {
    				        id:queryID+'delete',
							text : '删除',
							iconCls : 'page_delIcon'
						};
			   barComp.push(delComp);
			   var compDelSpit ="-";
			   barComp.push(compDelSpit);	
			  
    		}
    		//编辑表格，保存放到删除后面
    		if (wgPageOperFlag[1]=="1"){    		 	
    		 	if(wgPageModel.wgEditFlag()){
    		 	   	var compEdit2= {
					        id:queryID+'modify',
						text : '保存',
						iconCls : 'acceptIcon'
					};
			       barComp.push(compEdit2);    		
    			   var compEditSpit2 ="-";
    			   barComp.push(compEditSpit2);    	
    		 	}
    		}
    		//内部扩展
    		if (wgPageOperFlag[8]=="1"){
    			var extendComp = {
    				        id:queryID+'extend',
							text : wgPageModel.wgExtendName(),
							iconCls : 'plugin_addIcon'
						};
			   barComp.push(extendComp);
			   var extendCompSpit ="-";
			   barComp.push(extendCompSpit);	
			  
    		}
    		//外部扩展
    		if (wgPageOperFlag[9]=="1"){
    			var extend2Comp = {
    				        id:queryID+'extend2',
							text : wgPageModel.wgExtendName2(),
							iconCls : 'server_connectIcon'
						};
			   barComp.push(extend2Comp);
			   var extend2CompSpit ="-";
			   barComp.push(extend2CompSpit);	
			  
    		}
    		var split2 = '->';
			barComp.push(split2);		
    		if (wgPageOperFlag[4]=="1"){
	    		var queryTextComp = new Ext.form.TextField({
										id : queryID+'queryParam',
										name : queryID+'queryParam',
										emptyText : wgPageModel.wgQueryInfo(),
										enableKeyEvents : true,
										width : 130
									});
		        barComp.push(queryTextComp);
		       
		        var queryButtonComp={
		        	            id: queryID+'query',
								text : '查询',
								iconCls : 'previewIcon'
							};
				barComp.push(queryButtonComp);
				var queryButtonSpit="-";
				barComp.push(queryButtonSpit);
    		}
    		if (wgPageOperFlag[5]=="1"){
				var refreshComp ={
					            id:  queryID+'refresh',
								text : '刷新',
								iconCls : 'arrow_refreshIcon'
							};
			    barComp.push(refreshComp);  
			    var refreshButtonSpit="-";
				barComp.push(refreshButtonSpit);
    		}
    		if (wgPageOperFlag[6]=="1"){
    			var exportComp ={
					            id:  queryID+'export',
								text : '导出',
								iconCls : 'page_excelIcon'
							};
			    barComp.push(exportComp);  
			    var exportButtonSpit="-";
				barComp.push(exportButtonSpit);
    		}
    		if (wgPageOperFlag[7]=="1"){
    			var printComp ={
					            id:  queryID+'print',
								text : '打印',
								iconCls : 'printerIcon'
							};
			    barComp.push(printComp);  			  
    		}
		    tbar=barComp;				 
		    return tbar;
    		
    	};          
    }
    
    /***
     * 创建Gird上方工具条
     * @param {} wgPageModel
     */
    function WgTopBarQuery(wgPageModel){
    	//grid上方的工具条
    	var tbar;
    	var queryID=wgPageModel.wgQueryID;
    	this.wgTbar=function(){
    		if (!isEmpty(tbar)){
    			return tbar;
    		}
    		var barComp =[];  
    		var conditionFields= wgPageModel.wgConditionFields();
    		var i;
    	    for(i=0;i<conditionFields.length;i++){    		   
      	       var field=conditionFields[i];
      	       var fieldDisplay = field.fieldDisp+' ';
      	       barComp.push(fieldDisplay);
      	       var conditionComp;      
      	       if (isEmpty(field.codeFlag)||field.codeFlag=="0"){//非下拉框为
      	    	 if(field.fieldType=="DATE"||field.fieldType=="DATETIME"){
      	    		conditionComp = new Ext.form.DateField({
 						id : queryID + field.fieldName,
 						name : queryID + field.fieldName,
 						emptyText : '请输入...',
 						enableKeyEvents : true,
 						allowblank :false,
 						width : 120,
 					    format : 'Y-m-d',
 					    value: Ext.util.Format.date(new Date(),"Y-m-d")  
 					});
    	    	 }else{
    	    			conditionComp = new Ext.form.TextField({
     						id : queryID + field.fieldName,
     						name : queryID + field.fieldName,
     						emptyText : '请输入...',
     						allowblank :false,
     						enableKeyEvents : true,
     						width : 120
     					});
    	    	 }
      	       }else{
      	    	 var url=wgPageModel.wgRequestURL();
   	    	     var codeTable=field.codeName;
   	    	     var dependOtherField='0';
					if(field.codeFlag=='4'){
						if(field.codeName==field.fieldName){
							dependOtherField='0';							
						}else{
							dependOtherField='1';
							codeTable='dynamic';
						}
					}else{
						dependOtherField='2';
					}	
					var wgCodeTable =new WgCodeTable(url,codeTable,queryID,dependOtherField);
					conditionComp = new Ext.form.ComboBox({
						id : queryID + field.fieldName,
						hiddenName:field.fieldName,
						emptyText:'请输入...',
						valueField:'valueCode',
						displayField:'valueName',
						allowblank :false,
						store:wgCodeTable.getCode(),
						typeAhead:true,
						triggerAction:'all',
						lazyRender:true,
						mode:'local',
						editable:false,
						width:100
						});
      	       }
      	       barComp.push(conditionComp); 
      	     }     
    	    if(wgPageModel.wgQueryType()=="2"&&wgPageModel.wgMainQueryFlag()=="1"){
		        var queryButtonComp={
		        	            id: queryID+'query',
								text : '查询',
								iconCls : 'previewIcon'
							};
				barComp.push(queryButtonComp);
	       }
			var queryButtonSpit="-";
			barComp.push(queryButtonSpit);    
			
			var printComp ={
		            id:  queryID+'print',
					text : '打印',
					iconCls : 'printerIcon'
				};
            barComp.push(printComp);  	
          
		    tbar=barComp;				 
		    return tbar;
    		
    	};          
    }
    
    /***
     * 分页Gird对象
     */
    function WgPageSizeComb(queryID){
    	var pageSize_combo;
    	this.wgPageSize_combo=function(){
    		if(!isEmpty(pageSize_combo)){
    			return pageSize_combo;
    		}
       		pageSize_combo=  new Ext.form.ComboBox({
    			id: queryID+'combPagesize',
				name : queryID+'combPagesize',		
				typeAhead : true,
				triggerAction : 'all',		
				lazyRender : true,
				mode : 'local',
				store : new Ext.data.ArrayStore({
							fields : ['value', 'text'],
							data : [[10, '10条/页'], [20, '20条/页'],
									[50, '50条/页'], [100, '100条/页'],
									[250, '250条/页'], [500, '500条/页']]
						}),
				valueField : 'value',
				displayField : 'text',
				value : '20',
				editable : false,
				width : 85
			});	
			return pageSize_combo;
    	};
    }
    
    /**
     * 分页工具条
     */
    function WgBottonBar(wgPageModel){
    	var queryID=wgPageModel.wgQueryID;
    	var  wgPageSizeComb= new WgPageSizeComb(queryID);    	
    	var number = parseInt(wgPageSizeComb.wgPageSize_combo().getValue());
        //分页工具条
    	var bbar;
    	this.wgBbar=function(){
    		if(!isEmpty(bbar)){
    			return bbar;
    		}
    		bbar= new Ext.PagingToolbar({
    			id: queryID+'pageSizeTool',    	
    	     	pageSize : number,
				store : wgPageModel.wgStore(),
				displayInfo : true,
				displayMsg : '显示{0}条到{1}条,共{2}条',
				plugins : new Ext.ux.ProgressBarPager(), // 分页进度条
				emptyMsg : "没有符合条件的记录",
				items : ['-', '&nbsp;&nbsp;', wgPageSizeComb.wgPageSize_combo()]
			});
			return bbar;
    	};
    }
    
    /**
     * 创建一个表格选择框
     * @param {} queryID
     */
    function WgSm(wgPageModel){
    	var selectColumn;
    	this.wgSelectModel=function(){
    		if(wgPageModel.wgCheckFlag()){
    			return  new Ext.grid.CheckboxSelectionModel();      		
    		}else{
    			return null ;
    		}
    	};
    	
    }
   
    /**
     * 列头对象
     * @param {} wgPageModel
     */
    function WgCm(wgPageModel,selectColumn){       	
    	var columnModel;     	
    	this.wgColumnModel=function(){
    		if(!isEmpty(columnModel)){
    			return columnModel;
    		}
    		var columnHeader =[];
    		var columnRowID =new Ext.grid.RowNumberer();    
    		columnHeader.push(columnRowID);			
    		if(wgPageModel.wgCheckFlag()){    	
    			columnHeader.push(selectColumn);
    		}
    	    var wgColumns= wgPageModel.wgColumns();
    	    var i;
    	    for(i=0;i<wgColumns.length;i++){
    	       columnHeader.push(wgColumns[i]);
    	    }  
    	    var columnStr= JSON.stringify(columnHeader);
    		columnModel=new Ext.grid.ColumnModel(columnHeader);    		
    		return columnModel;
    	};
    	//结束
    }
    
   
    
    /**
     * 创建一个FormPanel实例对象
     */
    function WgFormPanel(wgPageModel){
    	var queryID=wgPageModel.wgQueryID;
    	var formPanel={};
    	//获得一个Panel
    	this.wgGetFormPanel=function(){
    		if(!isEmpty(formPanel)){
    		   return formPanel;
    		}
    		var panelItems=wgPageModel.wgPanelItems();    		
    
    		//不是复杂的直接返回
    		var panel={};
    		if(panelItems.length==1){	    			
	    		panel.id=queryID+'editForm';
	    		panel.name=queryID+'editForm';	    		
	    		panel.border=false;    	
	    		panel.margins = '0px 0px 0px 0px';  
	    		panel.autoScroll=true;
	    		panel.items=panelItems[0];  
	    		//panel.bodyStyle='background-color:#FFFFFF;padding:5px';
	    		if(wgPageModel.wgRowFields()>1){		
	    			panel.frame=false;//特别注意此项
	    			panel.layout={type:"hbox",align:"stretch"};
    			    //panel.defaults={xtype:"panel",flex:1};    			   
	    		}else{
    			   panel.layout="form"; 
    			   panel.frame=true;//特别注意此项
	    		}	    		 		
				formPanel=  new Ext.form.FormPanel(panel);  				
				
    		}else{   
	    		//复杂 的用accordion    		
	    		panel.id=queryID+'editForm';
	    		panel.name=queryID+'editForm';
	    		panel.layout= 'accordion';
	    		//panel.margins = '0px 0px 0px 0px'; 	
	    		var layoutConfig={};
	    		layoutConfig.activeOnTop=true;//设置打开的子面板置顶  
	    		layoutConfig.fill=true;//子面板充满父面板的剩余空间  
	    		layoutConfig.hideCollapseTool=false;////显示“展开收缩”按钮  
	    		layoutConfig.titleCollapse=true;//允许通过点击子面板的标题来展开或收缩面板  
	    		layoutConfig.animate=true;//使用动画效果  
	       		panel.layoutConfig=layoutConfig;
			    panel.frame=false;
			    panel.border=false;
			    panel.renderTo=Ext.getBody();
			    panel.defaults = {bodyStyle:'background-color:#FFFFFF;padding:5px'};//设置面板体的背景色  		            
			    var items=[];		 
			    for(var i=0;i<panelItems.length;i++){
			     	var panelTemp ={};
			     	panelTemp.frame=false;//特别注意此项
		    		panelTemp.border=false;    	
		    		panelTemp.margins = '0px 0px 0px 0px'; 	    	
		    		panelTemp.autoScroll=true;
		    		panelTemp.items=panelItems[i];  	    			 
		    		if(i===0){
		    			panelTemp.title="基本信息";
		    			if(wgPageModel.wgRowFields()>1){	    		 
			    			//panelTemp.layout={type:"hbox",align:"stretch"};	 
		    				panelTemp.layout={type:"hbox"};	    
			    		}else{
		    			   panelTemp.layout="form"; 
		    			   panelTemp.frame=false;//特别注意此项
			    		}
		    		}else{
		    			panelTemp.layout="form";   
		    			panelTemp.title=panelItems[i].fieldLabel;	
		    		}
		  		    items.push(panelTemp);
			    }
			    panel.items=items;
	    		formPanel = new Ext.form.FormPanel(panel);    	
    	  }    	 
    	  return formPanel;
    	};       	
    	//结束
    }
    
    //编辑窗口功能键
    function WgEditButton(wgPageModel){
    	var queryID=wgPageModel.wgQueryID;
    	var editButtons=[];    
        this.getButtons=function(){
        	if(!isEmpty(editButtons)){
        		return editButtons;
        	}
    		var saveButton= {};
    		saveButton.id=queryID+"editSave";
    		saveButton.text="保存";
    		saveButton.iconCls="acceptIcon";
    		editButtons.push(saveButton);
    		var closeButton= {};
    		closeButton.id =queryID+"editClose";
    		closeButton.text="关闭";
    		closeButton.iconCls="deleteIcon";
    		editButtons.push(closeButton);
    		return editButtons;
    	};
    	//结束
    }
    /**
     * 创建一个Window
     */
    function WgWindow(editFlag,wgPageModel){
    	var queryID=wgPageModel.wgQueryID;
       	//定义Panel
       	var panel;
    	this.wgPanel=function(){
    		if(!isEmpty(panel)){
    		  return panel;
    		}
    		var wgFormPanel = new WgFormPanel(wgPageModel);
    		panel=wgFormPanel.wgGetFormPanel();    
    		return panel;
    	};
    	//定义window
    	var wgwin; 
    	this.getWindow=function(){
    	   if(!isEmpty(wgwin)){
    	   	return wgwin;
    	   }
    	   var wgEditButton=new WgEditButton(wgPageModel);
    	   
    	   var winProperty={};
    	   winProperty.id=queryID+'editWind';
    	   winProperty.layout='fit';
    	   winProperty.width= wgPageModel.wgPageWidth();    
    	   winProperty.height =wgPageModel.wgPageHeight();
    	   winProperty.resizable = false;
    	   winProperty.draggable=true;
    	   winProperty.closeAction='close';
    	   if (editFlag=='add'){
    	   	 winProperty.title="【"+wgPageModel.wgFunctionDesc()+":新增】";
    	   }else{
    	   	 winProperty.title="【"+wgPageModel.wgFunctionDesc()+":修改】";
    	   }
    	   winProperty.modal = true;
    	   winProperty.collapsible = true;
    	   winProperty.titleCollapse=true;
    	   winProperty.maximizable=false;
    	   winProperty.buttonAlign='right';
    	   winProperty.border=false;
    	   winProperty.animCollapse=true;
    	   winProperty.animateTarget=Ext.getBody();
    	   winProperty.constrain=true;	 
    	   winProperty.items=[this.wgPanel()];
    	   winProperty.margins = '10px 0px 0px 0px';
       	   winProperty.buttons=wgEditButton.getButtons();
    	   wgwin = new Ext.Window(winProperty);
    	   wgwin.addListener('keypress',function(o,e,eOpts){   
    		    keyEnterPress();
    		 	},false);
    	   return wgwin;
    	};
    	var keyEnterPress=function(o,e){
    		if(e.getKey()==13){
    			e.keyCode=9;
    			//scope:this;
    		}
    	};
    	//End
    }
    
    /**
     * 创建一个Grid Window
     */
    function WgGridWindow(wgPageModel){
    	var queryID=wgPageModel.wgQueryID;
       	//定义wg Grid
       	var  grid;
       	this.wgGrid=function(){
       	   if(!isEmpty(grid)){
       	   	 return grid;
       	   }
       	   grid= new WgGrid(queryID);
       	   return grid;
       	};
    	//定义window
    	var wgwin; 
    	this.getWindow=function(){
    	   if(!isEmpty(wgwin)){
    	   	return wgwin;
    	   }
    	   var wgEditButton=new WgEditButton(wgPageModel);
    	   
    	   var winProperty={};
    	   winProperty.id=queryID+'editWind';
    	   winProperty.layout='fit';
    	   winProperty.width= wgPageModel.wgPageWidth();    
    	   winProperty.height =wgPageModel.wgPageHeight();
    	   winProperty.resizable = false;
    	   winProperty.draggable=true;
    	   winProperty.closeAction='close';    	 
    	   winProperty.title="【"+wgPageModel.wgFunctionDesc()+"】";    	
    	   winProperty.modal = true;
    	   winProperty.collapsible = true;
    	   winProperty.titleCollapse=true;
    	   winProperty.maximizable=false;
    	   winProperty.buttonAlign='right';
    	   winProperty.border=false;
    	   winProperty.animCollapse=true;
    	   winProperty.animateTarget=Ext.getBody();
    	   winProperty.constrain=true;	 
    	   winProperty.items=[this.wgGrid().wgCreateGrid()];
    	   winProperty.margins = '10px 0px 0px 0px';
       	   //winProperty.buttons=wgEditButton.getButtons();
    	   wgwin = new Ext.Window(winProperty);
    	   wgwin.addListener('keypress',function(o,e,eOpts){   
    		    keyEnterPress();
    		 	},false);
    	   return wgwin;
    	};
    	var keyEnterPress=function(o,e){
    		if(e.getKey()==13){
    			e.keyCode=9;
    			//scope:this;
    		}
    	};
    	//End
    }
    
    /***
     * 表格对象
     * @param {} queryID
     */
    function WgGrid(queryID){
     	//创建页面信息模型
    	var me=this;    	
    	var wgPageModel = new WgPageModel(queryID);    
    	wgPageModel.wgPanelItems();
    	wgPageModel.wgColumns();
    	wgPageModel.wgCheckFlag();
    	wgPageModel.wgQueryRoot();
    	//创建一个SelectModel
    	var wgSm= new WgSm(wgPageModel);
    	var sm= wgSm.wgSelectModel();
      	//创建一个ColumnModel    
    	var wgCm = new WgCm(wgPageModel,sm);
    	var cm = wgCm.wgColumnModel();
    	//创建一个TopBar
    	var wgTopBar;
    	if(wgPageModel.wgQueryType()=="1"){
    		wgTopBar = new WgTopBar(wgPageModel);   
    	}else{
    	   wgTopBar = new WgTopBarQuery(wgPageModel);  
    	}
    	var tbBar= wgTopBar.wgTbar();
    	//创建一个分页工具条
    	var wgBottonBar = new WgBottonBar(wgPageModel);
    	var bbBar = wgBottonBar.wgBbar();
    	//定义当前操作类型
    	var operType="query";    
    	//编辑窗口
    	var wgWindow;
    	//联动标志
    	var interLocking ="0";//不联动变化
    	this.wgInterLocking=function(flag){
    		interLocking=flag;
    		return interLocking;
    	};
    	//页面模型属性
    	this.wgGetPageModel=function(){
    	  return wgPageModel;
    	};
    	//外部驱动查询条件
    	var outQueryKeyValue="";    	
    	this.wgOutQueryKeyValue=function(fieldValue){  
    	   var queryField=wgPageModel.wgOutQueryField();   
		   var url=wgPageModel.wgRequestURL()+"?reqCode=query&queryID="+wgPageModel.wgQueryID+"&"+queryField+"="+fieldValue;
		   wgPageModel.wgStore().proxy = new Ext.data.HttpProxy({url :url});			  
		   outQueryKeyValue=fieldValue;		
		   return outQueryKeyValue;
    	};
    	this.wgOutKeyValue=function(){return outQueryKeyValue;};
    	//定义两个外部关联的Grid
    	var centerGrid;
    	this.wgCenterGrid=function(grid){
    	  centerGrid=grid;
    	  return centerGrid;
    	};
    	var eastGrid;
    	this.wgEastGrid =function(grid){
    	  eastGrid=grid;
    	  return eastGrid;
    	};
    	//Grid是否有标题
    	this.titleFlag=false;
    	//DataStore删除 
    	var deletedRecord=[];
    
      	//创建Grid
    	var grid ;    
    	//设置新增功能可用
    	var operDisableFlag=false;
    	this.setButtonDisabled=function(flag){
    		var wgPageOperFlag=wgPageModel.wgPageOperFlag();
    		//查询事件
    		/*if (wgPageOperFlag[4]=="1"){
	    		var queryButton=Ext.getCmp(queryID+'query');	
	    		queryButton.setDisabled(flag);
	    	    var queryParamInput = Ext.getCmp(queryID+"queryParam");	   
	    	    queryParamInput.setDisabled(flag);
    		}
    		
    	     //刷新事件
    		if (wgPageOperFlag[5]=="1"){
	    	     var refreshButton=Ext.getCmp(queryID+'refresh');
	    	     refreshButton.setDisabled(flag);
	    		
    		}*/
    	    if (wgPageOperFlag[0]=="1"){
	    	     //增加数据打开窗口功能
	    	     var addButton=Ext.getCmp(queryID+'add');
	    	     addButton.setDisabled(flag);	    		
    	    }
    	     //修改数据打开功能
    	     if (wgPageOperFlag[1]=="1"){
	    	     var modifyButton=Ext.getCmp(queryID+'modify');
	    	     modifyButton.setDisabled(flag);	    		
    	     }
    	     //删除功能
    	     if (wgPageOperFlag[2]=="1"){
	    	     var deleteButton=Ext.getCmp(queryID+'delete');
	    	     deleteButton.setDisabled(flag);	    		
    	     }  
    	     //内部扩展事件
    		if (wgPageOperFlag[8]=="1"){
	    	     var extendButton=Ext.getCmp(queryID+'extend');
	    		 extendButton.setDisabled(flag);
    		}
    		//外部扩展事件
    		if (wgPageOperFlag[9]=="1"){
	    	     var extend2Button=Ext.getCmp(queryID+'extend2');
	    		  extend2Button.setDisabled(flag);
    		}
    		operDisableFlag=flag;
    	};
    	this.wgGrid=function(){return grid;};
     	this.wgCreateGrid= function(){
    		if(!isEmpty(grid)){
    			return grid;
    		}
    		var gridInfo={};    	
    		gridInfo.height=parent.height;
    		/*if(this.titleFlag){
    			gridInfo.title=wgPageModel.wgFunctionDesc();
    		}*/
    		gridInfo.width= parent.width;
    		gridInfo.store=wgPageModel.wgStore();    
    		gridInfo.region="center";
    		gridInfo.layout="fit";    	
    		gridInfo.margins="-10 -8 -8 -8";
    		gridInfo.loadMask= {msg : '正在加载表格数据,请稍等...'};
    		gridInfo.stripeRows=true;
    		gridInfo.frame=true;
    		//如果是编辑表格，设置单击编辑属性
    		if(wgPageModel.wgEditFlag()){
    		   //如果没能 设置这个，将不能进行selection选择模式
    	       gridInfo.selModel=new Ext.grid.RowSelectionModel({singleSelect:false});
    		   gridInfo.clicksToEdit=1;
    		}
    		/*if(this.titleFlag){
	    		gridInfo.maximizable=true;    		
	    		gridInfo.collapsible=true;  //panel可收缩  
	    		gridInfo.draggable=true;
    		}    */	  		
    		gridInfo.autoExpandColumn=wgPageModel.wgExpandCol();
       		if(!isEmpty(sm)){
    			gridInfo.sm=sm;
    		}
    		gridInfo.cm=cm;    	
    		gridInfo.tbar=tbBar;      
    		if(wgPageModel.wgPageFlag()=="1"){
    	    	gridInfo.bbar=bbBar;    	
    		}
    		//给表格增加一个最大化
    		//var max=new wg.comp.MaximizeTool(gridInfo);
    		//gridInfo.plugins=max;  
    		if(wgPageModel.wgEditFlag()){
    		   grid =new Ext.grid.EditorGridPanel(gridInfo);
    		}else{
    		   grid =new Ext.grid.GridPanel(gridInfo);
     	    }    		
    		//刷新数据由外部出发
    		//wgRefresh();    
    		//增加查询监听事件
    		var wgPageOperFlag=wgPageModel.wgPageOperFlag();
    		if (wgPageOperFlag[4]=="1"){
	    		var queryButton=Ext.getCmp(queryID+'query');
	    		queryButton.addListener('click',function(){ 
	    	         wgQuery();
	    	     },false);
	    		//业务模型需发增加检索回车功能
	    		if(wgPageModel.wgQueryType()=="1"){
		    	     //增加检索事件
		    	     var queryParamInput = Ext.getCmp(queryID+"queryParam");
		    	     queryParamInput.addListener('keyup',function(b,e,eOpts){     	
		    	    	if (e.keyCode == Ext.EventObject.ENTER) {
							 wgQuery();
						}
		    	     },false  );
	    		}
    		}
    		//增加内部扩展事件
    		if (wgPageOperFlag[8]=="1"){
	    	     var extendButton=Ext.getCmp(queryID+'extend');
	    		 extendButton.addListener('click',function(){ 
	    	         wgExtend();
	    	     },false);
    		}
    		//增加外部扩展事件
    		if (wgPageOperFlag[9]=="1"){
	    	     var extend2Button=Ext.getCmp(queryID+'extend2');
	    		 extend2Button.addListener('click',function(){ 
	    	         wgExtend2();
	    	     },false);
    		}
    	     //增加刷新事件
    		if (wgPageOperFlag[5]=="1"){
	    	     var refreshButton=Ext.getCmp(queryID+'refresh');
	    		 refreshButton.addListener('click',function(){ 
	    	          wgRefresh();
	    	     },false);
    		}
    		 //增加导出事件 
    		if (wgPageOperFlag[6]=="1"){
	    	     var exportButton=Ext.getCmp(queryID+'export');
	    	     exportButton.addListener('click',function(){ 
	    	          wgExport();
	    	     },false);
    		}
    		 //增加打印事件
    		if (wgPageOperFlag[7]=="1"){
	    	     var printButton=Ext.getCmp(queryID+'print');
	    	     printButton.addListener('click',function(){ 
	    	          wgPrint();
	    	     },false);
    		}
    	    if (wgPageOperFlag[0]=="1"){
	    	     //增加数据打开窗口事件
	    	     var addButton=Ext.getCmp(queryID+'add');
	    		 addButton.addListener('click',function(){ 	    		 	
	    		 	 if(!wgPageModel.wgEditFlag()){
	    		 	   //弹出式增加
	    		       wgAddOpen();
	    		 	 }else{
	    		 	   //在表格中增加一行
	    		 	   wgGridAdd();
	    		 	 }
	    	     },false);
    	    }
    	     //修改数据打开王窗口事件
    	     if (wgPageOperFlag[1]=="1"){
	    	     var modifyButton=Ext.getCmp(queryID+'modify');
	    		 modifyButton.addListener('click',function(){
	    		 	 if(!wgPageModel.wgEditFlag()){
	    		 	 	//弹出式编辑
	    		        wgEditOpen();
	    		 	 }else{
	    		 	 	//保存功能实现
	    		 	 	wgGridEdit();
	    		 	 }
	    		 	},false);
    	     }
    	     //删除事件
    	     if (wgPageOperFlag[2]=="1"){
	    	     var deleteButton=Ext.getCmp(queryID+'delete');
	    		 deleteButton.addListener('click',function(){ 
	    		 	//不是编辑状态的删除直接提交后台
	    		 	if(!wgPageModel.wgEditFlag()){
	    	         wgDeleteData();
	    		 	}else{
	    		 		//编辑状态下只删除前台界面的内容
	    		 	 wgGridDelete();	
	    		 	}
	    	     },false);
    	     }
    	     
    	     if(wgPageModel.wgPageFlag()=="1"){
    	     	//增加页面Size Combox 选择
	    	     var pageCombSelect=Ext.getCmp(queryID+"combPagesize");    	     
	    	     pageCombSelect.on('select',function(){ 
	    	         wgPageCombSelect();
	    	     });
	    	    
	    	     //翻页增加查询参数
	             var baseParams;
	             wgPageModel.wgStore().on('beforeload', function() {    
	             	var queryParam;
	             	var queryObj = Ext.getCmp(queryID+'queryParam');
	             	if(!isEmpty(queryObj)){
	             		queryParam=queryObj.getValue();
	             	}else{
	             		queryParam="";
	             	}
	    	  		baseParams = {
						'queryParam' :queryParam,
						'queryID' : queryID
					};
				});
			 }
			 //增加一个双击监听事件	
			 grid.addListener('rowdblclick', function(){	
			 	    //直接网格编辑，则无双击事件
			 	    if(wgPageModel.wgEditFlag()){
			 	    	return;
			 	    }			 	   	 	  
				 	if(!operDisableFlag){				 	  	
					   if (wgPageOperFlag[3]=="1"){
					   	
					   	 if(wgPageModel.wgCopyPattern()=='0'){
					     	 wgCopy(); //新增复制
					   	 }else{
					   	 	 wgCopy2(); //匹配复制
					   	 }
					   }else{
						   	if (wgPageOperFlag[1]=="1"){
						     wgEditOpen();
						   	}
					   }
				 	}else{
				 		Ext.Msg.alert('操作提示', '当前状态下不能进行处理!');
				 	}
			   },false);
			 //行发生变化			 
			 grid.addListener('rowmousedown', function(thisGrid, rowIndex,  e  ){		
				 
				 grid.getSelectionModel().selectRow(rowIndex);//调用
				 var rows=grid.getSelectionModel().getSelections();	
				 //表格为非编辑状态
				 if (!wgPageModel.wgEditFlag()){			
			       wgDataLoadCallBack(rows);
				 }else{//当表格为编辑，单击事件发生时则触发动态代码表生成
				 	wgGetDynamicCode(rows);	
				 }
			   },false); 
			 //当表格为编辑，当Combox编辑时则触发相关传下的赋值
			  grid.addListener('afteredit', function( e ){	
			  	if (wgPageModel.wgEditFlag()){
			  	  wgComAfterEdit(e);
			  	}
			  },false);
			
			//增加代码字段编辑器
			wgCodeEditor();
			return grid;
    	};
    	   	
    	
    	//添加查询事件
       	var wgQuery=function(){   
       	   var start= 0;
		   var limit;
		   if(wgPageModel.wgPageFlag()=="1"){
		      limit=Ext.getCmp(queryID+'pageSizeTool').pageSize;
		   }
		   var queryParam="";
		   if( wgPageModel.wgQueryType()=="1"){
			   queryParam =Ext.getCmp(queryID+'queryParam').getValue();		 
		   }
		   wgLoadData(start,limit,queryParam);       		
		};
		
		//添加导出事件
       	var wgExport=function(){   
       		var url=wgPageModel.wgRequestURL()+	"?reqCode=export&queryID="+queryID+"&"+wgPageModel.wgOutQueryField()+"="+outQueryKeyValue;
       		if(wgPageModel.wgQueryType()=="2"&&wgPageModel.wgMainQueryFlag()=="1"){
	       		var conditionFields= wgPageModel.wgConditionFields();
	    		var i;
	    	    for(i=0;i<conditionFields.length;i++){    		   
	      	       var field=conditionFields[i];
	      	       var paramValue=Ext.getCmp(queryID+field.fieldName).getValue();	
	      	       if (field.dateFlag=="1"){
	      	    	 paramValue=Ext.util.Format.date(paramValue,"Y-m-d");
	      	    	 if(field.fieldName.substr(0 ,6)=='begin_'){
	      	    		paramValue=paramValue+' 00:00:01';
	      	    	 }else{
	      	    		paramValue=paramValue+' 23:59:59'; 
	      	    	 }
	      	        }         	       
	      	       url=url+"&"+field.fieldName+"="+ paramValue;     	      
	    	    }
       		}     
       		exportExcel(url);	
		};
		
		//添加打印事件
       	var wgPrint=function(){          	
       		var url=wgPageModel.wgRequestURL()+	"?reqCode=export&queryID="+queryID+"&"+wgPageModel.wgOutQueryField()+"="+outQueryKeyValue;
       		if(wgPageModel.wgQueryType()=="2"&&wgPageModel.wgMainQueryFlag()=="1"){
	       		var conditionFields= wgPageModel.wgConditionFields();
	    		var i;
	    	    for(i=0;i<conditionFields.length;i++){    		   
	      	       var field=conditionFields[i];
	      	       var paramValue=Ext.getCmp(queryID+field.fieldName).getValue();	
	      	       if (field.dateFlag=="1"){
	      	    	 paramValue=Ext.util.Format.date(paramValue,"Y-m-d");
	      	    	 if(field.fieldName.substr(0 ,6)=='begin_'){
	      	    		paramValue=paramValue+' 00:00:01';
	      	    	 }else{
	      	    		paramValue=paramValue+' 23:59:59'; 
	      	    	 }
	      	        }         	       
	      	       url=url+"&"+field.fieldName+"="+ paramValue;     	      
	    	    }
       		}     
       		exportExcel(url);	
		};
		
        //添加刷新事件
		var wgRefresh=function(){
			var start =0;
			var limit;
		    if(wgPageModel.wgPageFlag()=="1"){
		      limit=Ext.getCmp(queryID+'pageSizeTool').pageSize;
		    }
			var queryParam;			
	    	wgLoadData(start,limit,queryParam);   			
		};	
	   //增加刷新
	   this.refresh=function(){
		   //业务模型触发，查询模块需要主动点击查询或不是主查询		
		   if(wgPageModel.wgQueryType()=="1"||wgPageModel.wgMainQueryFlag()=="0"){
		     wgRefresh();
		   }
	   };
       //增加一个页面显示数量选择事件
	   var wgPageCombSelect =function() {	
	   			var start=0;
	   	        var number = parseInt(Ext.getCmp(queryID+'combPagesize').getValue());	
			    Ext.getCmp(queryID+'combPagesize').setValue(number);
			    Ext.getCmp(queryID+'pageSizeTool').pageSize=number;	
			    var queryInputObj =Ext.getCmp(queryID+'queryParam');	
			    var queryParam ="";	
		  	    if(!isEmpty(queryInputObj)){
		  	  	  queryParam=queryInputObj.getValue();		  	    		
		  	    }			   
			    var limit=number;			   
			    wgLoadData(start,limit,queryParam); 			
			};   
	   //Grid表格新增
	    var wgGridAdd=function(){
	    	//grid.stopEditing();
	    	var recordType = grid.getStore().recordType;
            var p = new recordType();       
           /* if(!isEmpty(outQueryKeyValue)){
               eval("p."+wgPageModel.wgOutQueryField()+"='"+outQueryKeyValue+"'");
            }else{
               eval("p."+wgPageModel.wgOutQueryField()+"=''");	
            }
            alert("out key Value:"+ eval("p."+wgPageModel.wgOutQueryField()));*/
            p.set(wgPageModel.wgOutQueryField(),outQueryKeyValue);
            var n = grid.getStore().getCount();
            wgPageModel.wgStore().insert(n, p);
            // grid.startEditing(n, 1);
	    };
	   //编辑窗口新增打开 事件	  
		var wgAddOpen=function(){
			 operType='add';
		     wgWindow = new WgWindow(operType,wgPageModel);
		     wgWindow.getWindow().show();
		     var editButton=Ext.getCmp(queryID+"editSave");
             editButton.addListener('click',function(){ 
    	          wgEditSave();
    	     },false);
    	     var closeButton= Ext.getCmp(queryID+"editClose");
    	     closeButton.addListener('click',function(){ 
    	          wgEditClose();
    	     },false);    	    
    	     //设置默认信息
    	     var defaultFields=wgPageModel.wgDefaultFields();        	  
    	     for (var i =0;i<defaultFields.length;i++){
    	     	var defaultField = defaultFields[i];    	     	
       	     	var fieldComp= wgWindow.wgPanel().getForm().findField(defaultField.fieldName);        	     	
    	     	if(defaultField.defaultType=="2"){       	     		
    	     		fieldComp.setValue(outQueryKeyValue) ;     	    
    	     	}else if(defaultField.defaultType=="3"){    	     		
    	     		if(wgPageModel.wgStore().getCount()>0){
    	     			var fieldValue=wgPageModel.wgStore().getAt(0).get(defaultField.defaultValue);   
    	     			fieldComp.setValue(fieldValue);    	     			
    	     		}
    	     	}
    	     	else{
    	     		fieldComp.setValue(defaultField.defaultValue);     	     		
    	     	}    	     	
    	     }	    
    	     //页面初始化进行动态代码的获取
		     dynamicCode();		    
		};
	   /**
	    * 动态代码增加事件监听
	    * @param {} formPanel
	    */
	   var dynamicCodeAddListener=function(){
          var codeFields= wgPageModel.wgCodeFields();
          if (isEmpty(codeFields)){
          	return;
          }       
		  for(var i=0;i<codeFields.length;i++){
		  	  var codeField=codeFields[i];
		  	  var field= wgWindow.wgPanel().getForm().findField(codeField.fieldName);     
			  field.addListener('select',function(combox, record, index){ 
				 dynamicSelectCode(combox, record, index);
			  },false);
		  }
	   };
	   
	   /**
	    * 动态代码发生变化时事件处理
	    * @param {} formPanel
	    * @param {} wgPageModel
	    * @param {} combox
	    * @param {} record
	    * @param {} index
	    */
	   var dynamicSelectCode=function(combox, record, index ){
			var wgDynamicFields= wgPageModel.wgDynamicFields();
			var fieldRelateValue=combox.getValue();		
			if(isEmpty(fieldRelateValue)){
				return;
			}			
			for (var i =0;i<wgDynamicFields.length;i++){
		     	var dynamicField = wgDynamicFields[i];
		     	var codeName=dynamicField.codeName;	
		     	if(combox.name==codeName){
		     		//动态代码表依赖自身不处理
		      		if(dynamicField.codeName==dynamicField.fieldName){
		     			return;
		     		}    	
		       		var comb=wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);	     		
		     		var fieldStore= comb.store;
		     		var dependOtherField='1';
		     		fieldStore.load({
						params : {	
							queryID  :wgPageModel.wgQueryID,
							codeFlag : '2',
							dependOtherField:dependOtherField,
							tableCode :codeName,
							condition :fieldRelateValue,
							reqCode : 'queryCodeValue'
						}
					}							
				  );
		     	}
			}
		};
		
		
		//获取动态Code
	   var wgSetComboxAdd=0;
	   var dynamicCode =function(){
	   	    //获取动态Code	   	     
    	     var wgDynamicFields= wgPageModel.wgDynamicFields();    	   
    	     for (var i =0;i<wgDynamicFields.length;i++){
    	     	var getCodeFlag='1';//动态代码表是否加载
    	     	var dynamicField = wgDynamicFields[i];
    	     	var fieldRelate= wgWindow.wgPanel().getForm().findField(dynamicField.codeName);   
    	     	var fieldRelateValue= fieldRelate.getValue();
    	     	var fieldSelf= wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);   
    	     	var fieldStore= fieldSelf.store;
    	     	var dependOtherField;    	  
                if(dynamicField.codeName==dynamicField.fieldName){    	
    	     		fieldRelateValue='';
    	     		dependOtherField='0';
    	     	}else{
    	     		dependOtherField='1';    	     		
    	     		if(isEmpty(fieldRelateValue)){
    	     			getCodeFlag='0';
    	     		}
    	     	}    	   
    	        if(getCodeFlag=='0'){
    	        	wgSetComboxAdd=wgSetComboxAdd+1;
    	        	wgSetComboxDisplay();	
    	        	continue;
    	        }
    	       	fieldStore.load({
							params : {	
								queryID  :wgPageModel.wgQueryID,
								codeFlag : '2',
								dependOtherField:dependOtherField,
								tableCode :dynamicField.codeName,
								condition :fieldRelateValue,
								reqCode : 'queryCodeValue'
							},
							callback:function(records, options, success){ 									
								wgSetComboxAdd=wgSetComboxAdd+1;														
							    wgSetComboxDisplay();	
							}
						});
    	     	
    	     }
	   };
	   
	   //选中值处理
	   var wgSetComboxDisplay=function(){
	      var wgDynamicFields= wgPageModel.wgDynamicFields();
	      if(wgSetComboxAdd!=wgDynamicFields.length){
	      	return;
	      }	      
	      wgSetComboxAdd=0;
    	  for (var i =0;i<wgDynamicFields.length;i++){
    	  	  var dynamicField = wgDynamicFields[i];
		   	  var fieldSelf= wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);   
		   	  var fieldRelateValue=fieldSelf.getValue();
	    	  var fieldStore= fieldSelf.store;
	    	  if(isEmpty(fieldStore)){
	    	  	return;
	    	  }	   
	    	  var index = fieldStore.find('valueCode', fieldRelateValue);		   	  
		      if (index != -1) {		    
		       	var  record=fieldStore.getAt(index);
		      	var displayValue = record.get("valueName");		      
		      	fieldSelf.setValue(fieldRelateValue);
		      	fieldSelf.setRawValue(displayValue);	         
	          }	   	
    	  }
    	   //增加对代码表的监听事件
    	   dynamicCodeAddListener();
	   };
	   //网格编辑保存功能
	   var wgGridEdit=function(){
	       grid.stopEditing();
	       var changeData=grid.getStore().getModifiedRecords();	  	      
           if (Ext.isEmpty(changeData)&&Ext.isEmpty(deletedRecord)) {
			  Ext.Msg.alert('修改提示', '没有任何修改的数据!');
			  return;
		   }
		    //验证修改数据
	       if(!wgGridValid()){
	       	 return;
	       }
	       //提交数据到后台
	       //删除的数据
		   var deletedLenth=deletedRecord.length;
		   var rowsDelete="";
		   var i=0;
		   var record;
		   var rowStr;
		   for (i=0;i<deletedLenth;i++){		
		   	    record= deletedRecord[i];		   	    
				rowStr='"'+i.toString()+'":'+Ext.encode(record.data);
				if(i<deletedLenth-1){
					rowsDelete=rowsDelete+rowStr+",";
				}else{					
					rowsDelete=rowsDelete+rowStr;
				}
		   }
	       //编辑与新增数据
	       var rowsData="";	       
		   var rowsLength=changeData.length;
		   for (i=0;i<rowsLength;i++){		
		   	    record= changeData[i];
		   	    var innerKeyValue =record.get(wgPageModel.wgInnerKeyField()) ;		   	
		   	    if(isEmpty(innerKeyValue)){		
		   	    	record.set("editflag","inserted");
		   	    }else{		
		   	    	record.set("editflag","modified");
		   	    }		   	   
		   	    var j=i+deletedLenth;
				rowStr='"'+j.toString()+'":'+Ext.encode(record.data);
				if(i<rowsLength-1){
					rowsData=rowsData+rowStr+",";
				}else{					
					rowsData=rowsData+rowStr;
				}
		   }
		   
		   if(!isEmpty(rowsData)&&!isEmpty(rowsDelete)){
		   	 rowsData=rowsDelete+","+rowsData;
		   }else if(isEmpty(rowsData)){
		   	  rowsData=rowsDelete;
		   }else{
		   	  rowsData=rowsData;
		   }
		   rowsData="{"+rowsData+"}";				  
			//Ext.Msg.alert(Ext.util.JSON.decode(rowsData));
		   var batchModify=wgPageModel.wgRequestURL();
		   batchModify=batchModify+	"?reqCode=batchModify&queryID="+queryID;
		   showWaitMsg();
		   Ext.Ajax.request({
						url : batchModify,
						success : function(response) {
							if(!ajaxExeAlert(response.responseText)){
								return;
							}
							wgPageModel.wgStore().reload();
							hideWaitMsg();
							deletedRecord=[];
							Ext.Msg.alert('提示', "数据保存成功!");
						},
						failure : function(response) {
							var resultArray = Ext.util.JSON
									.decode(response.responseText);
							Ext.Msg.alert('提示', resultArray.msg);
						},
						params : {
							rows : rowsData
						}
				   });	   
	   };
	   
	   /**
	    * 网格编辑保存验证数据
	    */
	  var wgGridValid=function(){
	   	var cm=grid.getColumnModel();
	   	var store=wgPageModel.wgStore();
	   	var m = store.modified.slice(0);
		for (var i = 0; i < m.length; i++) {
		  var record = m[i];//获得被修改的每行数据
		  var fields = record.fields.keys;//表示一共几行		
		  for (var j = 0; j < fields.length; j++) {		  	 
		      var name = fields[j];//列名
		      var value = record.data[name];//单元格的数值
		      var colIndex = cm.findColumnIndex(name);//列号		      
		      var rowIndex = store.indexOfId(record.id);//行号		
		      if(!cm.isCellEditable(colIndex)){
		      	continue;
		      }
		      var editor = cm.getCellEditor(colIndex).field;//colIndex使用的编辑器
		      if (!editor.allowBlank&&(isEmpty(value))) {  
		        Ext.Msg.alert('提示', '请确保输入的数据内容。', function(){
		              grid.startEditing(rowIndex, colIndex);
		          });
		          return false;
		      }		  
		  }
		}
		return true;
	  };	   
	   
	   
	   //编辑页面编辑打开事件
	   var wgEditOpen=function(){
			 operType='modify';
		 	 var record = grid.getSelectionModel().getSelected();
			 if (Ext.isEmpty(record)) {
				Ext.Msg.alert('修改提示', '请先选中要修改的项目!');
				return;
			 }
	         wgWindow = new WgWindow(operType,wgPageModel);	 	         
			 wgWindow.getWindow().show();			   
             var editButton=Ext.getCmp(queryID+"editSave");
             editButton.addListener('click',function(){ 
    	          wgEditSave();
    	     },false);
    	     var closeButton= Ext.getCmp(queryID+"editClose");
    	     closeButton.addListener('click',function(){ 
       	          wgEditClose();
    	     },false); 
    	     //设置主键不可编辑
    	     if(!isEmpty(wgPageModel.wgInnerKeyField())){    	     
    	     	var field=wgWindow.wgPanel().getForm().findField(wgPageModel.wgInnerKeyField());
    	     	if(!isEmpty(field)){
    	     		field.getEl().dom.readOnly =true;
    	     	}
    	     }    	  
    	     //给界面元素赋值
	         wgWindow.wgPanel().getForm().loadRecord(record); 	   
	         //页面初始化进行动态代码的获取	 
	  	     dynamicCode();		
	        
		};   
		//编辑页面保存功能
		var wgEditSave=function(){	
			wgWindow= Ext.getCmp(queryID+"editWind");
			var editPanel= wgWindow.getComponent(queryID+'editForm');
			if (!editPanel.form.isValid()) {
			   return;
		    }
		    var editUrl=wgPageModel.wgRequestURL();
		    //Ext.Msg.alert("操作提示1",editUrl);
		    if (operType=='add'){
		       editUrl=editUrl+	"?reqCode=insert&queryID="+queryID;
		    }else{
		       editUrl=editUrl+	"?reqCode=update&queryID="+queryID;
		    }
		   // Ext.Msg.alert("操作提示2",editUrl);
		    editPanel.form.submit({
					url : editUrl,
					waitTitle : '保存提示',
					method : 'POST',
					waitMsg : '正在处理数据,请稍候...',
					success : function(form, action) {
						if(operType=='add'){
							Ext.Msg.alert('操作提示', '数据保存成功。');
							wgEditClose();								
						}else{
						   wgEditClose();
						}
						wgPageModel.wgStore().reload();
						
					},
					failure : function(form, action) {
						var msg = action.result.msg;
						Ext.MessageBox.alert('提示', msg);
					}
				});
		};
		//Grid 编辑数据删除
		var wgGridDelete=function(){			
			grid.stopEditing();		
			var record = grid.getSelectionModel().getSelected();	
			if (Ext.isEmpty(record)) {
				Ext.Msg.alert('提示', '请先选中要删除的项目!');
				return;
			}
			var innerKeyValue=record.get(wgPageModel.wgInnerKeyField());			
			wgPageModel.wgStore().remove(record);
            grid.view.refresh();	
			if(!isEmpty(innerKeyValue)){
			   record.set("editflag","deleted");
			   //内部主键不为空，则可以证明是从服务端取出来的数据	
		       deletedRecord.push(record);
			}
					
	    };
		
		//删除数据
		var wgDeleteData=function(){			
			var rows = grid.getSelectionModel().getSelections();		
			if (Ext.isEmpty(rows)) {
				Ext.Msg.alert('提示', '请先选中要删除的项目!');
				return;
			}	
			var rowsData="";
			var rowsLength=rows.length;
			for (var i=0;i<rowsLength;i++){				
				var rowStr='"'+i.toString()+'":'+Ext.encode(rows[i].data);
				if(i<rowsLength-1){
					rowsData=rowsData+rowStr+",";
				}else{					
					rowsData=rowsData+rowStr;
				}
			}
			rowsData="{"+rowsData+"}";		
			//Ext.Msg.alert(Ext.util.JSON.decode(rowsData));
			var delUrl=wgPageModel.wgRequestURL();
			delUrl=delUrl+	"?reqCode=delete&queryID="+queryID;
			Ext.Msg.confirm('请确认', '你真的要删除项目吗?', function(btn, text) {
				if (btn == 'yes') {
					showWaitMsg();
					Ext.Ajax.request({
								url : delUrl,
								success : function(response) {
									if(!ajaxExeAlert(response.responseText)){
										return;
									}
									wgPageModel.wgStore().reload();
									hideWaitMsg();
									if(!isEmpty(centerGrid)){
								    	centerGrid.refresh();
									}								
								},
								failure : function(response) {
									var resultArray = Ext.util.JSON
											.decode(response.responseText);
									Ext.Msg.alert('提示', resultArray.msg);
								},
								params : {
									rows : rowsData
								}
							});
				}
			});
		};
	
	   		
		//增加一个wgExtend事件
		var wgExtend=function(){
			var rows = grid.getSelectionModel().getSelections();		
			if (Ext.isEmpty(rows)) {
				Ext.Msg.alert('提示', '请先选中要处理的项目!');
				return;
			}	
			var rowsData="";
			var rowsLength=rows.length;
			for (var i=0;i<rowsLength;i++){				
				var rowStr='"'+i.toString()+'":'+Ext.encode(rows[i].data);
				if(i<rowsLength-1){
					rowsData=rowsData+rowStr+",";
				}else{					
					rowsData=rowsData+rowStr;
				}
			}
			rowsData="{"+rowsData+"}";		
			//Ext.Msg.alert(Ext.util.JSON.decode(rowsData));
			var exttendUrl=wgPageModel.wgRequestURL();
			exttendUrl=exttendUrl+	"?reqCode=extend&queryID="+queryID;		
			showWaitMsg();		
			Ext.Ajax.request({
						url : exttendUrl,
						success : function(response) {						
						   var execResult=ajaxExeAlert(response.responseText);
				  		   if(!execResult){
								return;
							}
							hideWaitMsg();
							Ext.Msg.alert('提示', "任务成功执完毕!");
							wgPageModel.wgStore().reload();
							
						
						},
						failure : function(response) {
							var resultArray = Ext.util.JSON
									.decode(response.responseText);
							Ext.Msg.alert('提示', resultArray.msg);
						},
						params : {
							rows : rowsData
						}
					});
		
		
		};
		
		//增加外部扩展事件
		var wgExtend2=function(){
			//由函数本身判断选中行及数量
			var rows = grid.getSelectionModel().getSelections();		
			/*if (Ext.isEmpty(rows)) {
				Ext.Msg.alert('提示', '请先选中要处理的项目!');
				return;
			}	*/
		    var funname=wgPageModel.wgExtendFun()+"('"+wgPageModel.wgExtendQueryID()+"',me)";
			//执行外部事件
			eval(funname);
		};
		//从数据库中取回数据
		var wgLoadData=function(start,limit,queryParam){
			var paramObj ={};
			paramObj.start = start;
			paramObj.queryID=queryID;
			if(!isEmpty(limit)){
				paramObj.limit =limit;
			}			
			if(!isEmpty(queryParam)){
				paramObj.queryParam=queryParam;
			}	
			//增加查询参数
			if(wgPageModel.wgQueryType()=='2'){
				addQueryParam(paramObj);
			}
			wgPageModel.wgStore().load({
						params : paramObj,
						callback: function(records, options, success){						  
		                   if(success){
		                   	   wgDataLoadCallBack(records);
		                   }else{
		                   	  var msg = action.result.msg;
							  Ext.MessageBox.alert('提示', msg);
		                   }
		            	}
					});
		};
		
		//增加查询参数
		var addQueryParam=function(paramObj){
			var conditionFields= wgPageModel.wgConditionFields();
    		var i;
    	    for(i=0;i<conditionFields.length;i++){    		   
      	       var field=conditionFields[i];
      	       var paramValue=Ext.getCmp(queryID+field.fieldName).getValue();	
      	       if (field.dateFlag=="1"){
      	    	 paramValue=Ext.util.Format.date(paramValue,"Y-m-d");
      	    	 if(field.fieldName.substr(0 ,6)=='begin_'){
      	    		paramValue=paramValue+' 00:00:01';
      	    	 }else{
      	    		paramValue=paramValue+' 23:59:59'; 
      	    	 }
      	        }         	       
      	       eval("paramObj."+field.fieldName+"='"+paramValue+"';");      	      
    	    }			
		};
		
		var wgDataLoadCallBack=function(records){	
			 if(interLocking=="0"){
			 	return;
			 }
			 if (Ext.isEmpty(records)) {			
				return;
			 }
			 //得到内主键，通过内主键触发别的Grid
			var innerKey = wgPageModel.wgInnerKeyField();
			if(Ext.isEmpty(innerKey)){
				if(!isEmpty(centerGrid)||!isEmpty(eastGrid)){
				  Ext.MessageBox.alert('提示', "没有配置内部主键信息!");
				}else{
				  return;
				}
			}
			var record;
			if (records instanceof Array){
				record= records[0];
			}
			var keyValue=record.get(innerKey);	
			
		    if(!isEmpty(centerGrid)){	
		    	var outkey= centerGrid.wgOutKeyValue();
		  
		    	if(outkey!=keyValue){
					centerGrid.wgOutQueryKeyValue(keyValue);
					centerGrid.refresh();
		    	}
			};
			if(!isEmpty(eastGrid)){
				var outkey2= eastGrid.wgOutKeyValue();
		    	if(outkey2!=keyValue){
					eastGrid.wgOutQueryKeyValue(keyValue);
					eastGrid.refresh();
		    	}
			}
		};
	    //网格中双击复制事件
		var wgCopy=function(){	
			
			if(isEmpty(centerGrid)){
			  return;
			}
			//编辑标志说明双击要提交给后台
			//自身数据是否提交给后台
			if(wgPageModel.wgDblTrigger()){
				wgDeleteData();   
			}/*else{
			   	wgRefresh();
			}*/
			//相关表格是否提交数据给后台			
			if(centerGrid.wgGetPageModel().wgDblTrigger()){
				var rows = grid.getSelectionModel().getSelections();	
				var storeRecord =copyCenterData();				
				var url = centerGrid.wgGetPageModel().wgRequestURL();
				url=url+"?reqCode=insert&queryID="+centerGrid.wgGetPageModel().wgQueryID;
			    submitCopyData(rows[0],url,storeRecord.data,storeRecord);
			}/*else{
				//增加一行记录
				centerGrid.refresh();
			}*/
			
		};
		
		//得到复制提交数据
		var copyCenterData=function(){
			//得到Record结构
			var record=centerGrid.wgGetPageModel().wgStoreRecord();
			var storeRecord=new Ext.data.Record(record);	
		
		    //中心网格进行新增操作，赋值默认值	
			var firstRow ;
			var centerStore=centerGrid.wgGetPageModel().wgStore();	
			var data={};
			if(centerStore.getCount()>0){
				firstRow=centerStore.getAt(0);
			}
			//设置默认置
			var defaultFields=centerGrid.wgGetPageModel().wgDefaultFields(); 
			var j=0;
    	     for (j =0;j<defaultFields.length;j++){
    	     	var defaultField = defaultFields[j];    	     	
    	     	if(defaultField.defaultType=="2"){    
    	     		if(!isEmpty(centerGrid.wgOutKeyValue())){
    	     		  eval("data."+defaultField.fieldName+"='"+centerGrid.wgOutKeyValue()+"'");
    	     		}
    	     	}else if(defaultField.defaultType=="3"){    	     		
    	     		//当前选中行
    	     		if (!Ext.isEmpty(firstRow)){    	
    	     		   if(!isEmpty(firstRow.get(defaultField.fieldName))){
    	     		      eval("data."+defaultField.fieldName+"='"+firstRow.get(defaultField.fieldName)+"'");
    	     		   }
    	     		}
    	     	}
    	     	else{   
    	     		if(!isEmpty(defaultField.defaultValue)){
    	     		  eval("data."+defaultField.fieldName+"='"+defaultField.defaultValue+"'");  
    	     		}
	    	 	}        	     	
    	     }		 	    
    	     
			//把双击中相同字段进行赋值
			var assignFields= wgPageModel.wgStoreRecord();
			var rows = grid.getSelectionModel().getSelections();	
		    
	   	    for (j =0;j<assignFields.length;j++){
	   	     	var assignField = assignFields[j];	   	     	
	   	     	for (var i=0;i<record.length;i++){
	   	     		if(record[i].name!=assignField.name){
	   	     			continue;	   	     			
	   	     		}  
	   	     		if(!isEmpty(rows[0].get(assignField.name))){
	   	     		  eval("data."+assignField.name+"='"+rows[0].get(assignField.name)+"'"); 
	   	     		}
	   	     	}    	     	
    	    }	  
    	    storeRecord.data=data;
    	    return storeRecord;
    	    
		};
		
		//复制提交数据给后台
		var submitCopyData=function(currentRow,url,param,record){			
			Ext.Ajax.request({
								url : url,
								success : function(response) {
									if(!ajaxExeAlert(response.responseText)){
										return;
									}
									hideWaitMsg();
									//回调实现表格当前行数据变化																
									centerGrid.wgGetPageModel().wgStore().add(record);									
									centerGrid.wgCreateGrid().getSelectionModel().selectLastRow();																
								    wgPageModel.wgStore().remove(currentRow);
								},
								failure : function(response) {
									var resultArray = Ext.util.JSON
											.decode(response.responseText);
									Ext.Msg.alert('提示', resultArray.msg);
								},
								params : param
							});
		};
		
		//匹配复制
		var wgCopy2=function(){
			//关联表格			
			var rows = centerGrid.wgGrid().getSelectionModel().getSelections();		
			if (Ext.isEmpty(rows)) {
				Ext.Msg.alert('提示', '请先选中要匹配的行!');
				return;
			}	
			var record=rows[0];
			var fields=record.fields;
			//当前表
			var rowsCurrentGrid  = grid.getSelectionModel().getSelections();	
			if (Ext.isEmpty(rowsCurrentGrid)) {
				Ext.Msg.alert('提示', '请先选中要匹配的行!');
				return;
			}	
			var currentGridRecord = rowsCurrentGrid[0];
			var currentFields=currentGridRecord.fields;
			//相同字段赋值
			for(var i=0;i<fields.length;i++){
				var fieldName=fields.items[i].name;
				for(var j=0;j<currentFields.length;j++){
					var currentFieldsName= currentFields.items[j].name;
					if(fieldName!=currentFieldsName){
						continue;
					}
					var currentFieldValue= currentGridRecord.get(currentFieldsName);
					//alert(fieldName+" "+ currentFieldsName +" "+currentFieldValue);
					if(isEmpty(currentFieldValue)){
						continue;
					}
					record.set(fieldName,currentFieldValue);
				}
			}
			//提交数据
			if(centerGrid.wgGetPageModel().wgDblTrigger()){						
				var url = centerGrid.wgGetPageModel().wgRequestURL();
				url=url+"?reqCode=update&queryID="+centerGrid.wgGetPageModel().wgQueryID;
				//alert(Ext.util.JSON.encode(record.data));
			    submitCopy2Data(url,record.data);
			}
		};
		
		//复制匹配模式提交数据给后台
		var submitCopy2Data=function(url,param){
			Ext.Ajax.request({
								url : url,
								success : function(response) {
									if(!ajaxExeAlert(response.responseText)){
										return;
									}
									hideWaitMsg();								
								},
								failure : function(response) {
									var resultArray = Ext.util.JSON
											.decode(response.responseText);
									Ext.Msg.alert('提示', resultArray.msg);
								},
								params : param
							});
		};
		
	    //增加编辑页面关闭功能
	    var wgEditClose=function(){ 
	    	wgWindow= Ext.getCmp(queryID+"editWind");	  
	    	wgWindow.close();
	    };
	    //增加代码表编辑功能
	    var wgCodeEditor=function(){
	    	if(!wgPageModel.wgEditFlag()){
	    		return;
	    	}
	    	//得到所有的代码表
	    	var codeTabls=wgPageModel.wgCodeFields();
	    	//得到ColumnModel
	    	var columnModel= grid.getColumnModel();
	    	for(var i=0;i<codeTabls.length;i++){
	    		var field=codeTabls[i];
	    		var columnIdx= columnModel.findColumnIndex(field.fieldName);
	    		var editor=getComboxEditor(field);
	    		columnModel.setEditor(columnIdx,editor);
	    	}
	    };
	    //得到ComboxEdit
	    var getComboxEditor=function(field){
	    	var obj={};
	    	 obj.id ="combo"+field.fieldName;
	    	// obj.name = "combo"+field.fieldName;
		   //  obj.hiddenName = "combo"+field.fieldName;
	    	 var codeTable;
	    	 var dependOtherField='0';
	    	 if(field.codeFlag=='1'||field.codeFlag=='2'||field.codeFlag=='3'){
    	 	    codeTable=field.codeName;
    	 	    dependOtherField='2';
	    	 }else{	 
	    	 	if(field.codeName==field.fieldName){
	    	 	  codeTable=field.codeName;
	    	 	  dependOtherField='0';
	    	 	}else{
	    	 	   codeTable='dynamic';
	    	 	   dependOtherField='1';
	    	 	}
	    	 }	    	 
	    	 var url=wgPageModel.wgRequestURL();
	    	 var queryID= wgPageModel.wgQueryID;
			 var wgCodeTable =new WgCodeTable(url,codeTable,queryID,dependOtherField);			 
		     obj.store =wgCodeTable.getCode();		     
		     obj.valueField ='valueCode';
		     obj.displayField ='valueName';
		     obj.forceSelection =true;
		     obj.typeAhead =true;
		     obj.triggerAction ='all';
		     obj.selectOnFocus =true;// 用户不能自己输入,只能选择列表中有的记录
		     obj.allowBlank =field.allowBlank;
		     obj.lazyRender=false; 
		     obj.mode ="local";
		     obj.editable = false;
		     return new Ext.form.ComboBox(obj);
	    };
	    
	    //动态获取代表信息
	    var wgGetDynamicCode=function(rows){
	    	if(isEmpty(rows)){
	    		return;
	    	}
	    	var record=rows[0];
	    	var wgDynamicFields= wgPageModel.wgDynamicFields();
	    	var columnModel= grid.getColumnModel();
    	     for (var i =0;i<wgDynamicFields.length;i++){
    	     	var getCodeFlag='1';
    	     	var dynamicField = wgDynamicFields[i];
    	     	var codeName=dynamicField.codeName;
    	     	var fieldRelateValue;
    	     	if(codeName.indexOf('|')==-1){
    	     		fieldRelateValue= record.get(codeName);  
    	     	}else{
    	     		//专为代码匹配而准备的
    	     		var codeNames=codeName.split("|");
    	     		fieldRelateValue=record.get(codeNames[0])+record.get(codeNames[1]); 
    	     		codeName=fieldRelateValue;
    	     	}    	
    	     	var columnIdx= columnModel.findColumnIndex(dynamicField.fieldName);
	    		var field=columnModel.getCellEditor(columnIdx).field;    
	    		//alert("editor:"+field);
    	     	var fieldStore= field.store;
    	     	var dependOtherField;
				if(dynamicField.codeName==dynamicField.fieldName){
					fieldRelateValue='';
					dependOtherField='0';
				}else{
					dependOtherField='1';
					if(isEmpty(fieldRelateValue)){
						getCodeFlag='0';
					}
				}
				if(getCodeFlag=='0'){
				  	wgSetComboxAdd=wgSetComboxAdd+1;
				  	wgSetComboxDisplay();	
				  	continue;
				 }
    	       	fieldStore.load({
							params : {	
								queryID  :wgPageModel.wgQueryID,
								codeFlag : '2',
								dependOtherField:dependOtherField,
								tableCode :codeName,
								condition :fieldRelateValue,
								reqCode : 'queryCodeValue'
							},
							callback:function(records, options, success){ 	
								//wgSetComboxAdd=wgSetComboxAdd+1;
								if(!success){
									return;
								}								
								if(isEmpty(fieldRelateValue)){
									return;
								}								
							   // wgSetComboxDisplay();	
							}
						}							
					);
    	     	
    	     }
	    };
	//表格编辑后触发    
	var wgComAfterEdit=function(e){
		//得到所有的代码表
		var fieldName= e.field;
	    var codeTables=wgPageModel.wgCodeFields();
	    var columnModel=grid.getColumnModel();	  
	    var columnIdx= columnModel.findColumnIndex(fieldName);
	    for(var i=0;i<codeTables.length;i++){
	    	var codeTable=codeTables[i];
	    	if(codeTable.fieldName==fieldName){
	    		//找到相应的codeName;
	    		var index=fieldName.indexOf("value");
	    		if(index==-1){
	    			return;
	    		}
	    		var codeDispName= fieldName.substr(0,index )+"name";		    	
	    		//得到编辑字段
	    		var field=columnModel.getCellEditor(columnIdx).field;	    	
	    		//得到显示值
	    		var displayValue=field.getRawValue();	
	    		var record=e.record;
	    		var fields=record.fields.items;
	    		//判断record存在某个字段
	    		for(var j=0;fields.length;j++){
	    			var field2=fields[j];
	    			//给代码显示字段赋值
	    			if(field2.name==codeDispName){
	    			   record.set(codeDispName,displayValue);
	    			   //最后退出
	    		       return;
	    			}
	    		}
	    		
	    	}
	    }
	};
	//end;
  }
    
   /**
   * 定义一个WgTree
   * @param {} queryID
   * @param {} wgTreeMenuClick
   */
  function WgTree(queryID){
  	//创建页面信息模型
    var wgPageModel = new WgPageModel(queryID);  
    wgPageModel.wgPanelItems();
  	//Tree RootName
    var rootName;
    var operType; //操作类型
    var wgWindow;//编辑窗口
    var centerGrid;
    this.getCenterGrid=function(){return centerGrid;};
    this.wgCenterGrid=function(grid){
    	centerGrid=grid;
    	return centerGrid;
    };
    var centerGrid2;
    this.getCenterGrid2=function(){return centerGrid2;};
    this.wgCenterGrid2=function(grid){
    	centerGrid2=grid;
    	return centerGrid2;
    }; 
    var nodeValue ='';
    this.wgRootName= function(){
        if(!isEmpty(rootName)){
        	return rootName;
        }
    	var rootType= {};
    	rootType.text=wgPageModel.wgFunctionDesc();
    	rootType.expanded =true;
    	rootType.id='all';
    	rootName= new Ext.tree.AsyncTreeNode(rootType);      
        return rootName;
    };
    //右击菜单
    var contextMenu;
    this.wgContextMenu=function(){
    	if(!isEmpty(contextMenu)){	
    		return contextMenu;
    	}	
    	var items=[];
    	var wgPageOperFlag=wgPageModel.wgPageOperFlag();
    	if (wgPageOperFlag[0]=="1"){
	    	var itemAdd={};   	
	    	itemAdd.id =queryID+'addMenu';
	    	itemAdd.name =queryID+'addMenu';
	    	itemAdd.text ='新增';
	    	itemAdd.handler =function() {wgAddOpen();};
	    	itemAdd.iconCls='page_addIcon';
	    	itemAdd.hidden=false;
	    	items.push(itemAdd);
    	}
    	if (wgPageOperFlag[1]=="1"){
	    	var itemModify={};   	
	    	itemModify.id =queryID+'modifyMenu';
	    	itemModify.name =queryID+'modifyMenu';
	    	itemModify.handler =function() {wgEditOpen();};
	    	itemModify.iconCls='page_edit_1Icon';
	    	itemModify.text ='修改';
	    	itemModify.hidden=false;
	    	items.push(itemModify);
    	}
    	if (wgPageOperFlag[2]=="1"){
	    	var itemDelete={};   	
	    	itemDelete.id =queryID+'delMenu';
	    	itemDelete.name =queryID+'delMenu';
	    	itemDelete.iconCls='page_delIcon';
	    	itemDelete.handler =function() {wgDeleteData();};
	    	itemDelete.text ='删除';
	    	itemDelete.hidden=false;
	    	items.push(itemDelete);
    	}
    	if (wgPageOperFlag[3]=="1"){
	    	var itemExtend={};   	
	    	itemExtend.id =queryID+'extendMenu';
	    	itemExtend.name =queryID+'extendMenu';
	    	itemExtend.iconCls='configIcon';
	    	itemExtend.text ='复制';
	    	itemExtend.handler =function() {wgCopy();};
	    	itemExtend.hidden=false;
	    	items.push(itemExtend);
    	}
    	if (wgPageOperFlag[4]=="1"){
	    	var itemRefresh={};   	
	    	itemRefresh.id =queryID+'refreshMenu';
	    	itemRefresh.name =queryID+'refreshMenu';
	    	itemRefresh.iconCls='page_refreshIcon';
	    	itemRefresh.text='刷新';
	    	itemRefresh.hidden=false;
	    	itemRefresh.handler =function() {wgRefresh();};
	    	items.push(itemRefresh);
    	}
    	var menuObject={};
    	menuObject.id="menuTreeContextMenu";
    	menuObject.items=items;
    	contextMenu=new Ext.menu.Menu(menuObject);
    	return contextMenu;
    }
    //menu Tree
    var menuTree;
    this.wgGetMenuTree=function(){ 
    	if(!isEmpty(menuTree)){	
    		return menuTree;
    	}	
		var treeLoaderType={};
		treeLoaderType.baseAttrs={};
		treeLoaderType.dataUrl=wgPageModel.wgRequestURL()+"?reqCode=queryForTree&leaf=false&queryID="+queryID;
		var treeLoader =  new Ext.tree.TreeLoader(treeLoaderType);
		var menuTreeType = {};
		menuTreeType.loader=treeLoader;	
		menuTreeType.root=this.wgRootName();
		menuTreeType.title=wgPageModel.wgFunctionDesc();
		menuTreeType.rootVisible=true;     //隐藏根节点 
		menuTreeType.autoScroll=true;
		menuTreeType.containerScroll=true;
		menuTreeType.animate=true;
		menuTreeType.useArrows=true;		
		menuTreeType.border=false;	
	

		//menuTreeType.collapsible=true;  //panel可收缩  
		menuTree= new Ext.tree.TreePanel(menuTreeType);
		//menuTree.setRootNode(this.wgRootName()); //设置根结点
		//传递Node 参数
	    menuTree.on('beforeload', 
	            function(node){ 
	               //定义子节点的Loader 
	               var url=wgPageModel.wgRequestURL()+"?reqCode=queryForTree&queryID="+queryID+"&leaf="+node.isLeaf(); 	               
	               var outKey=wgPageModel.wgOutQueryField();
	               if(!isEmpty(outKey)){
	               	  url=url+"&"+outKey+"="+node.attributes.id;
	               }
	               if(!node.isLeaf()){		               
		               menuTree.loader.dataUrl=url;
	               }else{
	               }
	            }); 
		//增加一个Click事件
		menuTree.on('click', function(node) {
					var menuid = node.attributes.id;
					nodeValue=node;
					var menuname = node.attributes.text;
					if(node.isLeaf()){						
						centerGrid.setButtonDisabled(false);	
						if(!isEmpty(centerGrid2)){
							centerGrid2.setButtonDisabled(false);	
						}
					}else{				
						centerGrid.setButtonDisabled(true);	
						if(!isEmpty(centerGrid2)){
							centerGrid2.setButtonDisabled(true);	
						}
					}
					//增加对中心区域的数据刷新，利用store的功能
				    centerGrid.wgOutQueryKeyValue(menuid);
				    centerGrid.refresh();
				    if(!isEmpty(centerGrid2)){
				       centerGrid2.wgOutQueryKeyValue(menuid);
				       centerGrid2.refresh();
				    }
		       }
			);
		//增加一个上下文菜单
	    var menuObj= this.wgContextMenu();
		menuTree.on('contextmenu', function(node, e) {
			e.preventDefault();
			var menuid = node.attributes.id;		
			nodeValue=node;
			if(node.isLeaf()){
			    //增加对中心区域数据刷新	
				centerGrid.setButtonDisabled(false);	
				if(!isEmpty(centerGrid2)){
					centerGrid2.setButtonDisabled(false);	
				}
				node.select();							
				menuObj.showAt(e.getXY());
			    Ext.getCmp(queryID+'addMenu').disable();
				Ext.getCmp(queryID+'modifyMenu').enable();
				Ext.getCmp(queryID+'delMenu').enable();
				Ext.getCmp(queryID+'extendMenu').enable();
				Ext.getCmp(queryID+'refreshMenu').enable();			
				
			}else if((!isEmpty(node.firstChild)&&node.firstChild.isLeaf())||(isEmpty(node.firstChild)&&!node.isLeaf())){
				node.select();					
				menuObj.showAt(e.getXY());
				Ext.getCmp(queryID+'addMenu').enable();
				Ext.getCmp(queryID+'modifyMenu').disable();
				Ext.getCmp(queryID+'delMenu').disable();
				Ext.getCmp(queryID+'extendMenu').disable();
				Ext.getCmp(queryID+'refreshMenu').enable();	
				centerGrid.setButtonDisabled(true);	
				if(!isEmpty(centerGrid2)){
					centerGrid2.setButtonDisabled(true);	
				}			
			}else{
				centerGrid.setButtonDisabled(true);	
				if(!isEmpty(centerGrid2)){
					centerGrid2.setButtonDisabled(true);	
				}	
			}
			//右边同步刷新
			 centerGrid.wgOutQueryKeyValue(menuid);
			 centerGrid.refresh();
			 if(!isEmpty(centerGrid2)){
		       centerGrid2.wgOutQueryKeyValue(menuid);
		       centerGrid2.refresh();
		     }
		});	
		menuTree.render(Ext.getBody()); 
		//menuTree.render('tree-div'); 
		menuTree.getRootNode().expand();		
	
	   	return menuTree;
    }; 
    //编辑窗口新增打开 事件
		var wgAddOpen=function(){
			 operType='add';	
		     wgWindow = new WgWindow(operType,wgPageModel);
		     wgWindow.getWindow().show();		    
		     var editButton=Ext.getCmp(queryID+"editSave");
             editButton.addListener('click',function(){ 
    	          wgEditSave();
    	     },false);
    	     var closeButton= Ext.getCmp(queryID+"editClose");
    	     closeButton.addListener('click',function(){ 
    	          wgEditClose();
    	     },false);  
    	     //设置默认信息
    	     var defaultFields=wgPageModel.wgDefaultFields();    
    	     for (var i =0;i<defaultFields.length;i++){
    	     	var defaultField = defaultFields[i];    	     	
       	     	var fieldComp= wgWindow.wgPanel().getForm().findField(defaultField.fieldName);        	     	
    	     	if(defaultField.defaultType=="2"){       	     		
    	     		fieldComp.setValue(nodeValue.attributes.id) ;     	    
    	     	}else{
    	     		fieldComp.setValue(defaultField.defaultValue); 
    	     		//fieldComp.setRawValue(defaultField.defaultValue) ;   
    	     	}
    	     }	   
    	     //动态获取代码表
	         dynamicCode();
		};
		
	   //编辑页面编辑打开事件
	   
	   var wgEditOpen=function(){
			 operType='modify';
			var nodeID=nodeValue.attributes.id;
		    var innerKey= wgPageModel.wgInnerKeyField();
		    var param = {};
		    param.nodeID =nodeID;
		    param.start=0;
		    param.limit=50;
		    param.queryID=queryID;
		    if(!isEmpty(innerKey)){
		       eval("param."+innerKey+"='"+nodeID+"'");
		    }
			 wgPageModel.wgStore().load({
				params : param,
				callback: function(records, options, success){
                   if(success){
                   	   wgEditOpenCallBack(records);
                   }else{
                   	  var msg = action.result.msg;
					  Ext.MessageBox.alert('提示',  msg);
                   }
            	}
			});
		 	    
		};   
		
		//编辑打开窗口回调
		var wgEditOpenCallBack=function(records){
		    if (Ext.isEmpty(records)) {
				Ext.Msg.alert('修改提示', '请先选中要修改的项目');
				return;
			 }
	         wgWindow = new WgWindow(operType,wgPageModel);	 	         
			 wgWindow.getWindow().show();			   
             var editButton=Ext.getCmp(queryID+"editSave");
             editButton.addListener('click',function(){ 
    	          wgEditSave();
    	     },false);
    	     var closeButton= Ext.getCmp(queryID+"editClose");
    	     closeButton.addListener('click',function(){ 
       	          wgEditClose();
    	     },false); 
    	      //设置主键不可编辑
    	     if(!isEmpty(wgPageModel.wgInnerKeyField())){
    	     	var field=wgWindow.wgPanel().getForm().findField(wgPageModel.wgInnerKeyField());
    	     	if(!isEmpty(field)){
    	     		field.getEl().dom.readOnly =true;
    	     	}
    	     }    	
	         wgWindow.wgPanel().getForm().loadRecord(records[0]); 	
	         //动态获取代码表
	         dynamicCode();
		};
		
	  //获取动态Code
	   var wgSetComboxAdd=0;
	   var dynamicCode =function(){
	   	  //获取动态Code
    	     var wgDynamicFields= wgPageModel.wgDynamicFields();    	  
    	     for (var i =0;i<wgDynamicFields.length;i++){
    	     	var dynamicField = wgDynamicFields[i];
    	     	var fieldRelate= wgWindow.wgPanel().getForm().findField(dynamicField.codeName);   
    	     	var fieldRelateValue= fieldRelate.getValue();
    	     	var fieldSelf= wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);   
    	     	var fieldStore= fieldSelf.store;
    	     	var dependOtherField;    	 
    	     	if(dynamicField.codeName==dynamicField.fieldName){
    	     		dependOtherField='0';
    	     	}else{
    	     		dependOtherField='1';
    	     	}
    	       	fieldStore.load({
							params : {	
								queryID  :wgPageModel.wgQueryID,
								codeFlag : '2',
								dependOtherField:dependOtherField,
								tableCode :dynamicField.codeName,
								condition :fieldRelateValue,
								reqCode : 'queryCodeValue'
							},
							callback:function(records, options, success){ 	
								wgSetComboxAdd=wgSetComboxAdd+1;
								if(!success){
									return;
								}								
							/*	if(isEmpty(fieldRelateValue)){
									return;
								}	*/									
							    wgSetComboxDisplay();	
							}
						});
    	     	
    	     }
	   };
	   
	   
	  //选中值处理
	  var wgSetComboxDisplay=function(){
		  var wgDynamicFields= wgPageModel.wgDynamicFields();
		  if(wgSetComboxAdd!=wgDynamicFields.length){
		  	return;
		  }	  
		  wgSetComboxAdd=0;
		  for (var i =0;i<wgDynamicFields.length;i++){
		  	  var dynamicField = wgDynamicFields[i];
		 	  var fieldSelf= wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);   
		 	  var fieldRelateValue=fieldSelf.getValue();
			  var fieldStore= fieldSelf.store;
			  if(isEmpty(fieldStore)){
			  	return;
			  }	   
			  var index = fieldStore.find('valueCode', fieldRelateValue);		   	  
		      if (index != -1) {		    
		     	 var  record=fieldStore.getAt(index);
		    	 var displayValue = record.get("valueName");		      
		    	 fieldSelf.setValue(fieldRelateValue);
		    	 fieldSelf.setRawValue(displayValue);	         
		      }	   	
		  }
		  //增加对代码表的监听事件
		  dynamicCodeAddListener();
	  };
	  
	   /**
	* 动态代码增加事件监听
	* @param {} formPanel
	*/
	var dynamicCodeAddListener=function(){
	    var codeFields= wgPageModel.wgCodeFields();
	    if (isEmpty(codeFields)){
	    	return;
	    }       
			for(var i=0;i<codeFields.length;i++){
				  var codeField=codeFields[i];
				  var field= wgWindow.wgPanel().getForm().findField(codeField.fieldName);     
			  field.addListener('select',function(combox, record, index){ 
				 dynamicSelectCode(combox, record, index);
			  },false);
			}
     };
     
     /**
	    * 动态代码发生变化时事件处理
	    * @param {} formPanel
	    * @param {} wgPageModel
	    * @param {} combox
	    * @param {} record
	    * @param {} index
	    */
	   var dynamicSelectCode=function(combox, record, index ){
			var wgDynamicFields= wgPageModel.wgDynamicFields();
			var fieldRelateValue=combox.getValue();		
			if(isEmpty(fieldRelateValue)){
				return;
			}			
			for (var i =0;i<wgDynamicFields.length;i++){
		     	var dynamicField = wgDynamicFields[i];
		     	var codeName=dynamicField.codeName;			     	
		     	if(combox.name==codeName){		     		
		     		//动态代码表依赖自身不处理		     		
		      		if(dynamicField.codeName==dynamicField.fieldName){
		     			return;
		     		}    	
		       		var comb=wgWindow.wgPanel().getForm().findField(dynamicField.fieldName);	     		
		     		var fieldStore= comb.store;
		     		var dependOtherField='1';
		     		fieldStore.load({
						params : {	
							queryID  :wgPageModel.wgQueryID,
							codeFlag : '2',
							dependOtherField:dependOtherField,
							tableCode :codeName,
							condition :fieldRelateValue,
							reqCode : 'queryCodeValue'
						}
					}							
				  );
		     	}
			}
		};
		
	    //编辑页面保存功能
		var wgEditSave=function(){	
			wgWindow= Ext.getCmp(queryID+"editWind");
			var editPanel= wgWindow.getComponent(queryID+'editForm');
			if (!editPanel.form.isValid()) {
			   return;
		    }
		    var editUrl=wgPageModel.wgRequestURL();
		    Ext.Msg.alert("操作提示1",editUrl);
		    if (operType=='add'){
		       editUrl=editUrl+	"?reqCode=insert&queryID="+queryID;
		    }else{
		       editUrl=editUrl+	"?reqCode=update&queryID="+queryID;
		    }
		   // Ext.Msg.alert("操作提示2",editUrl);		    
		    editPanel.form.submit({
					'url' : editUrl,
					'waitTitle' : '保存提示',
					'method' : 'POST',
					'waitMsg' : '正在处理数据,请稍候...',
					'success' : function(form, action) {
						Ext.MessageBox.alert('提示', '数据保存成功!');	
						wgEditClose();
						wgRefresh();
					},
					failure : function(form, action) {
						var msg = action.result.msg;
						Ext.MessageBox.alert('提示',  msg);
					}
				});
		};
		//删除数据
		var wgDeleteData=function(){	
			if (Ext.isEmpty(nodeValue)) {
				Ext.Msg.alert('提示', '请先选中删除的项目!');
				return;
			}	
			if(!nodeValue.isLeaf()){
				Ext.Msg.alert('提示', '不是未级结点，不能进行删除!');
				return;
			}
			var delUrl=wgPageModel.wgRequestURL();
		    delUrl=delUrl+"?reqCode=treeDelete&queryID="+queryID;	
		    var innerKey= wgPageModel.wgInnerKeyField();
		    var param = {};
		    param.nodeID =nodeValue.attributes.id;
		    if(!isEmpty(innerKey)){
		       eval("param."+innerKey+"='"+nodeValue.attributes.id+"'");
		    }
			showWaitMsg();
			Ext.Ajax.request({
						url : delUrl,
						success : function(response) {
							if(!ajaxExeAlert(response.responseText)){
									return;
							}
							nodeValue.remove();                  			
							Ext.Msg.alert('提示', '成功删除数据集!');
							hideWaitMsg();
							centerGrid.wgOutQueryKeyValue("");
			                centerGrid.refresh();
						
						},
						failure : function(response) {
							var resultArray = Ext.util.JSON.decode(response.responseText);
							Ext.Msg.alert('提示', resultArray.msg);
						},
						params :param
					});
		};
		//复制
		var wgCopy=function(){
			if (Ext.isEmpty(nodeValue)) {
				Ext.Msg.alert('提示', '请先选中复制的项目!');
				return;
			}	
			if(!nodeValue.isLeaf()){
				Ext.Msg.alert('提示', '不是未级结点，不能进行复制!');
				return;
			}
			var copyUrl=wgPageModel.wgRequestURL();
		    copyUrl=copyUrl+"?reqCode=treeCopy&queryID="+queryID;	
			showWaitMsg();
			var innerKey= wgPageModel.wgInnerKeyField();
		    var param = {};
		    param.nodeID =nodeValue.attributes.id;
		    if(!isEmpty(innerKey)){
		       eval("param."+innerKey+"='"+nodeValue.attributes.id+"'");
		    }
			Ext.Ajax.request({
						url : copyUrl,
						success : function(response) {
							if(!ajaxExeAlert(response.responseText)){
									return;
							}
							var resultArray = Ext.util.JSON.decode(response.responseText);
							var parentCode=resultArray.parentCode;
							var nodeCode = resultArray.nodeCode;
							var nodeParent=menuTree.getNodeById(parentCode);
							nodeParent.expand();
							nodeParent.reload();		                  			
							//m展开树，选中复制节点
							hideWaitMsg();
						
						},
						failure : function(response) {
							var resultArray = Ext.util.JSON
									.decode(response.responseText);
							Ext.Msg.alert('提示', resultArray.msg);
						},
						params : param
					});
		};
		
        //刷新数据
		var wgRefresh=function(){
			var selectModel = menuTree.getSelectionModel();
			var selectNode = selectModel.getSelectedNode();
			if (selectNode.attributes.leaf) {
				selectNode.parentNode.reload();
			} else {
				selectNode.reload();
			}
		};
		//增加编辑页面关闭功能
	    var wgEditClose=function(){ 
	    	wgWindow= Ext.getCmp(queryID+"editWind");	  
	    	wgWindow.close();
	    };
  //end
  }
//结束 
  Ext.namespace('wg.comp');
  wg.comp.GridCombox = Ext.extend(Ext.form.ComboBox,{
  	multiSelect : false,	
  	createPicker : function(){
		var me = this;		
		alert("ok");
		var picker = Ext.create('Ext.grid.Panel', {
			title : '下拉表格',
		    store: me.store,
		    frame : true,
		    resizable : true,
		    columns : [{
		    	text : '编码',
		    	dataIndex : 'valueCode'
		    },{
		    	text : '名称' ,
		    	dataIndex : 'valueName'
		    },{
		    	text : '拼音码',
		    	dataIndex : 'pym'
		    },{
		    	text : '类别' ,
		    	dataIndex : 'memo'
		    }],
		    selModel: {
                mode: me.multiSelect ? 'SIMPLE' : 'SINGLE'
            },
		    floating: true,
            hidden: true,
            width : 300,
            columnLines : true,
            focusOnToFront: false
		});
		
		me.mon(picker, {
            itemclick: me.onItemClick,
            refresh: me.onListRefresh,
            scope: me
        });

        me.mon(picker.getSelectionModel(), {
            beforeselect: me.onBeforeSelect,
            beforedeselect: me.onBeforeDeselect,
            selectionchange: me.onListSelectionChange,
            scope: me
        });
		this.picker = picker;
		return picker;
	},
	
	onItemClick: function(picker, record){
		alert('ok');
        /*
         * If we're doing single selection, the selection change events won't fire when
         * clicking on the selected element. Detect it here.
         */
        var me = this,
            selection = me.picker.getSelectionModel().getSelection(),
            valueField = me.valueField;

        if (!me.multiSelect && selection.length) {
            if (record.get(valueField) === selection[0].get(valueField)) {
                // Make sure we also update the display value if it's only partial
                me.displayTplData = [record.data];
                me.setRawValue(me.getDisplayValue());
                me.collapse();
            }
        }
    },
    
    matchFieldWidth : false,
    
    onListSelectionChange: function(list, selectedRecords) {
        var me = this,
            isMulti = me.multiSelect,
            hasRecords = selectedRecords.length > 0;
        // Only react to selection if it is not called from setValue, and if our list is
        // expanded (ignores changes to the selection model triggered elsewhere)
        if (!me.ignoreSelection && me.isExpanded) {
            if (!isMulti) {
                Ext.defer(me.collapse, 1, me);
            }
            /*
             * Only set the value here if we're in multi selection mode or we have
             * a selection. Otherwise setValue will be called with an empty value
             * which will cause the change event to fire twice.
             */
            if (isMulti || hasRecords) {
                me.setValue(selectedRecords, false);
            }
            if (hasRecords) {
                me.fireEvent('select', me, selectedRecords);
            }
            me.inputEl.focus();
        }
        console.log(me.getValue());
    },
    
    doAutoSelect: function() {
        var me = this,
            picker = me.picker,
            lastSelected, itemNode;
        if (picker && me.autoSelect && me.store.getCount() > 0) {
            // Highlight the last selected item and scroll it into view
            lastSelected = picker.getSelectionModel().lastSelected;
            itemNode = picker.view.getNode(lastSelected || 0);
            if (itemNode) {
                picker.view.highlightItem(itemNode);
                picker.view.el.scrollChildIntoView(itemNode, false);
            }
        }
    }
 
   }
  );
  //结束
  Ext.reg('wggridcombox',wg.comp.GridCombox);
  
  wg.comp.TreeComboBox=Ext.extend(Ext.form.ComboBox,{
  	multiSelect : true,
	
	createPicker : function(){
		var me = this;
		
		//创建树控件
		var picker = Ext.create('Ext.tree.Panel', {
		    store: me.store,
		    rootVisible: false,
		     selModel: {
                    mode: me.multiSelect ? 'SIMPLE' : 'SINGLE'
                },
		    floating: true,
            hidden: true,
            focusOnToFront: false
		});
		//注册事件用于选择用户选择的值
		me.mon(picker, {
            itemclick: me.onItemClick,
            refresh: me.onListRefresh,
            scope: me
        });
        
        me.mon(picker.getSelectionModel(), {
            beforeselect: me.onBeforeSelect,
            beforedeselect: me.onBeforeDeselect,
            selectionchange: me.onListSelectionChange,
            scope: me
        });
		this.picker = picker;
		return picker;
	},
	
	onItemClick: function(picker, record){
        /*
         * If we're doing single selection, the selection change events won't fire when
         * clicking on the selected element. Detect it here.
         */
        var me = this,
            selection = me.picker.getSelectionModel().getSelection(),
            valueField = me.valueField;

        if (!me.multiSelect && selection.length) {
            if (record.get(valueField) === selection[0].get(valueField)) {
                // Make sure we also update the display value if it's only partial
                me.displayTplData = [record.data];
                me.setRawValue(me.getDisplayValue());
                me.collapse();
            }
        }
    }
  });
  //结束

  Ext.reg('wgTreeCombox',wg.comp.TreeComboBox);
  

 wg.comp.AutoMaxPanel = Ext.extend(Ext.Panel, {
    onRender :function(ct, position) {    	
    	var table=document.createElement("TABLE");
    	table.height="100%";
    	table.width ="100%";
    	var tbody= document.createElement("Tbody");
    	var tr = document.createElement("TR");
    	var td = document.createElement("TD");
    	table.appendChild(tbody);
    	tbody.appendChild(tr);
    	tr.appendChild(td);
    	var el = Ext.get(table);
    	ct.dom.insertBefore(el.dom, position);
    	
        ct=Ext.get(tr);
        this.el=Ext.get(td);        
        wg.comp.AutoMaxPanel.superclass.onRender.call(this,ct,position); 
        
        this.el.setHeight = Ext.emptyFn;
        this.el.setWidth = Ext.emptyFn;
        this.el.setSize = Ext.emptyFn;
        this.el.dom.scroll = 'no';
        this.allowDomMove = false;
        //this.autoWidth = true;
        //this.autoHeight = true;
        Ext.EventManager.onWindowResize(this.fireResize, this);
      
    },
    fireResize : function(w, h){
        this.fireEvent('resize', this, w, h, w, h);
    }
});   

Ext.reg('wgAutoMaxPanel',wg.comp.AutoMaxPanel);
   

 /**
    * 判断为空
    * @param {} obj
    * @return {Boolean}
    */
  function isEmpty(obj){
   	 switch (typeof obj){ 
		case 'undefined' : return true; break;
		case 'string' : if(trim(obj).length == 0) return true; break; 
		case 'boolean' : if(!obj) return true; break; 
		case 'number' : if(0 ==obj) return true; break; 
		case 'object' : 
		  if(null == obj) return true; 
		  if(undefined != obj.length && obj.length==0) return true; 
		  for(var k in obj){return false;} return true; 
		  break; 
		} 
		return false; 
   }

    /**
     * 异步调用提示
     * @param {} obj
     * @return {Boolean}
     */
  function ajaxExeAlert(responseText){
   	 var resultArray = Ext.util.JSON.decode(responseText);   	
   	 if(resultArray.success){
   	 	return true;
   	 }else{  
		 Ext.Msg.alert('提示', resultArray.msg);
		 return false;
   	 }
   }
   
  /**
   * 空格去除
   * @param {} str
   * @return {}
   */
  function trim(str){
    return str.replace(/(^\s*)|(\s*$)/g,"");
  }
   
    