/**
 * 参数管理
 * 
 * @author yangzhangheng
 * @since 2010-02-13
 */
Ext.onReady(function() {
	var wgGridComponent=[];
	var layout;
	var statements=queryStatement.split("|");
	var wgLayout = pageLayout.substr(0,1);
	var wgType = pageLayout.substr(1,1);
	var interLock=parseInt(interLocking);
    var wgTriggernstance;

	if(isEmpty(wgType)){
		wgType="1";
	}
	if (wgLayout=="1"){//只是网格
		var  wgGrid= new WgGrid(statements[0]);
		wgTriggernstance=wgGrid;
	    var  wgGridCreate = wgGrid.wgCreateGrid();	   
	    wgGridComponent.push(wgGridCreate);	  
	}else if (wgLayout=="2"){
		//树加表格
		if(wgType=="1"){
			var wgTreeComp = new WgTree(statements[0]);
			wgTriggernstance=wgTreeComp;
			var wgTree = wgTreeComp.wgGetMenuTree();
			var westWgCom={};
			westWgCom.region='west';
			westWgCom.xtype='panel';
			westWgCom.margins="-3 -1 -3 0"; 
			westWgCom.split=true; 
			westWgCom.layout="fit";
			westWgCom.width=200; 
			westWgCom.minWidth=100; 
			westWgCom.items=wgTree; 
			
			var centerGrid = new WgGrid(statements[1]);
			centerGrid.wgOutQueryKeyValue("");
			centerGrid.wgInterLocking("1");	
			var  wgCenterGrid =centerGrid.wgCreateGrid();
			wgCenterGrid.margins="-10 -8 -8 -8";
			wgTreeComp.wgCenterGrid(centerGrid);
	        var centerWgCom= {};
	        centerWgCom.region="center"; 
		    centerWgCom.xtype="panel"; 	
		    centerWgCom.layout='fit'; 
		    centerWgCom.margins="-3 0 -3 0";				    
		    centerWgCom.items=wgCenterGrid; 
		    
		    wgGridComponent.push(westWgCom);
		    wgGridComponent.push(centerWgCom);	
		}else if(wgType=="2"){//两个表格
			var northGrid = new WgGrid(statements[0]);	
			wgTriggernstance=northGrid;
			northGrid.titleFlag=true;
			northGrid.wgInterLocking("1");
			var  wgNorthGrid =northGrid.wgCreateGrid();
			wgNorthGrid.width=northGrid.wgGetPageModel().wgPageWidth();		
			wgNorthGrid.margins="-10 -8 -8 -8";
			
			var northWgCom={};
			northWgCom.region="west"; 
		    northWgCom.margins="-3 -1 -3 0";
			northWgCom.layout="fit";
		    northWgCom.xtype="panel";
			northWgCom.split=true; 
			northWgCom.width=northGrid.wgGetPageModel().wgPageWidth();					
			northWgCom.items=wgNorthGrid;	
		
			
			var centerGrid = new WgGrid(statements[1]);
			centerGrid.titleFlag=true;
			centerGrid.wgOutQueryKeyValue("");
			centerGrid.wgInterLocking("0");
			var  wgCenterGrid =centerGrid.wgCreateGrid();
			wgCenterGrid.margins="-10 -8 -8 -8";
			northGrid.wgCenterGrid(centerGrid);
	        var centerWgCom= {};
	        centerWgCom.region="center"; 
		    centerWgCom.xtype="panel"; 	
		    centerWgCom.layout='fit'; 
		    centerWgCom.margins="-3 0 -3 0";
		    centerWgCom.items=wgCenterGrid;          
			
			
		    wgGridComponent.push(northWgCom);
		    wgGridComponent.push(centerWgCom);	
		}
	}else if (wgLayout=="3"){
	   if(wgType=="1"){
			var wgTreeComp = new WgTree(statements[0]);
			var wgTree = wgTreeComp.wgGetMenuTree();
			wgTriggernstance=wgTreeComp;
			var westWgCom={};
			westWgCom.region='west';
			westWgCom.xtype='panel';
			westWgCom.id='westPanel';
			westWgCom.margins="-3 -1 -3 0"; 
			westWgCom.split=true; 
			westWgCom.layout="fit";
			westWgCom.width=200; 
			westWgCom.minWidth=100; 
			westWgCom.items=wgTree; 
			
			var northGrid = new WgGrid(statements[1]);
			northGrid.titleFlag=true;
			northGrid.wgOutQueryKeyValue("");
			northGrid.wgInterLocking("1");
			var  wgNorthGrid =northGrid.wgCreateGrid();
			wgNorthGrid.height="50%";
			wgNorthGrid.margins="-10 -8 -8 -8";
			wgTreeComp.wgCenterGrid(northGrid);
			var northWgCom={};
			northWgCom.region="north"; 
		    northWgCom.margins="-3 -1 -3 0";
			northWgCom.layout="fit";
		    northWgCom.xtype="panel";
		    northWgCom.id='northPanel';
			northWgCom.split=true; 
			northWgCom.height=240; 
			northWgCom.items=wgNorthGrid;	
			northWgCom.title=northGrid.wgGetPageModel().wgFunctionDesc();
		    northWgCom.collapsible=true;  //panel可收缩  
		    northWgCom.maximizable=true;  
		    var tools = [{
					id : 'maximize',
					handler : function(e, target, panel) {	
					  panelReSize(e, target, panel);		 
					}
				}];
			northWgCom.tools= tools;
			
			
			var centerGrid = new WgGrid(statements[2]);
			centerGrid.titleFlag=true;
			centerGrid.wgOutQueryKeyValue("");
			var  wgCenterGrid =centerGrid.wgCreateGrid();
			wgCenterGrid.margins="-10 -8 -8 -8";			
			if(interLock==1){
			   wgTreeComp.wgCenterGrid2(centerGrid);
			   northGrid.wgCenterGrid(centerGrid);
			   northGrid.wgInterLocking("0");
			}else{
			   northGrid.wgCenterGrid(centerGrid);
			}
			//建立两者之间互动
			centerGrid.wgCenterGrid(northGrid);
			
	        var centerWgCom= {};
	        centerWgCom.region="center"; 
		    centerWgCom.xtype="panel"; 
		    centerWgCom.id="centerPanel";
		    centerWgCom.layout='fit'; 
		    centerWgCom.margins="-3 0 -3 0";
		    centerWgCom.items=wgCenterGrid;               
	        centerWgCom.title=centerGrid.wgGetPageModel().wgFunctionDesc();   		
		     var tools = [{
					id : 'maximize',
					handler : function(e, target, panel) {	
						panelReSize(e, target, panel);
					}
				}];
			centerWgCom.tools= tools;		
			centerGrid.wgCenterGrid(northGrid);
			centerGrid.wgInterLocking("0");
			
			var wgCenterComb ={};
			wgCenterComb.region="center";
			wgCenterComb.layout="border";
			wgCenterComb.margins="-3 -1 -3 0";
			wgCenterComb.items=[northWgCom,centerWgCom];
	
			wgGridComponent.push(westWgCom);
			wgGridComponent.push(wgCenterComb);		
	   }else if(wgType=="2"){//三个表格
	  	
	   }else if(wgType=="3"){
	  	    var wgTreeComp = new WgTree(statements[0]);
		 	var wgTree = wgTreeComp.wgGetMenuTree();
			wgTriggernstance=wgTreeComp;
			var westWgCom={};
			westWgCom.region='west';
			westWgCom.xtype='panel';
			westWgCom.id='westPanel';
			westWgCom.margins="-3 -1 -3 0"; 
			westWgCom.split=true; 
			westWgCom.layout="fit";
			westWgCom.width=200; 
			westWgCom.minWidth=100; 
			westWgCom.items=wgTree; 			
			
			var centerGrid = new WgGrid(statements[1]);
			wgTreeComp.wgCenterGrid(centerGrid);
			centerGrid.titleFlag=true;
			centerGrid.wgOutQueryKeyValue("");
			var  wgCenterGrid =centerGrid.wgCreateGrid();
			wgCenterGrid.margins="-10 -8 -8 -8";			
	        var centerWgCom= {};
	        centerWgCom.region="center"; 
		    centerWgCom.xtype="panel"; 
		    centerWgCom.id="centerPanel";
		    centerWgCom.layout='fit'; 
		    centerWgCom.margins="-3 0 -3 0";
		    centerWgCom.items=wgCenterGrid;               
	        centerWgCom.title=centerGrid.wgGetPageModel().wgFunctionDesc();   		
		     var tools = [{
					id : 'maximize',
					handler : function(e, target, panel) {	
						panelReSize(e, target, panel);
					}
				}];
			centerWgCom.tools= tools;
	
			
			var eastGrid = new WgGrid(statements[2]);
			if(interLock==1){
			   wgTreeComp.wgCenterGrid2(eastGrid);
			   centerGrid.wgCenterGrid(eastGrid);
			   centerGrid.wgInterLocking("0");
			}else{
			   centerGrids.wgCenterGrid(eastGrid);
			}
						
			eastGrid.titleFlag=true;
			eastGrid.wgOutQueryKeyValue("");
			var  wgEastGrid =eastGrid.wgCreateGrid();
			wgEastGrid.margins="-10 -8 -8 -8";			
			var eastWgCom= {};
			eastWgCom.region="east"; 
		    eastWgCom.split=true; 
			eastWgCom.width=(document.body.clientWidth -200 )/2 ; 
			eastWgCom.minWidth=150;
			eastWgCom.layout="fit";
			eastWgCom.xtype="panel";
			eastWgCom.id="eastPanel";
			eastWgCom.margins="-3 -1 -3 0";
			eastWgCom.items=wgEastGrid;   
			eastWgCom.title=eastGrid.wgGetPageModel().wgFunctionDesc();		
		    eastWgCom.collapsible=true;  //panel可收缩  
		
		     var tools = [{
					id : 'maximize',
					handler : function(e, target, panel) {		
						panelReSize(e, target, panel);
					}
				}];
			eastWgCom.tools= tools;
			//建立两者之间互动		
			eastGrid.wgCenterGrid(centerGrid);
			eastGrid.wgInterLocking("0");
			
			var wgCenterComb ={};
			wgCenterComb.region="center";
			wgCenterComb.layout="border";
			wgCenterComb.margins="-3 -1 -3 0";
			wgCenterComb.items=[centerWgCom,eastWgCom];
	
			wgGridComponent.push(westWgCom);
			wgGridComponent.push(wgCenterComb);		
	  }
	}else if (wgLayout=="4"){	
		var wgTreeComp = new WgTree(statements[0]);
		var wgTree = wgTreeComp.wgGetMenuTree();
		wgTriggernstance=wgTreeComp;
		var westWgCom={};
		westWgCom.region='west';
		westWgCom.xtype='panel';
		westWgCom.id='westPanel';
		westWgCom.margins="-3 -1 -3 0"; 
		westWgCom.split=true; 
		westWgCom.layout="fit";
		westWgCom.width=200; 
		westWgCom.minWidth=100; 
		westWgCom.items=wgTree; 
	
		
		var northGrid = new WgGrid(statements[1]);
		northGrid.titleFlag=true;
		northGrid.wgOutQueryKeyValue("");
		northGrid.wgInterLocking("1");
		var  wgNorthGrid =northGrid.wgCreateGrid();
		wgNorthGrid.height="50%";
		wgNorthGrid.margins="-10 -8 -8 -8";
		wgTreeComp.wgCenterGrid(northGrid);
		var northWgCom={};
		northWgCom.region="north"; 
	    northWgCom.margins="-3 -1 -3 0";
		northWgCom.layout="fit";
	    northWgCom.xtype="panel";
	    northWgCom.id='northPanel';
		northWgCom.split=true; 
		northWgCom.height=document.body.clientHeight/2; 
		northWgCom.items=wgNorthGrid;	
		northWgCom.title=northGrid.wgGetPageModel().wgFunctionDesc();
	    northWgCom.collapsible=true;  //panel可收缩  
	    northWgCom.maximizable=true;  
	    var tools = [{
				id : 'maximize',
			    qtip : 'Maximize'  ,
				handler : function(e, target, panel) {	
				  panelReSize(e, target, panel);		 
				}
			}];
		northWgCom.tools= tools;
		
		var centerGrid = new WgGrid(statements[2]);
		centerGrid.titleFlag=true;
		centerGrid.wgOutQueryKeyValue("");
		var  wgCenterGrid =centerGrid.wgCreateGrid();
		wgCenterGrid.margins="-10 -8 -8 -8";
		northGrid.wgCenterGrid(centerGrid);
        var centerWgCom= {};
        centerWgCom.region="center"; 
	    centerWgCom.xtype="panel"; 
	    centerWgCom.id="centerPanel";
	    centerWgCom.layout='fit'; 
	    centerWgCom.margins="-3 0 -3 0";
	    centerWgCom.items=wgCenterGrid;               
        centerWgCom.title=centerGrid.wgGetPageModel().wgFunctionDesc();   		
	     var tools = [{
				id : 'maximize',
				qtip : 'Maximize'  ,
				handler : function(e, target, panel) {	
					panelReSize(e, target, panel);
				}
			}];
		centerWgCom.tools= tools;

		
		var eastGrid = new WgGrid(statements[3]);
		eastGrid.titleFlag=true;
		eastGrid.wgOutQueryKeyValue("");
		var  wgEastGrid =eastGrid.wgCreateGrid();
		wgEastGrid.margins="-10 -8 -8 -8";
		northGrid.wgEastGrid(eastGrid);
		var eastWgCom= {};
		eastWgCom.region="east"; 
	    eastWgCom.split=true; 
		eastWgCom.width=(document.body.clientWidth -200 )/2 ; 
		eastWgCom.minWidth=150;
		eastWgCom.layout="fit";
		eastWgCom.xtype="panel";
		eastWgCom.id="eastPanel";
		eastWgCom.margins="-3 -1 -3 0";
		eastWgCom.items=wgEastGrid;   
		eastWgCom.title=eastGrid.wgGetPageModel().wgFunctionDesc();		
	    eastWgCom.collapsible=true;  //panel可收缩  
	
	     var tools = [{
				id : 'maximize',
				qtip : 'Maximize'  ,
				handler : function(e, target, panel) {		
					panelReSize(e, target, panel);
				}
			}];
		eastWgCom.tools= tools;
		//建立两者之间互动
		centerGrid.wgCenterGrid(eastGrid);
		centerGrid.wgInterLocking("0");
		eastGrid.wgCenterGrid(centerGrid);
		centerGrid.wgInterLocking("0");
		
		var wgCenterComb ={};
		wgCenterComb.region="center";
		wgCenterComb.layout="border";
		wgCenterComb.margins="-3 -1 -3 0";
		wgCenterComb.items=[northWgCom,centerWgCom,eastWgCom];

		wgGridComponent.push(westWgCom);
		wgGridComponent.push(wgCenterComb);		
	}
	
		
	var  viewport = new Ext.Viewport({
		        id:"wgViewport",
				layout : "border",
				items : wgGridComponent
			});
			
   /**
     * 增加Resize事件
     */
     viewport.addListener('resize',function(viewport, adjWidth, adjHeight, rawWidth, rawHeight ){     	
    	wgResize(viewport, adjWidth, adjHeight, rawWidth, rawHeight);
     },false  );
     
  
  //初始化数据
   if(wgType=="1" || wgType=="3"){   	  
	   var centerGrid = wgTriggernstance.getCenterGrid();
	   centerGrid.wgOutQueryKeyValue('all');
	   centerGrid.refresh();	
	   centerGrid.setButtonDisabled(true);	   
	   var centerGrid2 = wgTriggernstance.getCenterGrid2();
	   if(!isEmpty(centerGrid2)){
		   centerGrid2.wgOutQueryKeyValue('all');
		   centerGrid2.refresh();	
		   centerGrid2.setButtonDisabled(true);	   
	   }
   }else{
   	   wgTriggernstance.refresh();	
   }
 

      
     var centerPanel=Ext.getCmp('centerPanel');
	 var eastPanel=Ext.getCmp('eastPanel');
	 var northPanel = Ext.getCmp('northPanel');
	 var westPanel = Ext.getCmp('westPanel');
	 var maxHeight=document.body.clientHeight;
     if(isEmpty(eastPanel))
	    var MaxWidth= document.body.clientWidth;
	 else{
	 	var MaxWidth= document.body.clientWidth - 200;	 	
	 }
     if(!isEmpty(northPanel)){
	   var northPosition= northPanel.getPosition(true);
	   var northSize  = northPanel.getSize();

    }
	if(!isEmpty(centerPanel)){
		var centerPostion = centerPanel.getPosition(true);		
		var centerSize  = centerPanel.getSize();

	}
    if(!isEmpty(eastPanel)){
	    var eastPostion =  eastPanel.getPosition(true);
		var eastSize  = eastPanel.getSize();

    }
    
	//增加一个Resize 事件
    function wgResize(viewport, adjWidth, adjHeight, rawWidth, rawHeight){     
    	if(!isEmpty(northPanel)){
	       var northPosition = northPanel.getPosition(true);		
		   var northSize  = northPanel.getSize();
		}
		if(!isEmpty(centerPanel)){
			var centerPostion = centerPanel.getPosition(true);		
	    	var centerSize  = centerPanel.getSize();
		}
	    if(!isEmpty(eastPanel)){
		    var eastPostion =  eastPanel.getPosition(true);
		    var eastSize  = eastPanel.getSize();
	    }
	    maxHeight=rawHeight;
	    if(isEmpty(eastPanel))
		    MaxWidth=rawWidth;
		 else{
		 	MaxWidth= rawWidth -200;	 
		 }
	    
    }
	//begin
	function panelReSize(e, target, panel){
	    if(panel.id=="northPanel"){		    	
			if(northPanel.getHeight()!=maxHeight||northPanel.getWidth()!=MaxWidth){	
				
				if(!isEmpty(centerPanel)){
				  centerPanel.setVisible(false);	
				}
				if(!isEmpty(eastPanel)){
				    eastPanel.setVisible(false);	   
				    northPanel.setSize(MaxWidth,maxHeight);	
				}
			}else{	
				
			    northPanel.setSize(northSize);
			    if(!isEmpty(centerPanel)){
			    	centerPanel.setVisible(true);	
			    }
			    if(!isEmpty(eastPanel)){
			      eastPanel.setVisible(true);	
			    }
			}
		}
		if(panel.id=="centerPanel"){
			if(centerPanel.getHeight()!=maxHeight||centerPanel.getWidth()!=MaxWidth ){	
				if(!isEmpty(eastPanel)){
			   	  eastPanel.setVisible(false);	
				}
				if(!isEmpty(northPanel)){
			      northPanel.setVisible(false);	
				}
			    centerPanel.setPosition(0,0);
			    centerPanel.setSize(MaxWidth,maxHeight);	
			}else{	
				centerPanel.setPosition(centerPostion);
		        centerPanel.setSize(centerSize);
		        if(!isEmpty(northPanel)){
				  northPanel.setVisible(true);
		        }
		        if(!isEmpty(eastPanel)){
			      eastPanel.setVisible(true);   
		        }
			}
		}
	
		if(panel.id=="eastPanel"){			
			if(eastPanel.getHeight()!=maxHeight||eastPanel.getWidth()!=MaxWidth  ){		
				if(!isEmpty(centerPanel)){
			    	centerPanel.setVisible(false);		
				}
				if(!isEmpty(northPanel)){
			      northPanel.setVisible(false);	
				}
			    eastPanel.setPosition(0,0);
			    eastPanel.setSize(MaxWidth,maxHeight);	
			}else{		
				eastPanel.setPosition(eastPostion);
		        eastPanel.setSize(eastSize);
		        if(!isEmpty(northPanel)){
				  northPanel.setVisible(true);	
		        }
		        if(!isEmpty(centerPanel)){
			      centerPanel.setVisible(true);    
		        }
			}
		}
   }	
   //end 
});


