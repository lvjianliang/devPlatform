------------------------------------------------------
-- Export file for user DEV                         --
-- Created by Administrator on 2015-10-28, 12:58:19 --
------------------------------------------------------

set define off
spool 02oracleSql.log

prompt
prompt Creating table EACODE
prompt =====================
prompt
create table DEV.EACODE
(
  field     VARCHAR2(15) not null,
  fieldname VARCHAR2(20) not null,
  code      VARCHAR2(10) not null,
  codedesc  VARCHAR2(100) not null,
  enabled   VARCHAR2(2) default '1' not null,
  editmode  VARCHAR2(2) default '1' not null,
  sortno    NUMBER,
  remark    VARCHAR2(200),
  codeid    VARCHAR2(8) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EACODE
  is '代码对照表';
comment on column DEV.EACODE.field
  is '对照字段';
comment on column DEV.EACODE.fieldname
  is '对照字段名称';
comment on column DEV.EACODE.code
  is '代码';
comment on column DEV.EACODE.codedesc
  is '代码描述';
comment on column DEV.EACODE.enabled
  is '启用状态(0:禁用;1:启用)';
comment on column DEV.EACODE.editmode
  is '编辑模式(0:只读;1:可编辑)';
comment on column DEV.EACODE.sortno
  is '排序号';
comment on column DEV.EACODE.remark
  is '备注';
comment on column DEV.EACODE.codeid
  is '对照ID';
create unique index DEV.INDEX_EACODE on DEV.EACODE (FIELD, CODE)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EACODE
  add constraint PK_EACODE primary key (CODEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EADEPT
prompt =====================
prompt
create table DEV.EADEPT
(
  deptid   VARCHAR2(100) not null,
  deptname VARCHAR2(50) not null,
  parentid VARCHAR2(100) not null,
  customid VARCHAR2(20),
  sortno   NUMBER(4),
  leaf     VARCHAR2(2) default '0' not null,
  remark   VARCHAR2(100),
  enabled  NVARCHAR2(2) default '1' not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EADEPT
  is '部门信息表';
comment on column DEV.EADEPT.deptid
  is '部门编号';
comment on column DEV.EADEPT.deptname
  is '部门名称';
comment on column DEV.EADEPT.parentid
  is '上级部门编号';
comment on column DEV.EADEPT.customid
  is '自定义部门编号';
comment on column DEV.EADEPT.sortno
  is '排序号';
comment on column DEV.EADEPT.leaf
  is '叶子节点(0:树枝节点;1:叶子节点)';
comment on column DEV.EADEPT.remark
  is '备注';
comment on column DEV.EADEPT.enabled
  is '启用状态(1:启用;0:停用)';
alter table DEV.EADEPT
  add constraint PK_EADEPT primary key (DEPTID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAEVENT
prompt ======================
prompt
create table DEV.EAEVENT
(
  eventid     VARCHAR2(20) not null,
  userid      VARCHAR2(8),
  account     VARCHAR2(30),
  username    VARCHAR2(20),
  description VARCHAR2(100),
  activetime  NUMBER(14),
  requestpath VARCHAR2(200),
  methodname  VARCHAR2(50),
  costtime    NUMBER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAEVENT
  is '操作员事件表';
comment on column DEV.EAEVENT.eventid
  is '事件ID';
comment on column DEV.EAEVENT.userid
  is '用户ID';
comment on column DEV.EAEVENT.account
  is '登录账号';
comment on column DEV.EAEVENT.username
  is '用户名';
comment on column DEV.EAEVENT.description
  is '事件描述';
comment on column DEV.EAEVENT.activetime
  is '活动时间';
comment on column DEV.EAEVENT.requestpath
  is '请求路劲';
comment on column DEV.EAEVENT.methodname
  is 'Action方法名';
comment on column DEV.EAEVENT.costtime
  is '耗时';
alter table DEV.EAEVENT
  add constraint PK_EAEVENT primary key (EVENTID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAEXCEPTION
prompt ==========================
prompt
create table DEV.EAEXCEPTION
(
  exceptionid VARCHAR2(20) not null,
  clazz       VARCHAR2(200),
  methodname  VARCHAR2(50),
  exception   VARCHAR2(2000),
  activetime  NUMBER(14)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAEXCEPTION
  is '系统异常监控表';
comment on column DEV.EAEXCEPTION.exceptionid
  is '异常ID';
comment on column DEV.EAEXCEPTION.clazz
  is '异常类';
comment on column DEV.EAEXCEPTION.methodname
  is '异常方法';
comment on column DEV.EAEXCEPTION.exception
  is '异常信息';
comment on column DEV.EAEXCEPTION.activetime
  is '激活时间';
alter table DEV.EAEXCEPTION
  add constraint PK_EABEANMONITOR primary key (EXCEPTIONID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAHELP
prompt =====================
prompt
create table DEV.EAHELP
(
  helpid  VARCHAR2(8) not null,
  menuid  VARCHAR2(8) not null,
  content VARCHAR2(4000)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAHELP
  is '帮助信息表';
comment on column DEV.EAHELP.helpid
  is '功能模块帮助编号
';
comment on column DEV.EAHELP.menuid
  is '菜单编号';
comment on column DEV.EAHELP.content
  is '帮助内容';
create unique index DEV.INDEX_MENUID on DEV.EAHELP (MENUID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAHELP
  add constraint PK_EAHELP primary key (HELPID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAHTTPSESSION
prompt ============================
prompt
create table DEV.EAHTTPSESSION
(
  sessionid  VARCHAR2(100) not null,
  username   VARCHAR2(20),
  account    VARCHAR2(30),
  createtime VARCHAR2(20),
  loginip    VARCHAR2(15),
  userid     VARCHAR2(8),
  explorer   VARCHAR2(15)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAHTTPSESSION
  is 'HTTP会话资源管理';
comment on column DEV.EAHTTPSESSION.sessionid
  is '会话ID';
comment on column DEV.EAHTTPSESSION.username
  is '用户名';
comment on column DEV.EAHTTPSESSION.account
  is '登录账户';
comment on column DEV.EAHTTPSESSION.createtime
  is '会话创建时间';
comment on column DEV.EAHTTPSESSION.loginip
  is '登录IP';
comment on column DEV.EAHTTPSESSION.userid
  is '用户编号';
comment on column DEV.EAHTTPSESSION.explorer
  is '客户端浏览器';
alter table DEV.EAHTTPSESSION
  add constraint PK_EAHTTPSESSION primary key (SESSIONID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAICON
prompt =====================
prompt
create table DEV.EAICON
(
  iconid   VARCHAR2(8) not null,
  filename VARCHAR2(50) not null,
  cssname  VARCHAR2(50) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAICON
  is '系统图标表';
comment on column DEV.EAICON.iconid
  is '图标编号';
comment on column DEV.EAICON.filename
  is '文件名';
comment on column DEV.EAICON.cssname
  is 'CSS类名';
alter table DEV.EAICON
  add constraint PK_EAICON primary key (ICONID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAICON
  add constraint UK1_EAICON unique (FILENAME)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAICON
  add constraint UK2_EAICON unique (CSSNAME)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAJDBCMONITOR
prompt ============================
prompt
create table DEV.EAJDBCMONITOR
(
  monitorid  NUMBER(20) not null,
  sqltext    VARCHAR2(2000),
  starttime  NUMBER(14),
  costtime   NUMBER(8),
  effectrows NUMBER(8),
  type       VARCHAR2(1)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAJDBCMONITOR
  is 'JDBC监控信息表';
comment on column DEV.EAJDBCMONITOR.monitorid
  is '监控ID';
comment on column DEV.EAJDBCMONITOR.sqltext
  is 'SQL语句';
comment on column DEV.EAJDBCMONITOR.starttime
  is '执行开始时间';
comment on column DEV.EAJDBCMONITOR.costtime
  is '执行总耗时';
comment on column DEV.EAJDBCMONITOR.effectrows
  is '影响行数';
comment on column DEV.EAJDBCMONITOR.type
  is '数据操作类型(1:INSERT;2:UPDATE;3:DELETE;4:SELECT;5:CALLPRC)';
alter table DEV.EAJDBCMONITOR
  add primary key (MONITORID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAMENU
prompt =====================
prompt
create table DEV.EAMENU
(
  menuid    VARCHAR2(60) not null,
  menuname  VARCHAR2(50) not null,
  parentid  VARCHAR2(60) not null,
  iconcls   VARCHAR2(50),
  expanded  VARCHAR2(2) default '0',
  request   VARCHAR2(200),
  leaf      VARCHAR2(2) default '0' not null,
  sortno    NUMBER(4),
  remark    VARCHAR2(200),
  icon      VARCHAR2(50),
  menutype  VARCHAR2(2) default '0',
  shortcut  VARCHAR2(50),
  width     NUMBER(4),
  height    NUMBER(4),
  scrollbar VARCHAR2(2) default '0'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAMENU
  is '菜单资源信息表';
comment on column DEV.EAMENU.menuid
  is '菜单编号';
comment on column DEV.EAMENU.menuname
  is '菜单名称';
comment on column DEV.EAMENU.parentid
  is '上级菜单编号';
comment on column DEV.EAMENU.iconcls
  is '节点图标CSS类名';
comment on column DEV.EAMENU.expanded
  is '展开状态(1:展开;0:收缩)';
comment on column DEV.EAMENU.request
  is '请求地址';
comment on column DEV.EAMENU.leaf
  is '叶子节点(0:树枝节点;1:叶子节点)';
comment on column DEV.EAMENU.sortno
  is '排序号';
comment on column DEV.EAMENU.remark
  is '备注';
comment on column DEV.EAMENU.icon
  is '节点图标';
comment on column DEV.EAMENU.menutype
  is '菜单类型(1:系统菜单;0:业务菜单)';
comment on column DEV.EAMENU.shortcut
  is '桌面图标文件';
comment on column DEV.EAMENU.width
  is '桌面窗口宽度';
comment on column DEV.EAMENU.height
  is '桌面窗口高度';
comment on column DEV.EAMENU.scrollbar
  is '桌面窗口滚动条(1：有；0：无)';
alter table DEV.EAMENU
  add constraint PK_EAMENU primary key (MENUID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAMENUPART
prompt =========================
prompt
create table DEV.EAMENUPART
(
  partid  VARCHAR2(8) not null,
  menuid  VARCHAR2(60) not null,
  cmpid   VARCHAR2(20) not null,
  cmptype VARCHAR2(2) not null,
  remark  VARCHAR2(100) default ''
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAMENUPART
  is 'UI托管组件';
comment on column DEV.EAMENUPART.partid
  is 'UI组件编号';
comment on column DEV.EAMENUPART.menuid
  is '菜单ID';
comment on column DEV.EAMENUPART.cmpid
  is 'UI组件ID';
comment on column DEV.EAMENUPART.cmptype
  is 'UI组件类型(1:按钮组件;2:表单输入组件;3:容器面板组件)';
comment on column DEV.EAMENUPART.remark
  is '备注';
alter table DEV.EAMENUPART
  add constraint PK_EAMENUPART primary key (PARTID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAPARAM
prompt ======================
prompt
create table DEV.EAPARAM
(
  paramid    VARCHAR2(8) not null,
  paramkey   VARCHAR2(50) not null,
  paramvalue VARCHAR2(1000) not null,
  remark     VARCHAR2(200)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAPARAM
  is '全局参数表';
comment on column DEV.EAPARAM.paramid
  is '参数编号';
comment on column DEV.EAPARAM.paramkey
  is '参数键名';
comment on column DEV.EAPARAM.paramvalue
  is '参数键值';
comment on column DEV.EAPARAM.remark
  is '备注';
alter table DEV.EAPARAM
  add constraint PK_EAPARAM primary key (PARAMID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAPARAM
  add constraint UK_EAPARAM unique (PARAMKEY)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAROLE
prompt =====================
prompt
create table DEV.EAROLE
(
  roleid   VARCHAR2(8) not null,
  rolename VARCHAR2(50) not null,
  deptid   VARCHAR2(100) not null,
  roletype VARCHAR2(2) default '1' not null,
  remark   VARCHAR2(2),
  locked   VARCHAR2(2) default '0'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAROLE
  is '角色信息表';
comment on column DEV.EAROLE.roleid
  is '角色编号';
comment on column DEV.EAROLE.rolename
  is '角色名称';
comment on column DEV.EAROLE.deptid
  is '所属部门编号';
comment on column DEV.EAROLE.roletype
  is '角色类型(1:业务角色;2:管理角色 ;3:系统内置角色)';
comment on column DEV.EAROLE.remark
  is '备注';
comment on column DEV.EAROLE.locked
  is '锁定标志(1:锁定;0:激活)';
alter table DEV.EAROLE
  add constraint PK_EAROLE primary key (ROLEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAROLEAUTHORIZE
prompt ==============================
prompt
create table DEV.EAROLEAUTHORIZE
(
  roleid         VARCHAR2(8) not null,
  menuid         VARCHAR2(60) not null,
  authorizelevel VARCHAR2(2) default '1' not null,
  authorizeid    VARCHAR2(8) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAROLEAUTHORIZE
  is '角色授权表';
comment on column DEV.EAROLEAUTHORIZE.roleid
  is '角色编号';
comment on column DEV.EAROLEAUTHORIZE.menuid
  is '菜单编号';
comment on column DEV.EAROLEAUTHORIZE.authorizelevel
  is '权限级别(1:访问权限;2:管理权限)';
comment on column DEV.EAROLEAUTHORIZE.authorizeid
  is '授权ID';
alter table DEV.EAROLEAUTHORIZE
  add constraint PK_EARWAUTHORIZE primary key (AUTHORIZEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAROLEAUTHORIZE
  add constraint UK_EARWAUTHORIZE unique (ROLEID, MENUID, AUTHORIZELEVEL)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAROLEMENUPART
prompt =============================
prompt
create table DEV.EAROLEMENUPART
(
  authorizeid  VARCHAR2(8) not null,
  roleid       VARCHAR2(8) not null,
  menuid       VARCHAR2(60) not null,
  partid       VARCHAR2(8) not null,
  partauthtype VARCHAR2(1) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAROLEMENUPART
  is 'UI元素角色授权表';
comment on column DEV.EAROLEMENUPART.authorizeid
  is '授权ID';
comment on column DEV.EAROLEMENUPART.roleid
  is '角色ID';
comment on column DEV.EAROLEMENUPART.menuid
  is '菜单ID';
comment on column DEV.EAROLEMENUPART.partid
  is 'UI元素ID';
comment on column DEV.EAROLEMENUPART.partauthtype
  is 'UI元素授权类型';
alter table DEV.EAROLEMENUPART
  add constraint PK_EAROLEMENUPART primary key (AUTHORIZEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EASEQUENCE
prompt =========================
prompt
create table DEV.EASEQUENCE
(
  sequenceid VARCHAR2(4) not null,
  fieldname  VARCHAR2(50) not null,
  maxid      VARCHAR2(50) not null,
  remark     VARCHAR2(100),
  pattern    VARCHAR2(50) not null,
  idtype     VARCHAR2(2) default 2 not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EASEQUENCE
  is '序列号生成器';
comment on column DEV.EASEQUENCE.sequenceid
  is '编号';
comment on column DEV.EASEQUENCE.fieldname
  is '字段名';
comment on column DEV.EASEQUENCE.maxid
  is '当前最大值';
comment on column DEV.EASEQUENCE.remark
  is '备注';
comment on column DEV.EASEQUENCE.pattern
  is '样式';
comment on column DEV.EASEQUENCE.idtype
  is 'ID类型(1:系统内置;2:用户自定义)';
create unique index DEV.INDEX_EASEQUENCE on DEV.EASEQUENCE (FIELDNAME)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EASEQUENCE
  add constraint PK_EASEQUENCE primary key (SEQUENCEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAUSER
prompt =====================
prompt
create table DEV.EAUSER
(
  userid   VARCHAR2(8) not null,
  username VARCHAR2(20) not null,
  account  VARCHAR2(30) not null,
  password VARCHAR2(50) not null,
  sex      VARCHAR2(2) default '0' not null,
  deptid   VARCHAR2(100) not null,
  locked   VARCHAR2(2) default '0' not null,
  remark   VARCHAR2(50),
  usertype VARCHAR2(2) default '1',
  enabled  VARCHAR2(2) default '1' not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAUSER
  is '用户信息表';
comment on column DEV.EAUSER.userid
  is '用户编号';
comment on column DEV.EAUSER.username
  is '用户名';
comment on column DEV.EAUSER.account
  is '登陆帐户';
comment on column DEV.EAUSER.password
  is '密码';
comment on column DEV.EAUSER.sex
  is '性别(0:未知;1:男;2:女)';
comment on column DEV.EAUSER.deptid
  is '部门编号';
comment on column DEV.EAUSER.locked
  is '锁定标志(1:锁定;0:激活)';
comment on column DEV.EAUSER.remark
  is '备注';
comment on column DEV.EAUSER.usertype
  is '人员类型(1:经办员;2:管理员;3:系统内置人员;4;项目网站注册用户)';
comment on column DEV.EAUSER.enabled
  is '启用状态(1:启用;0:停用)';
create unique index DEV.UK_EAUSER on DEV.EAUSER (ACCOUNT)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAUSER
  add constraint PK_EAUSER primary key (USERID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAUSERAUTHORIZE
prompt ==============================
prompt
create table DEV.EAUSERAUTHORIZE
(
  userid      VARCHAR2(8) not null,
  roleid      VARCHAR2(8) not null,
  authorizeid VARCHAR2(8) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAUSERAUTHORIZE
  is '用户授权表';
comment on column DEV.EAUSERAUTHORIZE.userid
  is '用户编号';
comment on column DEV.EAUSERAUTHORIZE.roleid
  is '角色编号';
comment on column DEV.EAUSERAUTHORIZE.authorizeid
  is '授权ID';
alter table DEV.EAUSERAUTHORIZE
  add constraint PK_EAUSERAUTHORIZE primary key (AUTHORIZEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAUSERMENUMAP
prompt ============================
prompt
create table DEV.EAUSERMENUMAP
(
  userid         VARCHAR2(8) not null,
  menuid         VARCHAR2(60) not null,
  authorizeid    VARCHAR2(8) not null,
  authorizelevel VARCHAR2(2) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAUSERMENUMAP
  is '用户菜单映射表';
comment on column DEV.EAUSERMENUMAP.userid
  is '人员编号';
comment on column DEV.EAUSERMENUMAP.menuid
  is '菜单编号';
comment on column DEV.EAUSERMENUMAP.authorizeid
  is '授权编号';
comment on column DEV.EAUSERMENUMAP.authorizelevel
  is '权限级别(1:访问权限;2:管理权限)';
alter table DEV.EAUSERMENUMAP
  add constraint PK_EAUSERMENUMAP primary key (AUTHORIZEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.EAUSERMENUMAP
  add constraint UK_EAUSERMENUMAP unique (MENUID, USERID, AUTHORIZELEVEL)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EAUSERSUBINFO
prompt ============================
prompt
create table DEV.EAUSERSUBINFO
(
  userid     VARCHAR2(8) not null,
  theme      VARCHAR2(50),
  layout     VARCHAR2(1),
  background VARCHAR2(50)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EAUSERSUBINFO
  is '人员信息附加属性表';
comment on column DEV.EAUSERSUBINFO.userid
  is '人员编号';
comment on column DEV.EAUSERSUBINFO.theme
  is '自定义主题皮肤';
comment on column DEV.EAUSERSUBINFO.layout
  is '系统级缺省主界面布局模式。1:传统经典布局;2:个性桌面布局。';
comment on column DEV.EAUSERSUBINFO.background
  is '自定义桌面背景';
alter table DEV.EAUSERSUBINFO
  add constraint PK_EAUSERSUBINFO primary key (USERID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EA_DEMO_BYJSB
prompt ============================
prompt
create table DEV.EA_DEMO_BYJSB
(
  sxh  VARCHAR2(20) not null,
  dwmc VARCHAR2(100),
  grbm VARCHAR2(20),
  xm   VARCHAR2(20),
  xb   VARCHAR2(3),
  xnl  NUMBER,
  fyze NUMBER(14,2),
  zfje NUMBER(14,2),
  ybbx NUMBER(14,2),
  jssj DATE,
  yymc VARCHAR2(50),
  jbr  VARCHAR2(20)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EA_DEMO_BYJSB
  is '演示表(病员结算明细)';
alter table DEV.EA_DEMO_BYJSB
  add constraint PK_EZ_BYJSB primary key (SXH)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EA_DEMO_BYMXB
prompt ============================
prompt
create table DEV.EA_DEMO_BYMXB
(
  sxh    VARCHAR2(20) not null,
  xh     VARCHAR2(20) not null,
  zflb   VARCHAR2(10),
  grbm   VARCHAR2(20),
  xm     VARCHAR2(20),
  sfdlmc VARCHAR2(20),
  sfxmmc VARCHAR2(100),
  sl     NUMBER(8,2),
  sjjg   NUMBER(14,3),
  fyze   NUMBER(14,2),
  cd     VARCHAR2(128),
  gg     VARCHAR2(50),
  sfsj   DATE,
  sfy    VARCHAR2(20)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EA_DEMO_BYMXB
  is '演示表(病员费用明细)';
alter table DEV.EA_DEMO_BYMXB
  add constraint PK_EZ_BYMXB primary key (SXH, XH)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table EA_DEMO_CHINAAREA
prompt ================================
prompt
create table DEV.EA_DEMO_CHINAAREA
(
  areacode VARCHAR2(12),
  areaname VARCHAR2(50)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EA_DEMO_CHINAAREA
  is '演示表(中国行政区域)';
comment on column DEV.EA_DEMO_CHINAAREA.areacode
  is '区划代码';
comment on column DEV.EA_DEMO_CHINAAREA.areaname
  is '区划名称';

prompt
prompt Creating table EA_DEMO_FCF
prompt ==========================
prompt
create table DEV.EA_DEMO_FCF
(
  name     VARCHAR2(20),
  value    VARCHAR2(10),
  color    VARCHAR2(10),
  alpha    VARCHAR2(10),
  issliced VARCHAR2(1) default 0,
  product  VARCHAR2(1),
  fcfid    NUMBER(10) not null
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EA_DEMO_FCF
  is '演示表(FlashReport数据源)';
comment on column DEV.EA_DEMO_FCF.name
  is '名称';
comment on column DEV.EA_DEMO_FCF.value
  is '值';
comment on column DEV.EA_DEMO_FCF.color
  is '颜色';
comment on column DEV.EA_DEMO_FCF.alpha
  is '透明度';
comment on column DEV.EA_DEMO_FCF.issliced
  is '是否浮动(仅对饼图有效)';
comment on column DEV.EA_DEMO_FCF.product
  is '产品';

prompt
prompt Creating table EA_DEMO_SFXM
prompt ===========================
prompt
create table DEV.EA_DEMO_SFXM
(
  xmid   VARCHAR2(20) not null,
  sfdlbm VARCHAR2(20),
  xmmc   VARCHAR2(100),
  xmrj   VARCHAR2(20),
  gg     VARCHAR2(50),
  dw     VARCHAR2(20),
  zfbl   NUMBER,
  jx     VARCHAR2(50),
  cd     VARCHAR2(100),
  qybz   VARCHAR2(3),
  yybm   VARCHAR2(8),
  ggsj   DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.EA_DEMO_SFXM
  is '演示表(医院收费项目)';
alter table DEV.EA_DEMO_SFXM
  add constraint PK_EZ_SFXM primary key (XMID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_BASECODEINFO
prompt ===============================
prompt
create table DEV.P_B_BASECODEINFO
(
  basecodetablecode VARCHAR2(32) not null,
  basecodetablename VARCHAR2(50),
  opertime          DATE,
  opercode          VARCHAR2(32),
  codetype          VARCHAR2(1) default 2,
  modifyflag        VARCHAR2(1) default 1,
  metadatacode      VARCHAR2(32),
  usestandard       VARCHAR2(50),
  classtype         VARCHAR2(32),
  pym               VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_BASECODEINFO
  is '公共代码基础信息表';
comment on column DEV.P_B_BASECODEINFO.basecodetablecode
  is '基础代码表编码';
comment on column DEV.P_B_BASECODEINFO.basecodetablename
  is '基础代码表名称';
comment on column DEV.P_B_BASECODEINFO.opertime
  is '操作时间';
comment on column DEV.P_B_BASECODEINFO.opercode
  is '操作人员编码';
comment on column DEV.P_B_BASECODEINFO.codetype
  is '代码类别标志  1系统级 2用户级 3两者都适用';
comment on column DEV.P_B_BASECODEINFO.modifyflag
  is '代码编辑标志 1可以修改,0不可修改';
comment on column DEV.P_B_BASECODEINFO.metadatacode
  is '数据元标识';
comment on column DEV.P_B_BASECODEINFO.usestandard
  is '使用标准';
comment on column DEV.P_B_BASECODEINFO.classtype
  is '所属分类';
comment on column DEV.P_B_BASECODEINFO.pym
  is '拼音码';
alter table DEV.P_B_BASECODEINFO
  add constraint PK_P_B_BASECODEINFO primary key (BASECODETABLECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_CODEVALUE
prompt ============================
prompt
create table DEV.P_B_CODEVALUE
(
  basecodetablecode VARCHAR2(32) not null,
  valuecode         VARCHAR2(32) not null,
  valuename         VARCHAR2(40),
  memo              VARCHAR2(100),
  pym               VARCHAR2(32),
  parentcode        VARCHAR2(32),
  specialtype       VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 192K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_CODEVALUE
  is '公共代码值域表';
comment on column DEV.P_B_CODEVALUE.basecodetablecode
  is '基础代码表编码';
comment on column DEV.P_B_CODEVALUE.valuecode
  is '代码编码';
comment on column DEV.P_B_CODEVALUE.valuename
  is '代码名称';
comment on column DEV.P_B_CODEVALUE.memo
  is '备注';
comment on column DEV.P_B_CODEVALUE.pym
  is '拼音码';
comment on column DEV.P_B_CODEVALUE.parentcode
  is '上级代码';
comment on column DEV.P_B_CODEVALUE.specialtype
  is '特殊类别标识';
alter table DEV.P_B_CODEVALUE
  add constraint PK_P_B_CODEVALUE primary key (BASECODETABLECODE, VALUECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 192K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_B_CODEVALUE
  add constraint FK_P_1 foreign key (BASECODETABLECODE)
  references DEV.P_B_BASECODEINFO (BASECODETABLECODE);

prompt
prompt Creating table P_B_DATASET
prompt ==========================
prompt
create table DEV.P_B_DATASET
(
  datasetcode        VARCHAR2(32) not null,
  datasetname        VARCHAR2(40),
  currentlevel       INTEGER,
  parentdatasetcode  VARCHAR2(32),
  datasetforeignkey  VARCHAR2(32),
  datasetprimaykey   VARCHAR2(32),
  datasetdesc        VARCHAR2(200),
  datasetorder       INTEGER,
  templagteid        VARCHAR2(32),
  datasettype        VARCHAR2(32),
  processflag        VARCHAR2(1) default 0,
  attributivedataset VARCHAR2(32),
  state              VARCHAR2(1),
  oldtemplateid      VARCHAR2(32),
  tablename          VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_DATASET
  is '数据集';
comment on column DEV.P_B_DATASET.datasetcode
  is '数据集编码';
comment on column DEV.P_B_DATASET.datasetname
  is '数据集名称';
comment on column DEV.P_B_DATASET.currentlevel
  is '所处级别';
comment on column DEV.P_B_DATASET.parentdatasetcode
  is '上级数据集编码';
comment on column DEV.P_B_DATASET.datasetforeignkey
  is '数据集外键,此数据集对应上级数据集的主键';
comment on column DEV.P_B_DATASET.datasetprimaykey
  is '数据集的业务主键';
comment on column DEV.P_B_DATASET.datasetdesc
  is '数据集描述(数据集对外总的名称)';
comment on column DEV.P_B_DATASET.datasetorder
  is '数据集顺序号';
comment on column DEV.P_B_DATASET.templagteid
  is '数据集模板编码';
comment on column DEV.P_B_DATASET.datasettype
  is '数据集类别编码';
comment on column DEV.P_B_DATASET.processflag
  is '过程类标识 0不是过程类 1是过程类';
comment on column DEV.P_B_DATASET.attributivedataset
  is '所属数据集';
comment on column DEV.P_B_DATASET.state
  is '数据集状态 I新增 2生成物理表 3生成物理表更新';
comment on column DEV.P_B_DATASET.oldtemplateid
  is '老的模板编码';
comment on column DEV.P_B_DATASET.tablename
  is '物理表名';
create index DEV.IDX_DS_ATTRIBUTIVEDATASET on DEV.P_B_DATASET (ATTRIBUTIVEDATASET)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create index DEV.IDX_DS_PARENTDATASETCODE on DEV.P_B_DATASET (PARENTDATASETCODE)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_B_DATASET
  add constraint PK_P_B_DATASET primary key (DATASETCODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_DATASETTEMPLATE
prompt ==================================
prompt
create table DEV.P_B_DATASETTEMPLATE
(
  templatetype VARCHAR2(32),
  templateid   VARCHAR2(32) not null,
  templatename VARCHAR2(40),
  templatedesc VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_DATASETTEMPLATE
  is '数集模板';
comment on column DEV.P_B_DATASETTEMPLATE.templatetype
  is '模板类别';
comment on column DEV.P_B_DATASETTEMPLATE.templateid
  is '数据集模板编码';
comment on column DEV.P_B_DATASETTEMPLATE.templatename
  is '数据集模板名称';
comment on column DEV.P_B_DATASETTEMPLATE.templatedesc
  is '描述信息';
alter table DEV.P_B_DATASETTEMPLATE
  add constraint PK_P_B_DATASETTEMPLATE primary key (TEMPLATEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_DATASETTEMPLATEDATA
prompt ======================================
prompt
create table DEV.P_B_DATASETTEMPLATEDATA
(
  metadatacode VARCHAR2(32) not null,
  templateid   VARCHAR2(32) not null,
  ordernumber  INTEGER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_DATASETTEMPLATEDATA
  is '数据子集模板与元数据对应表';
comment on column DEV.P_B_DATASETTEMPLATEDATA.metadatacode
  is '数据元编码';
comment on column DEV.P_B_DATASETTEMPLATEDATA.templateid
  is '数据集模板编码';
comment on column DEV.P_B_DATASETTEMPLATEDATA.ordernumber
  is '顺序号';
alter table DEV.P_B_DATASETTEMPLATEDATA
  add constraint PK_T_M primary key (TEMPLATEID, METADATACODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 192K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_DATASETVSMETADATA
prompt ====================================
prompt
create table DEV.P_B_DATASETVSMETADATA
(
  datasetcode   VARCHAR2(32) not null,
  metadatacode  VARCHAR2(32) not null,
  metadataorder INTEGER,
  metadataflag  VARCHAR2(1) default 0
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_DATASETVSMETADATA
  is '数据子集与元数据对应表';
comment on column DEV.P_B_DATASETVSMETADATA.datasetcode
  is '数据集编码';
comment on column DEV.P_B_DATASETVSMETADATA.metadatacode
  is '数据元编码';
comment on column DEV.P_B_DATASETVSMETADATA.metadataorder
  is '数据元顺序号';
comment on column DEV.P_B_DATASETVSMETADATA.metadataflag
  is '0普通字段 1主键 2外键 3普通索引';
create index DEV.IDX_DS_METADATA on DEV.P_B_DATASETVSMETADATA (METADATACODE)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_B_DATASETVSMETADATA
  add constraint PK_P_DATASET primary key (DATASETCODE, METADATACODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_B_DATASETVSMETADATA
  add constraint FK_P_DATASET foreign key (DATASETCODE)
  references DEV.P_B_DATASET (DATASETCODE);

prompt
prompt Creating table P_B_MEMORYSYNCH
prompt ==============================
prompt
create table DEV.P_B_MEMORYSYNCH
(
  beanid   VARCHAR2(32) not null,
  beanname VARCHAR2(50),
  memo     VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_MEMORYSYNCH
  is '内存同步';
comment on column DEV.P_B_MEMORYSYNCH.beanid
  is 'BeanId';
comment on column DEV.P_B_MEMORYSYNCH.beanname
  is 'BeanName';
comment on column DEV.P_B_MEMORYSYNCH.memo
  is '描述';
alter table DEV.P_B_MEMORYSYNCH
  add constraint PK_P_B_MEMOSYNCH primary key (BEANID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_METADATA
prompt ===========================
prompt
create table DEV.P_B_METADATA
(
  metadataclass     VARCHAR2(32),
  metadatacode      VARCHAR2(32) not null,
  metadataname      VARCHAR2(40),
  metadatatype      VARCHAR2(32),
  metadatalength    INTEGER,
  metadataprecision INTEGER,
  valuecodetable    VARCHAR2(32),
  minvalue          FLOAT,
  maxvalue          FLOAT,
  memo              VARCHAR2(200),
  fieldname         VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 256K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_METADATA
  is '数据元表';
comment on column DEV.P_B_METADATA.memo
  is '备注';
comment on column DEV.P_B_METADATA.fieldname
  is '字段名称';
create index DEV.IDX_METADATA_CODE on DEV.P_B_METADATA (VALUECODETABLE)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
create unique index DEV.INDEX_METAFIELD on DEV.P_B_METADATA (FIELDNAME)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_B_METADATA
  add constraint PK_P_B_METADATA primary key (METADATACODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_ONLYCODE
prompt ===========================
prompt
create table DEV.P_B_ONLYCODE
(
  codeid          VARCHAR2(32) not null,
  codename        VARCHAR2(50),
  prefix          VARCHAR2(12),
  suffix          VARCHAR2(12),
  codelength      NUMBER,
  begincode       NUMBER,
  increase_length NUMBER,
  frequency       VARCHAR2(32),
  current_code    NUMBER,
  updatedate      DATE
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_ONLYCODE
  is '系统编码信息';
comment on column DEV.P_B_ONLYCODE.codeid
  is '唯一号编码';
comment on column DEV.P_B_ONLYCODE.codename
  is '唯一号名称';
comment on column DEV.P_B_ONLYCODE.prefix
  is '前缀';
comment on column DEV.P_B_ONLYCODE.suffix
  is '后缀';
comment on column DEV.P_B_ONLYCODE.codelength
  is '长度';
comment on column DEV.P_B_ONLYCODE.begincode
  is '开始号';
comment on column DEV.P_B_ONLYCODE.increase_length
  is '增长长度';
comment on column DEV.P_B_ONLYCODE.frequency
  is '频度';
comment on column DEV.P_B_ONLYCODE.current_code
  is '当前号';
comment on column DEV.P_B_ONLYCODE.updatedate
  is '更新时间';
alter table DEV.P_B_ONLYCODE
  add constraint PK_P_B_ONLYCODE primary key (CODEID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_B_PARAM
prompt ========================
prompt
create table DEV.P_B_PARAM
(
  paramcode  VARCHAR2(32) not null,
  paramname  VARCHAR2(40),
  valuetype  VARCHAR2(32),
  paramvalue VARCHAR2(40),
  paramref   VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_B_PARAM
  is '参数信息表(同步客户端有启动数据采集线程数、数据源结构变化检测时间）';
comment on column DEV.P_B_PARAM.paramcode
  is '参数编码';
comment on column DEV.P_B_PARAM.paramname
  is '参数名称';
comment on column DEV.P_B_PARAM.valuetype
  is '参数值类型';
comment on column DEV.P_B_PARAM.paramvalue
  is '参数值';
comment on column DEV.P_B_PARAM.paramref
  is '参数值参考';
alter table DEV.P_B_PARAM
  add constraint PK_P_B_PARAM primary key (PARAMCODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_CORP_REGISTER
prompt ==============================
prompt
create table DEV.P_CORP_REGISTER
(
  corp_code     VARCHAR2(32) not null,
  corp_name     VARCHAR2(60),
  register_date DATE,
  setup_date    DATE,
  system_number INTEGER,
  mq_number     INTEGER,
  register_code VARCHAR2(60)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on column DEV.P_CORP_REGISTER.corp_code
  is '单位编码';
comment on column DEV.P_CORP_REGISTER.corp_name
  is '单位名称';
comment on column DEV.P_CORP_REGISTER.register_date
  is '注册时间';
comment on column DEV.P_CORP_REGISTER.setup_date
  is '安装时间';
comment on column DEV.P_CORP_REGISTER.system_number
  is '接入系统数量';
comment on column DEV.P_CORP_REGISTER.mq_number
  is '消息队列数量';
comment on column DEV.P_CORP_REGISTER.register_code
  is '注册码';
alter table DEV.P_CORP_REGISTER
  add constraint PK_P_CORP_REGISTER primary key (CORP_CODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_D_DYNAMICCODE
prompt ==============================
prompt
create table DEV.P_D_DYNAMICCODE
(
  dynamiccode      VARCHAR2(32) not null,
  queryid          VARCHAR2(32) not null,
  codeid           VARCHAR2(32) not null,
  dependotherfield VARCHAR2(1) not null,
  codename         VARCHAR2(50),
  querysql         VARCHAR2(2000)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_DYNAMICCODE
  is '动态代码表';
comment on column DEV.P_D_DYNAMICCODE.dynamiccode
  is '动态代码唯一号';
comment on column DEV.P_D_DYNAMICCODE.queryid
  is '查询ID';
comment on column DEV.P_D_DYNAMICCODE.codeid
  is '代码表标识';
comment on column DEV.P_D_DYNAMICCODE.dependotherfield
  is '依赖其他字段 1依赖其他字段 2不依赖其他字段';
comment on column DEV.P_D_DYNAMICCODE.codename
  is '代码表名称';
comment on column DEV.P_D_DYNAMICCODE.querysql
  is '查询语句，查询结果为四个字段，分别为valueCode,valueName,pym,memo';
alter table DEV.P_D_DYNAMICCODE
  add constraint PK_P_D_DYNAMICCODE primary key (DYNAMICCODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_D_PAGE
prompt =======================
prompt
create table DEV.P_D_PAGE
(
  pagecode     VARCHAR2(32) not null,
  pagename     VARCHAR2(50),
  pagelayout   VARCHAR2(2),
  ordernumber  INTEGER,
  memo         VARCHAR2(100),
  pageid       VARCHAR2(32),
  pageflag     VARCHAR2(1),
  interlocking INTEGER default 1
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_PAGE
  is '页面设置';
comment on column DEV.P_D_PAGE.pagecode
  is '页面编码';
comment on column DEV.P_D_PAGE.pagename
  is '页面名称';
comment on column DEV.P_D_PAGE.pagelayout
  is '页面布局';
comment on column DEV.P_D_PAGE.ordernumber
  is '序号';
comment on column DEV.P_D_PAGE.memo
  is '备注';
comment on column DEV.P_D_PAGE.pageid
  is '内部编码';
comment on column DEV.P_D_PAGE.pageflag
  is '系统标志  1系统级 2用户级';
comment on column DEV.P_D_PAGE.interlocking
  is '联动子页面数量';
create index DEV.IDX_PAGEID on DEV.P_D_PAGE (PAGEID)
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_D_PAGE
  add constraint PK_P_D_PAGE primary key (PAGECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_D_QUERYMAIN
prompt ============================
prompt
create table DEV.P_D_QUERYMAIN
(
  subpagecode     VARCHAR2(32) not null,
  querydesc       VARCHAR2(100),
  querysql        VARCHAR2(4000),
  expanderflag    VARCHAR2(1),
  queryinfo       VARCHAR2(50),
  editflag        VARCHAR2(1) default 0,
  expandcol       VARCHAR2(32),
  rowfields       INTEGER default 1,
  dateformat      VARCHAR2(32) default 'yyyy-mm-dd hh:mm:ss',
  countsql        VARCHAR2(4000),
  sendclient      VARCHAR2(1) default 1,
  queryparam      VARCHAR2(500),
  pagewidth       INTEGER default 400,
  pageheight      INTEGER default 300,
  functiondesc    VARCHAR2(40),
  ordernumber     INTEGER,
  pagecode        VARCHAR2(32),
  pageflag        VARCHAR2(1),
  outqueryfield   VARCHAR2(32),
  innerkeyfield   VARCHAR2(32),
  treequery       VARCHAR2(4000),
  checkflag       VARCHAR2(1) default 1,
  pageflaginsert  VARCHAR2(1) default 1,
  pageflagmodify  VARCHAR2(1) default 1,
  pageflagdelete  VARCHAR2(1) default 1,
  pageflagcopy    VARCHAR2(1) default 0,
  pageflagfind    VARCHAR2(1) default 1,
  pageflagrefresh VARCHAR2(1) default 1,
  pageflagexport  VARCHAR2(1) default 1,
  pageflagprint   VARCHAR2(1) default 1,
  pageflagextend  VARCHAR2(1) default 0,
  extendname      VARCHAR2(32),
  pageflagextend2 VARCHAR2(1) default 0,
  extendname2     VARCHAR2(32),
  extendfun       VARCHAR2(50),
  extendqueryid   VARCHAR2(32),
  dbltrigger      VARCHAR2(1) default 0,
  copypattern     VARCHAR2(1) default 0,
  printtemplate   VARCHAR2(100)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 8K
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_QUERYMAIN
  is '查询语句配置主表';
comment on column DEV.P_D_QUERYMAIN.subpagecode
  is '查询语句ID';
comment on column DEV.P_D_QUERYMAIN.querydesc
  is '查询语句描述';
comment on column DEV.P_D_QUERYMAIN.querysql
  is '查询语句';
comment on column DEV.P_D_QUERYMAIN.expanderflag
  is '展开显示 0不展开显示 1展开显示';
comment on column DEV.P_D_QUERYMAIN.queryinfo
  is '查询默认显示信息';
comment on column DEV.P_D_QUERYMAIN.editflag
  is '表格编辑标志 0不可编辑 1可编辑';
comment on column DEV.P_D_QUERYMAIN.expandcol
  is '展开列';
comment on column DEV.P_D_QUERYMAIN.rowfields
  is '编辑时一行几列';
comment on column DEV.P_D_QUERYMAIN.dateformat
  is '日期格式';
comment on column DEV.P_D_QUERYMAIN.countsql
  is '汇总语句';
comment on column DEV.P_D_QUERYMAIN.sendclient
  is '传送客户端标志 0不传 1传送';
comment on column DEV.P_D_QUERYMAIN.queryparam
  is '查询参数 tableName.Field=#fieldName#| tableName.Field=#fieldName#';
comment on column DEV.P_D_QUERYMAIN.pagewidth
  is '页面宽度';
comment on column DEV.P_D_QUERYMAIN.pageheight
  is '页面高度';
comment on column DEV.P_D_QUERYMAIN.functiondesc
  is '功能名称描述';
comment on column DEV.P_D_QUERYMAIN.ordernumber
  is '排序号，内部用';
comment on column DEV.P_D_QUERYMAIN.pagecode
  is '页面编码';
comment on column DEV.P_D_QUERYMAIN.pageflag
  is '分页标志0不分页1分页';
comment on column DEV.P_D_QUERYMAIN.outqueryfield
  is '外部查询字段配置';
comment on column DEV.P_D_QUERYMAIN.innerkeyfield
  is '内部主键';
comment on column DEV.P_D_QUERYMAIN.treequery
  is '树形查询语句';
comment on column DEV.P_D_QUERYMAIN.checkflag
  is '选择框标志 0没有选择框 1有选择框';
comment on column DEV.P_D_QUERYMAIN.pageflaginsert
  is '页面操作标志-新增 0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagmodify
  is '页面操作标志-修改  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagdelete
  is '页面操作标志-删除  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagcopy
  is '页面操作标志-复制  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagfind
  is '页面操作标志-查找  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagrefresh
  is '页面操作标志-刷新 0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagexport
  is '页面操作标志-导出  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagprint
  is '页面操作标志-打印   0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.pageflagextend
  is '页面操作标志-内部扩展（(选择数据提交后台)）  0不允许 1允许';
comment on column DEV.P_D_QUERYMAIN.extendname
  is '页面操作扩展名称';
comment on column DEV.P_D_QUERYMAIN.pageflagextend2
  is '页面操作标志-外部扩展  0不允许 1允许(传入Record调用本地函数)';
comment on column DEV.P_D_QUERYMAIN.extendname2
  is '页面操作扩展名称';
comment on column DEV.P_D_QUERYMAIN.extendfun
  is '外部扩展调用Fun 自动传给QueryID与当前的Grid';
comment on column DEV.P_D_QUERYMAIN.extendqueryid
  is '外部扩展用到的QueryID';
comment on column DEV.P_D_QUERYMAIN.dbltrigger
  is '双击触发后台 0不触发后台 1触发后台';
comment on column DEV.P_D_QUERYMAIN.copypattern
  is '复制模式 0 新增  1当前行匹配';
comment on column DEV.P_D_QUERYMAIN.printtemplate
  is '打印模板名称';
alter table DEV.P_D_QUERYMAIN
  add constraint PK_P_D_QUERYMAIN primary key (SUBPAGECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_D_QUERYMAIN
  add constraint FK_P_D_QUER2 foreign key (PAGECODE)
  references DEV.P_D_PAGE (PAGECODE);

prompt
prompt Creating table P_D_QUERYFIELD
prompt =============================
prompt
create table DEV.P_D_QUERYFIELD
(
  subpagecode    VARCHAR2(32) not null,
  fieldname      VARCHAR2(32) not null,
  fielddisp      VARCHAR2(50),
  fieldtype      VARCHAR2(32) default 'VARCHAR2',
  fieldlength    INTEGER,
  fieldprecision INTEGER,
  codename       VARCHAR2(32),
  dispwidth      INTEGER default 100,
  sortable       VARCHAR2(1) default 0,
  multi          INTEGER default 1,
  editflag       VARCHAR2(1) default 1,
  visibleflag    VARCHAR2(1) default 1,
  fieldorder     INTEGER default 10,
  fixed          VARCHAR2(1) default 1,
  menudisabled   VARCHAR2(1) default 1,
  tooltip        VARCHAR2(100),
  renderer       VARCHAR2(100),
  disptype       VARCHAR2(32) default 'string',
  align          VARCHAR2(32) default 'left',
  allowblank     VARCHAR2(1) default 1,
  regex          VARCHAR2(200),
  regextext      VARCHAR2(50),
  maskre         VARCHAR2(200),
  defaulttype    VARCHAR2(1) default 0,
  codeflag       VARCHAR2(1) default 0,
  defaultvalue   VARCHAR2(50)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_QUERYFIELD
  is '查询字段配置';
comment on column DEV.P_D_QUERYFIELD.subpagecode
  is '查询语句ID';
comment on column DEV.P_D_QUERYFIELD.fieldname
  is '列名称(显示列名称)';
comment on column DEV.P_D_QUERYFIELD.fielddisp
  is '中文名';
comment on column DEV.P_D_QUERYFIELD.fieldtype
  is '字段类型';
comment on column DEV.P_D_QUERYFIELD.fieldlength
  is '字段长度';
comment on column DEV.P_D_QUERYFIELD.fieldprecision
  is '字段精度';
comment on column DEV.P_D_QUERYFIELD.codename
  is '代码名称';
comment on column DEV.P_D_QUERYFIELD.dispwidth
  is '显示宽度';
comment on column DEV.P_D_QUERYFIELD.sortable
  is '排度标志 1排序 0不排序';
comment on column DEV.P_D_QUERYFIELD.multi
  is '多行标记';
comment on column DEV.P_D_QUERYFIELD.editflag
  is '可否编辑';
comment on column DEV.P_D_QUERYFIELD.visibleflag
  is '是否可见';
comment on column DEV.P_D_QUERYFIELD.fieldorder
  is '序号';
comment on column DEV.P_D_QUERYFIELD.fixed
  is '列的宽度能否改变 0不能改变 1能改变';
comment on column DEV.P_D_QUERYFIELD.menudisabled
  is '禁止列菜单 0禁止 1不禁止';
comment on column DEV.P_D_QUERYFIELD.tooltip
  is '列头显示提示文字';
comment on column DEV.P_D_QUERYFIELD.renderer
  is '转换函数';
comment on column DEV.P_D_QUERYFIELD.disptype
  is '页面显示字段类型';
comment on column DEV.P_D_QUERYFIELD.align
  is '对齐方式';
comment on column DEV.P_D_QUERYFIELD.allowblank
  is '允许为空 0 不允许 1允许';
comment on column DEV.P_D_QUERYFIELD.regex
  is '验证正则表达式';
comment on column DEV.P_D_QUERYFIELD.regextext
  is '验证错误提示信息';
comment on column DEV.P_D_QUERYFIELD.maskre
  is '掩码';
comment on column DEV.P_D_QUERYFIELD.defaulttype
  is ' 默认值类型0 没有默认值 1常量 2外部 3内部';
comment on column DEV.P_D_QUERYFIELD.codeflag
  is '下拉框代码类型  0非Combox 1Combox，2GridCombox 3TreeCombox   4dynamicCombobox';
comment on column DEV.P_D_QUERYFIELD.defaultvalue
  is '默认值 ';
alter table DEV.P_D_QUERYFIELD
  add constraint PK_P_D_QUERYFIELD primary key (SUBPAGECODE, FIELDNAME)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_D_QUERYFIELD
  add constraint FK_P_D_QUER_REFERENCE_P_D_QUER foreign key (SUBPAGECODE)
  references DEV.P_D_QUERYMAIN (SUBPAGECODE);

prompt
prompt Creating table P_D_QUERYMODIFY
prompt ==============================
prompt
create table DEV.P_D_QUERYMODIFY
(
  subpagecode  VARCHAR2(32) not null,
  modifyid     VARCHAR2(32) not null,
  modifytype   VARCHAR2(1),
  modifysql    VARCHAR2(4000),
  clienttype   VARCHAR2(1),
  procparam    VARCHAR2(500),
  ordernumber  INTEGER,
  keyfield     VARCHAR2(32),
  keycode      VARCHAR2(32),
  procoutparam VARCHAR2(500) default 'prm_AppCode|prm_ErrorMsg'
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 128K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_QUERYMODIFY
  is '查询语句配置主表';
comment on column DEV.P_D_QUERYMODIFY.subpagecode
  is '查询语句ID';
comment on column DEV.P_D_QUERYMODIFY.modifyid
  is '编辑语句ID';
comment on column DEV.P_D_QUERYMODIFY.modifytype
  is '后台操作类别 U 更新 D删除 P调用存储过程 I插入    Z功能扩展';
comment on column DEV.P_D_QUERYMODIFY.modifysql
  is '编辑参数  tableName.Field=#fieldName#| tableName.Field=#fieldName#';
comment on column DEV.P_D_QUERYMODIFY.clienttype
  is '客户端操作类型 I新增,U修改,D删除    C树节点复制   E树节点删除   Z功能扩展';
comment on column DEV.P_D_QUERYMODIFY.procparam
  is '存储过程入参参数#fieldName#| #fieldName##fieldName#';
comment on column DEV.P_D_QUERYMODIFY.ordernumber
  is '序号';
comment on column DEV.P_D_QUERYMODIFY.keyfield
  is '主键';
comment on column DEV.P_D_QUERYMODIFY.keycode
  is '主键号';
comment on column DEV.P_D_QUERYMODIFY.procoutparam
  is '存储过程输出参数';
alter table DEV.P_D_QUERYMODIFY
  add constraint PK_P_D_QUERYMODIFY primary key (SUBPAGECODE, MODIFYID)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_D_Q_PAGE
prompt =========================
prompt
create table DEV.P_D_Q_PAGE
(
  pagecode     VARCHAR2(32) not null,
  pagename     VARCHAR2(50),
  pagelayout   VARCHAR2(2),
  ordernumber  INTEGER,
  memo         VARCHAR2(100),
  interlocking INTEGER
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_Q_PAGE
  is '查询页面设置';
comment on column DEV.P_D_Q_PAGE.pagecode
  is '页面编码';
comment on column DEV.P_D_Q_PAGE.pagename
  is '页面名称';
comment on column DEV.P_D_Q_PAGE.pagelayout
  is '页面布局';
comment on column DEV.P_D_Q_PAGE.ordernumber
  is '序号';
comment on column DEV.P_D_Q_PAGE.memo
  is '备注';
comment on column DEV.P_D_Q_PAGE.interlocking
  is '联动数量';
alter table DEV.P_D_Q_PAGE
  add constraint PK_P_D_Q_PAGE primary key (PAGECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating table P_D_Q_MODEL
prompt ==========================
prompt
create table DEV.P_D_Q_MODEL
(
  subpagecode   VARCHAR2(32) not null,
  pagecode      VARCHAR2(32),
  functiondesc  VARCHAR2(40),
  querydesc     VARCHAR2(100),
  querysql      VARCHAR2(4000),
  expanderflag  VARCHAR2(1),
  expandcol     VARCHAR2(32),
  dateformat    VARCHAR2(32),
  countsql      VARCHAR2(4000),
  ordernumber   INTEGER,
  pageflag      VARCHAR2(1),
  checkflag     VARCHAR2(1),
  printtemplate VARCHAR2(100),
  outqueryfield VARCHAR2(32),
  innerkeyfield VARCHAR2(32),
  pagewidth     INTEGER default 400,
  pageheight    INTEGER default 300,
  mainqueryflag VARCHAR2(1)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 8K
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_Q_MODEL
  is '查询语句配置';
comment on column DEV.P_D_Q_MODEL.subpagecode
  is '子页面编码';
comment on column DEV.P_D_Q_MODEL.pagecode
  is '页面编码';
comment on column DEV.P_D_Q_MODEL.functiondesc
  is '功能名称描述';
comment on column DEV.P_D_Q_MODEL.querydesc
  is '查询语句描述';
comment on column DEV.P_D_Q_MODEL.querysql
  is '查询语句';
comment on column DEV.P_D_Q_MODEL.expanderflag
  is '展开显示 0不展开显示 1展开显示';
comment on column DEV.P_D_Q_MODEL.expandcol
  is '展开列';
comment on column DEV.P_D_Q_MODEL.dateformat
  is '日期格式';
comment on column DEV.P_D_Q_MODEL.countsql
  is '汇总语句';
comment on column DEV.P_D_Q_MODEL.ordernumber
  is '子页面序号';
comment on column DEV.P_D_Q_MODEL.pageflag
  is '分页标志0不分页1分页';
comment on column DEV.P_D_Q_MODEL.checkflag
  is '选择框标志 0没有选择框 1有选择框';
comment on column DEV.P_D_Q_MODEL.printtemplate
  is '打印模板名称';
comment on column DEV.P_D_Q_MODEL.outqueryfield
  is '外部查询查询字段';
comment on column DEV.P_D_Q_MODEL.innerkeyfield
  is '内部主键';
comment on column DEV.P_D_Q_MODEL.pagewidth
  is '页面宽度';
comment on column DEV.P_D_Q_MODEL.pageheight
  is '页面高度';
comment on column DEV.P_D_Q_MODEL.mainqueryflag
  is '主查询标志';
alter table DEV.P_D_Q_MODEL
  add constraint PK_P_D_Q_MODEL primary key (SUBPAGECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_D_Q_MODEL
  add constraint FK_P_V_SUBPAGE foreign key (PAGECODE)
  references DEV.P_D_Q_PAGE (PAGECODE);

prompt
prompt Creating table P_D_Q_FIELD
prompt ==========================
prompt
create table DEV.P_D_Q_FIELD
(
  fieldname      VARCHAR2(32) not null,
  subpagecode    VARCHAR2(32) not null,
  fielddisp      VARCHAR2(50) not null,
  fieldtype      VARCHAR2(32),
  fieldlength    INTEGER,
  fieldprecision INTEGER,
  dispwidth      INTEGER,
  sortable       VARCHAR2(1),
  visibleflag    VARCHAR2(1),
  fieldorder     INTEGER,
  fixed          VARCHAR2(1),
  menudisabled   VARCHAR2(1),
  tooltip        VARCHAR2(100),
  renderer       VARCHAR2(100),
  disptype       VARCHAR2(32),
  align          VARCHAR2(32),
  codeflag       VARCHAR2(1),
  codename       VARCHAR2(32),
  condtionflag   VARCHAR2(1),
  conditonorder  INTEGER,
  dateflag       VARCHAR2(1)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_D_Q_FIELD
  is '查询模块字段配置';
comment on column DEV.P_D_Q_FIELD.fieldname
  is '列名称(显示列名称)';
comment on column DEV.P_D_Q_FIELD.subpagecode
  is '子页面编码';
comment on column DEV.P_D_Q_FIELD.fielddisp
  is '中文名';
comment on column DEV.P_D_Q_FIELD.fieldtype
  is '字段类型';
comment on column DEV.P_D_Q_FIELD.fieldlength
  is '字段长度';
comment on column DEV.P_D_Q_FIELD.fieldprecision
  is '字段精度';
comment on column DEV.P_D_Q_FIELD.dispwidth
  is '显示宽度';
comment on column DEV.P_D_Q_FIELD.sortable
  is '排度标志 0排序 1不排序';
comment on column DEV.P_D_Q_FIELD.visibleflag
  is '是否可见';
comment on column DEV.P_D_Q_FIELD.fieldorder
  is '序号';
comment on column DEV.P_D_Q_FIELD.fixed
  is '列的宽度能否改变 0不能改变 1能改变';
comment on column DEV.P_D_Q_FIELD.menudisabled
  is '禁止列菜单 0禁止 1不禁止';
comment on column DEV.P_D_Q_FIELD.tooltip
  is '列头显示提示文字';
comment on column DEV.P_D_Q_FIELD.renderer
  is '转换函数';
comment on column DEV.P_D_Q_FIELD.disptype
  is '页面数据类型';
comment on column DEV.P_D_Q_FIELD.align
  is '对齐方式';
comment on column DEV.P_D_Q_FIELD.codeflag
  is '代码类型 0非Combox 1Combox，2GridCombox 3TreeCombox  4dynamicCombobox';
comment on column DEV.P_D_Q_FIELD.codename
  is '代码名称';
comment on column DEV.P_D_Q_FIELD.condtionflag
  is '查询时间字段标识 0非查询条件字段 1查询条件字段';
comment on column DEV.P_D_Q_FIELD.conditonorder
  is '查询条件序号';
comment on column DEV.P_D_Q_FIELD.dateflag
  is '查询时间段标志 0非时间段 1时间段';
alter table DEV.P_D_Q_FIELD
  add constraint PK_P_D_Q_FIELD primary key (FIELDNAME, SUBPAGECODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
alter table DEV.P_D_Q_FIELD
  add constraint FK_P_SUB_V_FIELD foreign key (SUBPAGECODE)
  references DEV.P_D_Q_MODEL (SUBPAGECODE);

prompt
prompt Creating table P_L_OPERLOG
prompt ==========================
prompt
create table DEV.P_L_OPERLOG
(
  onlycode     VARCHAR2(32) not null,
  businessid   VARCHAR2(32),
  ipaddr       VARCHAR2(15),
  programcode  VARCHAR2(32),
  programname  VARCHAR2(50),
  operdatetime DATE,
  runstate     VARCHAR2(1),
  statedesc    VARCHAR2(500),
  functioncode VARCHAR2(32),
  functionname VARCHAR2(500),
  opercode     VARCHAR2(32)
)
tablespace USERS
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 3M
    next 1M
    minextents 1
    maxextents unlimited
  );
comment on table DEV.P_L_OPERLOG
  is '操作人员操作日日志';
comment on column DEV.P_L_OPERLOG.onlycode
  is '唯一号';
comment on column DEV.P_L_OPERLOG.businessid
  is '业务编码';
comment on column DEV.P_L_OPERLOG.ipaddr
  is '所在机器IP';
comment on column DEV.P_L_OPERLOG.programcode
  is '前台程序';
comment on column DEV.P_L_OPERLOG.programname
  is '前台程序名称';
comment on column DEV.P_L_OPERLOG.operdatetime
  is '状态发生时间';
comment on column DEV.P_L_OPERLOG.runstate
  is '1启动0关机2异常';
comment on column DEV.P_L_OPERLOG.statedesc
  is '状态信息';
comment on column DEV.P_L_OPERLOG.functioncode
  is '模块编码';
comment on column DEV.P_L_OPERLOG.functionname
  is '模块名称';
comment on column DEV.P_L_OPERLOG.opercode
  is '操作人员编码';
alter table DEV.P_L_OPERLOG
  add constraint PK_P_L_OPERLOG primary key (ONLYCODE)
  using index 
  tablespace USERS
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 384K
    next 1M
    minextents 1
    maxextents unlimited
  );

prompt
prompt Creating sequence G4_SEQ_EAJDBCMONITOR_MONITORID
prompt ================================================
prompt
create sequence DEV.G4_SEQ_EAJDBCMONITOR_MONITORID
minvalue 1
maxvalue 9999999999
start with 5382
increment by 1
nocache;

prompt
prompt Creating view V_CODETABLE
prompt =========================
prompt
create or replace force view dev.v_codetable as
select t.basecodetablecode,t.basecodetablename
    from p_b_basecodeinfo t;

prompt
prompt Creating view V_CODEVIEW
prompt ========================
prompt
create or replace force view dev.v_codeview as
select t.basecodetablecode,t.valuecode,t.valuename,t.memo,t.pym,t.parentcode
    from p_b_codevalue t;

prompt
prompt Creating view V_DYNAIMCCODE
prompt ===========================
prompt
create or replace force view dev.v_dynaimccode as
select dynamiccode, queryid, codeid, dependotherfield, codename, querysql from p_d_dynamiccode;

prompt
prompt Creating package PKG_D_PAGE
prompt ===========================
prompt
create or replace package dev.PKG_D_PAGE is
  def_OK         CONSTANT VARCHAR2(10)    := '0' ;          --成功
  /*-------------------------------------------------------------------------
   || 过程名称 ：新增页面
   || 功能描述 ：新增页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Add_Page(     
     prm_Pageid     In  VARCHAR2, --页面内部编码
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
  /*-------------------------------------------------------------------------
   || 过程名称 ：修改页面
   || 功能描述 ：修改页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Modidfy_Page(  
     prm_pageid          IN VARCHAR2, --页面内部编码
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_layout          IN  VARCHAR2, --页面布局    
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除页面
   || 功能描述 ：删除页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Delete_Page(     
     prm_PageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
    
    /*-------------------------------------------------------------------------
   || 过程名称 ：删除子页页面
   || 功能描述 ：删除子页页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Delete_SubPage(     
     prm_SubPageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
  
    
  /*-------------------------------------------------------------------------
   || 过程名称 ：自动生成子页面信息
   || 功能描述 ：自动生成子页面信息
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Auto_subPage(     
     prm_Pageid         In  VARCHAR2, --页面内部编码 
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
 
  /*-------------------------------------------------------------------------
   || 过程名称 ：自动页面逻辑生成
   || 功能描述 ：自动页面逻辑生成
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Auto_pageLogic(     
     prm_subPageCode     In  VARCHAR2, --子页面编码
     prm_fundesc         IN  VARCHAR2, --子页面名称
     prm_Tables          In  VARCHAR2, --传入的物理表
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
end PKG_D_PAGE;
/

prompt
prompt Creating package PKG_D_QUERY
prompt ============================
prompt
create or replace package dev.PKG_D_Query is
  def_OK         CONSTANT VARCHAR2(10)    := '0' ;          --成功
  /*-------------------------------------------------------------------------
   || 过程名称 ：新增页面
   || 功能描述 ：新增页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Query_Add(     
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
  /*-------------------------------------------------------------------------
   || 过程名称 ：修改页面
   || 功能描述 ：修改页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Query_Modidfy(  
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_layout          IN  VARCHAR2, --页面布局    
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除页面
   || 功能描述 ：删除页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Query_Delete(     
     prm_PageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
    
    /*-------------------------------------------------------------------------
   || 过程名称 ：删除子页页面
   || 功能描述 ：删除子页页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_SubQuery_Delete(     
     prm_SubPageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
  
    
  /*-------------------------------------------------------------------------
   || 过程名称 ：自动生成子页面信息
   || 功能描述 ：自动生成子页面信息
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_subQuery_Auto(     
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
 
  /*-------------------------------------------------------------------------
   || 过程名称 ：自动页面逻辑生成
   || 功能描述 ：自动页面逻辑生成
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_subQuery_AutoLogic(     
     prm_subPageCode     In  VARCHAR2, --子页面编码
     prm_fundesc         IN  VARCHAR2, --子页面名称
     prm_Tables          In  VARCHAR2, --传入的物理表
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    );   
end PKG_D_Query;
/

prompt
prompt Creating package PKG_P_RUN
prompt ==========================
prompt
CREATE OR REPLACE PACKAGE DEV.PKG_P_RUN AS
   
   def_OK     CONSTANT VARCHAR2(10)    := '0' ;          --成功
   IPAddr     CONSTANT VARCHAR2(20):='IP_ADDRESS';
   ProgramCode CONSTANT VARCHAR2(32):='HOST';
   ProgramName CONSTANT VARCHAR2(32):='HOST';
   OperCode    CONSTANT VARCHAR2(32):='SESSION_USER';
   FunctionCode CONSTANT VARCHAR2(32):='CLIENT_INFO';
   FunctionName  CONSTANT VARCHAR2(32):='CLIENT_INFO';
   
    /*-------------------------------------------------------------------------
   || 过程名称 ：写入日志
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Save_Log(
     prm_State          IN  VARCHAR2, --状态信息
     prm_Desc           IN  VARCHAR2 --描述信息
     ) ; 
   
  

  

END PKG_P_RUN ;
/

prompt
prompt Creating package PKG_P_UTILTY
prompt =============================
prompt
CREATE OR REPLACE PACKAGE DEV.PKG_P_Utilty AS
   
   def_OK     CONSTANT VARCHAR2(10)    := '0' ;          --成功
   Env_Domain CONSTANT VARCHAR2(20)  :='USERENV';

   
   /*-------------------------------------------------------------------------
   || 过程名称 ：加密
   || 功能描述 ：对字符串进行加密
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Encrypt_DES
     (prm_InputStr       IN VARCHAR2,     --输入字符串
      prm_EncryptStr     OUT VARCHAR2,    --输出字符串
      prm_AppCode        OUT VARCHAR2,      --执行代码
      prm_ErrorMsg       OUT VARCHAR2
     ) ; 
     
   /*-------------------------------------------------------------------------
   || 过程名称 ：解密
   || 功能描述 ：根据算法对数据进行解密
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/   
    PROCEDURE Prc_DECRYPT_DES
     (prm_InputStr       IN VARCHAR2,     --输入字符串
      prm_DecryptStr     OUT VARCHAR2,    --输出字符串
      prm_AppCode        OUT VARCHAR2,      --执行代码
      prm_ErrorMsg       OUT VARCHAR2
     ) ;
     
   /*-------------------------------------------------------------------------
   || 过程名称 ：得到系统唯一号
   || 功能描述 ：通过编码规则得到系统唯一号
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_get_Code(
      prm_Code               IN  VARCHAR2, --唯一号编码
      prm_OnlyOne            OUT VARCHAR2,
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 );
    /*-------------------------------------------------------------------------
   || 过程名称 ：得到参数值
   || 功能描述 ：通过编码规则得到系统唯一号
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_get_Param(
      prm_paramCode          IN  VARCHAR2, --参数编码
      prm_paramValue         OUT VARCHAR2, --参数值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 );
    /*-------------------------------------------------------------------------
   || 过程名称 ：设置参数值
   || 功能描述 ：通过编码规则得到系统唯一号
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Set_ParamValue(
      prm_paramCode          IN  VARCHAR2, --参数编码
      prm_paramValue         IN VARCHAR2, --参数值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 );
    /*-------------------------------------------------------------------------
   || 过程名称 ：设置会话级环境变量
   || 功能描述 ：设置数据库连接会话级环境变量
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：USERENV
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Set_SessionENV(
      prm_Code               IN  VARCHAR2, --环境变量键
      prm_Value               IN  VARCHAR2, --环境变量键值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 );
        /*-------------------------------------------------------------------------
   || 过程名称 ：得到会话级环境变量
   || 功能描述 ：得到数据库连接会话级环境变量
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：USERENV
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Get_SessionENV(
      prm_Code               IN  VARCHAR2, --环境变量键
      prm_Value              OUT  VARCHAR2, --环境变量键值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 );
   /*-------------------------------------------------------------------------
   || 过程名称 ：对索引进行处理
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Create_TableKey(
    prm_TableName    IN  VARCHAR2,        --物理表名
    prm_KeyType      IN  VARCHAR2,         --Key类型 P为主键, U唯一索引 ,N不般索引
    prm_ColName      IN  VARCHAR2,
    prm_DataSouceID  IN  VARCHAR2,         --数据源标识
    prm_AppCode      OUT VARCHAR2,      --执行代码
    prm_ErrorMsg     OUT VARCHAR2
   ); 
   
  /*-------------------------------------------------------------------------
   || 过程名称 ：分割字符串
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Split_Str(    
   prm_str           in   varchar2,  --分割字符串
   prm_separator     in   varchar2, --分割符
   prm_CharTable     OUT  dbms_sql.Varchar2_Table,      --执行代码
   prm_AppCode      OUT VARCHAR2,      --执行代码
   prm_ErrorMsg     OUT VARCHAR2
   );
      
   

END PKG_P_Utilty ;
/

prompt
prompt Creating procedure G4_PRC_CURSOR_DEMO
prompt =====================================
prompt
create or replace procedure dev.g4_prc_cursor_demo(prm_Xm       IN VARCHAR2,
                                               prm_Cur      OUT sys_refcursor,
                                               prm_AppCode  OUT VARCHAR2,
                                               prm_ErrorMsg OUT VARCHAR2) is
begin
  prm_AppCode := 1;
  open prm_Cur for
    select xm, fyze from ea_demo_byjsb where xm like '%' || prm_Xm || '%';
EXCEPTION
  WHEN OTHERS THEN
    prm_AppCode  := -1;
    prm_ErrorMsg := '出错：' || SQLERRM;
end g4_prc_cursor_demo;
/

prompt
prompt Creating procedure G4_PRC_DEMO
prompt ==============================
prompt
create or replace procedure dev.g4_prc_demo(prm_myname   IN VARCHAR2,
                                        prm_number1  IN NUMBER,
                                        prm_number2  IN NUMBER,
                                        prm_sum      OUT NUMBER,
                                        prm_result   OUT VARCHAR2,
                                        prm_AppCode  OUT VARCHAR2,
                                        prm_ErrorMsg OUT VARCHAR2) is
begin
  prm_sum      := prm_number1 + prm_number2;
  prm_result   := 'hello, ' || prm_myname || '!';
  prm_AppCode  := 1;
  prm_ErrorMsg := '';
end g4_prc_demo;
/

prompt
prompt Creating procedure P_WRAPED_USER
prompt ================================
prompt
CREATE OR REPLACE PROCEDURE DEV.p_wraped_user AUTHID CURRENT_USER AS
--Created by xsb on 2006-11-10  
--For:批量加密本用户下的所有代码，包括存储过程、函数、包。 
v_procs dbms_sql.varchar2a; 

BEGIN 
    FOR n IN (SELECT DISTINCT NAME, TYPE 
    FROM user_source 
    WHERE NAME <> 'P_WRAPED_USER' AND 
     TYPE like  'PACKAGE%' 
    MINUS 
    SELECT DISTINCT NAME, TYPE 
    FROM user_source 
    WHERE line = 1 AND 
    instr(text, 'wrapped') > 0 
    ORDER BY TYPE) LOOP 
        FOR i IN (SELECT rownum rn, text 
            FROM (SELECT decode(line, 1, 'create or replace ') || text text 
            FROM user_source 
            WHERE NAME = n.NAME AND 
            TYPE = n.TYPE 
            ORDER BY line)) LOOP 
            v_procs(i.rn) := i.text; 
         END LOOP; 
      dbms_ddl.create_wrapped(v_procs, 1, v_procs.COUNT); 
      v_procs.DELETE; 
      END LOOP; 

END;
/

prompt
prompt Creating package body PKG_D_PAGE
prompt ================================
prompt
create or replace package body dev.PKG_D_PAGE is

 /*-------------------------------------------------------------------------
   || 过程名称 ：新增页面
   || 功能描述 ：新增页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Add_Page(     
     prm_pageid          IN VARCHAR2, --页面内部编码
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    BEGIN
     prm_AppCode:=def_OK;   
     ------------------调用存储过程生成初始化子页面信息---------------------
     Prc_Auto_subPage(prm_Pageid,prm_PageCode,prm_AppCode,prm_ErrorMsg);
     if(prm_AppCode<>def_OK) then
        return;
     end if;
   EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);    
  end;    
  /*-------------------------------------------------------------------------
   || 过程名称 ：修改页面
   || 功能描述 ：修改页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||先调用存储过程，再进行修改
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Modidfy_Page(    
     prm_Pageid         In  VARCHAR2, --页面内部编码 
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_layout          IN  VARCHAR2, --页面布局    
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    i_count  Integer;
    r_page   p_d_page%rowType;
    BEGIN
     prm_AppCode:=def_OK;
      ---查找页面信息------------------
      select *
      into r_page
      from p_d_page
     where pageid = prm_Pageid;
      if(r_page.pageflag='1') then
        prm_ErrorMsg:='系统页面配置信息，不能进行删除!';
        prm_AppCode:='-1';     
        return;
      end if;
      if r_page.pagecode<> prm_PageCode then
        prm_ErrorMsg:='页面编码是一个全局信息，新增后不能更改!';
        prm_AppCode:='-1';     
        return;
     end if;
     if r_page.pagelayout<> prm_layout then
        prm_ErrorMsg:='页面布局不能初始化后，不能被修改，如果真要修改，请先删除页面!';
        prm_AppCode:='-1';     
        return;
     end if;
     -----------查找有无初始化信息，没有自动生成----------------------
     SELECT Count(1)
     into i_count
     from p_d_querymain
     where pagecode=prm_PageCode;
     If (i_count>0 ) then
        return;
     end if;
      ------------------调用存储过程生成初始化子页面信息---------------------
     Prc_Auto_subPage(prm_Pageid,prm_PageCode,prm_AppCode,prm_ErrorMsg);
     if(prm_AppCode<>def_OK) then
        return;
     end if;
    EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END;  
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除页面
   || 功能描述 ：删除页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Delete_Page(     
     prm_PageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    r_page   p_d_page%rowType;
    BEGIN
     prm_AppCode:=def_OK;
    
      select *
      into r_page
      from p_d_page
      where pagecode = prm_PageCode;
      if(r_page.pageflag='1') then
        prm_ErrorMsg:='系统页面配置信息，不能进行删除!';
        prm_AppCode:='-1';     
        return;
      end if;    
     --------------------删除字段信息----------------------------------------------
     delete P_D_QUERYFIELD f
     where exists(select 1 
                  from p_d_querymain m
                  where  f.subpagecode=m.subpagecode and
                         m.pagecode=prm_PageCode);
     ----------------------删除数据逻辑处理信息-------------------------------------
     delete  p_d_querymodify p
     where exists(select 1 
                  from p_d_querymain m
                  where  p.subpagecode=m.subpagecode and
                         m.pagecode=prm_PageCode);
    ---------------删除子页面配置信息---------------------------------------------------
    DELETE FROM P_D_QUERYMAIN
    where pagecode=prm_PageCode;
    EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END;   
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除子页页面
   || 功能描述 ：删除子页页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Delete_SubPage(     
     prm_SubPageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg           OUT VARCHAR2
     )  is   
    BEGIN   
     --------------------删除字段信息----------------------------------------------
      prm_AppCode:=def_OK;
     delete P_D_QUERYFIELD f
     where exists(select 1 
                  from p_d_querymain m
                  where  f.subpagecode=m.subpagecode and
                         m.subpagecode=prm_SubPageCode);
     ----------------------删除数据逻辑处理信息-------------------------------------
     delete  p_d_querymodify p
     where exists(select 1 
                  from p_d_querymain m
                  where  p.subpagecode=m.subpagecode and
                         m.subpagecode=prm_SubPageCode);  
  Exception 
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
   End ;   
   /*-------------------------------------------------------------------------
   || 过程名称 ：自动生成子页面信息
   || 功能描述 ：自动生成子页面信息
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Auto_subPage(     
     prm_pageid          IN VARCHAR2, --页面内部编码
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    i_PageCount  integer;
    v_CopyFlag   VARCHAR2(1);
    r_page   p_d_page%rowType;
    T_tables      dbms_sql.Varchar2_Table;
    BEGIN
     prm_AppCode:=def_OK;
 
      ---查找页面信息------------------
      select *
      into r_page
      from p_d_page
      where pageid = prm_Pageid;
      -----------------生成子页面初始化信息----------------------
      i_PageCount:=to_number(substr(r_page.pagelayout,1,1));
      if i_PageCount=1 Then
         T_tables(1):=prm_PageCode||'Cennter';
      end if;
      if i_PageCount=2 Then        
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
      end if;
      if i_PageCount=3 Then
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
         T_tables(3):=prm_PageCode||'Botton';
      end if;
      if i_PageCount=4 Then
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
         T_tables(3):=prm_PageCode||'Botton';
         T_tables(4):=prm_PageCode||'East';
      end if;
      if (substr(r_page.pagelayout,2,1) ='1') then
         v_CopyFlag:='1';
      else
        v_CopyFlag:='0';
      end if;
      
      for i in 1.. T_tables.count loop
         insert into p_d_querymain
          (subpagecode,         querydesc,              querysql,           expanderflag,          queryinfo, 
           editflag,            expandcol,              rowfields,          dateformat,        countsql, 
           sendclient,          queryparam,             pagewidth,          pageheight,        functiondesc, 
           ordernumber,         pagecode,               pageflag,           outqueryfield,     innerkeyfield, 
           treequery,           checkflag,              pageflaginsert,     pageflagmodify,    pageflagdelete, 
           pageflagcopy,        pageflagfind,           pageflagrefresh,    pageflagexport,    pageflagprint, 
           pageflagextend,      extendname,             pageflagextend2,    extendname2,       extendfun, 
           extendqueryid)
        values
           (T_tables(i),       '请输入',               null,               '1',               '编码,名称', 
           '0',                 null,                   1,                  'yyyy-mm-dd hh:mm:ss',null, 
           '1',                 null,                   500,                400,               '请输入', 
           i*10,                prm_PageCode,           '1',                null,              null, 
           null,                '1',                    '1',                '1',               '1', 
           v_CopyFlag,          '1',                    '1',                '1',               '1', 
           '0',                 null,                   '0',                null,              null, 
           null);
      end loop;

    EXCEPTION
      WHEN OTHERS THEN      
          prm_ErrorMsg:=SQLERRM;
          prm_AppCode:='-1';           
          PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END; 
   
 /*-------------------------------------------------------------------------
   || 过程名称 ：自动页面逻辑生成
   || 功能描述 ：自动页面逻辑生成
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Auto_pageLogic(     
     prm_subPageCode     In  VARCHAR2, --子页面编码
     prm_fundesc         IN  VARCHAR2, --子页面名称
     prm_Tables          In  VARCHAR2, --传入的物理表
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )IS 
    I_Count            Integer;
    V_Temp             VARCHAR2(4000);
    V_Temp2            VARCHAR2(4000);
    V_AllTables        VARCHAR2(200);
    v_KeyCol           VARCHAR2(200);
    V_PrimaryKey       VARCHAR2(4000);
    V_ColumnNames      VARCHAR2(4000);
    V_RelateCon        VARCHAR2(1000);
    V_SelectSql        VARCHAR2(4000);
    V_CountSql         VARCHAR2(4000);   
    V_TreeSql          VARCHAR2(4000); 
    V_InsertSql        VARCHAR2(4000);
    V_DeleteSql        VARCHAR2(4000);
    V_UpdateSql        VARCHAR2(4000);
    T_tables      dbms_sql.Varchar2_Table;
    r_page   p_d_page%rowType;
  BEGIN
     prm_AppCode:=def_OK;  
    
    
     
     if NVL(prm_Tables,'0')='0' then
        prm_ErrorMsg:='请输入要自动化生成页面逻辑的物理表!';
        prm_AppCode:='-1';     
        return ;
     end if;
     SELECT COUNT(1)
     INTO   I_COUNT  
     FROM p_d_queryfield
     WHERE subpagecode=prm_subPageCode;
     IF I_COUNT>0 THEN
        prm_ErrorMsg:='系统中有子页面的配置信息，不能使用此功能重新初始化!';
        prm_AppCode:='-1';     
        return ;
     END IF;
    select *
      into r_page
      from p_d_page p
      where exists(select 1 
        from P_D_QUERYMAIN  m 
        where   m.pagecode=p.pagecode and
            m.subpagecode=prm_subPageCode);
     ----------分割字符串-----------------
     select regexp_substr(Upper(prm_Tables),'[^|]+',1,rownum)
     BULK COLLECT INTO T_tables --实现数据装载
     from dual
     connect by rownum <= length(Upper(prm_Tables)) -length(replace(Upper(prm_Tables),'|')) + 1;
     
     -------------------验证输入表是否正确-----------------------
     V_AllTables:='';
     for i in 1.. T_tables.count loop
        SELECT COUNT(1)
        INTO I_Count
        FROM TAB 
        WHERE TNAME=T_tables(i);
        If ( I_Count=0 ) then
           prm_ErrorMsg:='输入表名不正确，请核实物理表名!';
           prm_AppCode:='-1';     
           return ;
        end if;
        if (i=1) then
           V_AllTables:=T_tables(i)||' '||chr(64+ i);
        else
           V_AllTables:=V_AllTables||','||T_tables(i)||' '||chr(64+ i);
        end if;     
     end loop;
     ---------------------得到主表的列信息-------------------------------
      SELECT  replace(wm_concat ('a.'||a.COLUMN_NAME||  ' as '||a.COLUMN_NAME),',',','||chr(10)) new_result
      INTO V_ColumnNames
        FROM  (select TABLE_NAME,COLUMN_NAME
             FROM  cols
             WHERE TABLE_NAME=T_tables(1)
             ORDER BY  COLUMN_ID
             ) a     
      group by   a.TABLE_NAME;
      --------------------得到关联条件--------------------------------
      V_RelateCon:=' WHERE 1=1 ';     
      BEGIN
        select t.COLUMN_NAME
        into V_PrimaryKey
        from user_ind_columns t, user_indexes i
        where t.index_name = i.index_name AND
             t.table_name = i.table_name and
             t.table_name =T_tables(1) and
             i.uniqueness='UNIQUE' and 
             rownum =1;  
             v_KeyCol:=V_PrimaryKey;
             V_PrimaryKey:='A.'||V_PrimaryKey;       
      EXCEPTION
          WHEN NO_DATA_Found THEN
              V_PrimaryKey:='A.主键';  
              v_KeyCol:='主键';           
      END ;
      ----------拼接其他的键
      for i in 2.. T_tables.count loop
          V_RelateCon:=V_RelateCon||chr(10)||' AND '||V_PrimaryKey||'='||chr(64+ i)||'.'||V_PrimaryKey;
      end loop;
  
      V_SelectSql:='SELECT '||chr(10)|| V_ColumnNames    ;
      V_SelectSql:=V_SelectSql||chr(10)||' FROM '||V_AllTables;
      V_SelectSql:=V_SelectSql||chr(10)||V_RelateCon;
      V_CountSql:='SELECT '||chr(10)|| ' count(1)'    ;
      V_CountSql:=V_CountSql||chr(10)||' FROM '||V_AllTables;
      V_CountSql:=V_CountSql||chr(10)||V_RelateCon;

      V_TreeSql:='例子参考 select pagecode as id, pagename as text,'||'''00'''||' as parentcode,'||'''0'''||' as leaf ';
      V_TreeSql:=V_TreeSql||chr(10)||' from p_d_page ';
      V_TreeSql:=V_TreeSql||chr(10)||' where 1=1 ';
      V_TreeSql:=V_TreeSql||chr(10)||' order by ordernumber ';
   
      --------------------更新子页面信息--------------------------------------
      update p_d_querymain
       set  querysql =lower( V_SelectSql),
            countsql =lower(V_CountSql),
            TREEQUERY =lower(V_TreeSql),
            QUERYDESC=prm_fundesc,
            FUNCTIONDESC=prm_fundesc            
     where subpagecode = prm_subPageCode;     
     -------------------更新字段显示信息------------------------------------
     insert into p_d_queryfield
     (subpagecode,         fieldname,          fielddisp,            fieldtype,           fieldlength, 
      fieldprecision,      codename,           dispwidth,            sortable,            multi, 
      editflag,            visibleflag,        fieldorder,           fixed,               menudisabled, 
      tooltip,             renderer,           disptype,             align,               allowblank, 
      regex,               regextext,          maskre,               defaulttype,         codeflag, 
      defaultvalue)
     select 
      prm_subPageCode,     lower(c.COLUMN_NAME), nvl(substr(cc.comments,1,20),c.COLUMN_NAME),c.DATA_TYPE,      c.DATA_LENGTH, 
      c.DATA_PRECISION,    null,               decode(greatest(c.DATA_LENGTH*3,80),80,80,c.DATA_LENGTH*3),      '1', decode(greatest(c.DATA_LENGTH,50),50,1,3), 
      '1',                 '1',                rownum*10,            '1',                 '1', 
      null,                null,                decode(c.DATA_TYPE,'NUMBER','int','DATE','date','string'), 'left', '1', 
      null,                null,                null,               '0',                  '0', 
      null
      from user_tab_columns c,user_col_comments cc
      where c.TABLE_NAME=cc.table_name and
            c.COLUMN_NAME=cc.column_name and
            c.TABLE_NAME=T_tables(1)
      order by c.COLUMN_ID;
      ------------------数据逻辑处理----------------------------------
      ----------------形成新增语句列信息------------------------------------ 
      SELECT  replace(wm_concat (a.COLUMN_NAME),',',','||chr(10)) new_result
      INTO v_temp
        FROM  (select TABLE_NAME,COLUMN_NAME
             FROM  cols
             WHERE TABLE_NAME=T_tables(1)
             ORDER BY  COLUMN_ID
             ) a 
      group by   a.TABLE_NAME;
        ----------------形成新增语句值信息------------------------------------ 
      SELECT  replace(wm_concat ('#'||a.COLUMN_NAME||'#'),',',','||chr(10)) new_result
      INTO v_temp2
      FROM  (select TABLE_NAME,COLUMN_NAME
             FROM  cols
             WHERE TABLE_NAME=T_tables(1)
             ORDER BY  COLUMN_ID
             ) a 
      group by   a.TABLE_NAME;
      V_InsertSql:='INSERT INTO  '|| T_tables(1);
      V_InsertSql:=V_InsertSql||chr(10)||'('||v_temp||')';
      V_InsertSql:=V_InsertSql||chr(10)||' VALUES ';
      V_InsertSql:=V_InsertSql||chr(10)||'('||v_temp2||')';
      V_InsertSql:=lower(V_InsertSql);
      insert into p_d_querymodify
      (subpagecode,           modifyid,              modifytype,               modifysql,         clienttype, 
       procparam,             ordernumber,           keyfield,                 keycode)
      values
      (prm_subPageCode,       'I'||prm_subPageCode,  'I',                      V_InsertSql,        'I', 
       null,                  10,                     null,                    null);
      ------------------形成更新语句---------------------------------
        ----------------形成更新语句更新字段与更新值信息----------------------------------- 
      SELECT  replace(wm_concat (a.COLUMN_NAME||'='||'#'||a.COLUMN_NAME||'#'),',',','||chr(10)) new_result
      INTO v_temp
      FROM  (select TABLE_NAME,COLUMN_NAME
             FROM  cols
             WHERE TABLE_NAME=T_tables(1)
             ORDER BY  COLUMN_ID
             ) a   
      group by   a.TABLE_NAME;
      V_UpdateSql:='UPDATE '|| T_tables(1);
      V_UpdateSql:=V_UpdateSql||chr(10)||' SET '|| v_temp;
      V_UpdateSql:=V_UpdateSql||chr(10)||' WHERE '||v_KeyCol||'=#'||v_KeyCol||'#';
      V_UpdateSql:=lower(V_UpdateSql);
       insert into p_d_querymodify
      (subpagecode,           modifyid,              modifytype,               modifysql,         clienttype, 
       procparam,             ordernumber,           keyfield,                 keycode)
      values
      (prm_subPageCode,       'U'||prm_subPageCode,  'U',                      V_UpdateSql,        'U', 
       null,                  10,                     null,                    null);
      ----------------形成删除语句-----------------------------------------
      V_DeleteSql:='DELETE FROM '||T_tables(1);
      V_DeleteSql:=V_DeleteSql||chr(10)||' WHERE '||v_KeyCol||'=#'||v_KeyCol||'#';
      V_DeleteSql:=lower(V_DeleteSql);
      insert into p_d_querymodify
      (subpagecode,           modifyid,              modifytype,               modifysql,         clienttype, 
       procparam,             ordernumber,           keyfield,                 keycode)
      values
      (prm_subPageCode,       'D'||prm_subPageCode,  'D',                      V_DeleteSql,        'D', 
       null,                  10,                     null,                    null);       
   EXCEPTION
     WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END; 
    
end PKG_D_PAGE;
/

prompt
prompt Creating package body PKG_D_QUERY
prompt =================================
prompt
create or replace package body dev.PKG_D_Query is

 /*-------------------------------------------------------------------------
   || 过程名称 ：新增页面
   || 功能描述 ：新增页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Query_Add(     
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    BEGIN
     prm_AppCode:=def_OK;   
     ------------------调用存储过程生成初始化子页面信息---------------------
     Prc_subQuery_Auto(prm_PageCode,prm_AppCode,prm_ErrorMsg);
     if(prm_AppCode<>def_OK) then
        return;
     end if;
   EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);    
  end;    
  /*-------------------------------------------------------------------------
   || 过程名称 ：修改页面
   || 功能描述 ：修改页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||先调用存储过程，再进行修改
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Query_Modidfy(    
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_layout          IN  VARCHAR2, --页面布局    
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    i_count  Integer;
    r_page   p_d_q_page%rowType;
    BEGIN
     prm_AppCode:=def_OK;
      ---查找页面信息------------------
      select *
      into r_page
      from p_d_q_page
     where pagecode = prm_PageCode;     
      if r_page.pagecode<> prm_PageCode then
        prm_ErrorMsg:='页面编码是一个全局信息，新增后不能更改!';
        prm_AppCode:='-1';     
        return;
     end if;
     if r_page.pagelayout<> prm_layout then
        prm_ErrorMsg:='页面布局不能初始化后，不能被修改，如果真要修改，请先删除页面!';
        prm_AppCode:='-1';     
        return;
     end if;
     -----------查找有无初始化信息，没有自动生成----------------------
     SELECT Count(1)
     into i_count
     from p_d_q_model
     where pagecode=prm_PageCode;
     If (i_count>0 ) then
        return;
     end if;
      ------------------调用存储过程生成初始化子页面信息---------------------
     Prc_subQuery_Auto(prm_PageCode,prm_AppCode,prm_ErrorMsg);
     if(prm_AppCode<>def_OK) then
        return;
     end if;
    EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END;  
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除页面
   || 功能描述 ：删除页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Query_Delete(     
     prm_PageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    r_page   p_d_q_page%rowType;
    BEGIN
     prm_AppCode:=def_OK;
    
      select *
      into r_page
      from p_d_q_page
      where pagecode = prm_PageCode;
   
     --------------------删除字段信息----------------------------------------------
     delete P_D_Q_Field f
     where exists(select 1 
                  from P_D_Q_Model m
                  where  f.subpagecode=m.subpagecode and
                         m.pagecode=prm_PageCode);
   
    ---------------删除子页面配置信息---------------------------------------------------
    DELETE FROM P_D_Q_Model
    where pagecode=prm_PageCode;
    EXCEPTION
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END;   
   /*-------------------------------------------------------------------------
   || 过程名称 ：删除子页页面
   || 功能描述 ：删除子页页面
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_SubQuery_Delete(     
     prm_SubPageCode         In  VARCHAR2, --页面编码 
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg           OUT VARCHAR2
     )  is   
    BEGIN   
     --------------------删除字段信息----------------------------------------------
     prm_AppCode:=def_OK;
     delete P_D_Q_Field f
     where subpagecode=prm_SubPageCode;
  Exception 
    WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
   End ;   
   /*-------------------------------------------------------------------------
   || 过程名称 ：自动生成子页面信息
   || 功能描述 ：自动生成子页面信息
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_subQuery_Auto(     
     prm_PageCode        In  VARCHAR2, --页面编码
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )is
    i_PageCount  integer;
    v_mainQueryFlag varchar2(1);
    r_page   p_d_q_page%rowType;
    T_tables      dbms_sql.Varchar2_Table;
    BEGIN
     prm_AppCode:=def_OK;
      ---查找页面信息------------------
      select *
      into r_page
      from p_d_q_page
      where pagecode = prm_PageCode;
      -----------------生成子页面初始化信息----------------------
      if not( r_page.pagelayout='10' or r_page.pagelayout='22' or r_page.pagelayout='32') then
          prm_AppCode:='-1';
          prm_ErrorMsg:='请选择表格内容的风格!';
          return ;
      end if;
      i_PageCount:=to_number(substr(r_page.pagelayout,1,1));
      if i_PageCount=1 Then
         T_tables(1):=prm_PageCode||'Cennter';
      end if;
      if i_PageCount=2 Then        
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
      end if;
      if i_PageCount=3 Then
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
         T_tables(3):=prm_PageCode||'Botton';
      end if;
      if i_PageCount=4 Then
         T_tables(1):=prm_PageCode||'West';
         T_tables(2):=prm_PageCode||'Cennter';
         T_tables(3):=prm_PageCode||'Botton';
         T_tables(4):=prm_PageCode||'East';
      end if;     
      for i in 1.. T_tables.count loop
         if( i=1) then
           v_mainQueryFlag:='1';
         else
           v_mainQueryFlag:='0';
         end if;
         insert into p_d_q_model
           (subpagecode,                pagecode,               functiondesc,             querydesc,          querysql, 
            expanderflag,               expandcol,              dateformat,               countsql,           ordernumber,
            pageflag,                   checkflag,              printtemplate,            outqueryfield,      innerkeyfield,
            pagewidth,                  pageheight,             mainQueryFlag)
         values
           (T_tables(i),                prm_PageCode,           null,                      null,              null, 
            '0',                        null,                   'yyyy-mm-dd hh:mm:ss',     null,              i*10,
            '1',                        '1',                    null,                      null,             null,
            600,                        400,                    v_mainQueryFlag);         
      end loop;

    EXCEPTION
      WHEN OTHERS THEN      
          prm_ErrorMsg:=SQLERRM;
          prm_AppCode:='-1';           
          PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END; 
   
 /*-------------------------------------------------------------------------
   || 过程名称 ：自动页面逻辑生成
   || 功能描述 ：自动页面逻辑生成
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_subQuery_AutoLogic(     
     prm_subPageCode     In  VARCHAR2, --子页面编码
     prm_fundesc         IN  VARCHAR2, --子页面名称
     prm_Tables          In  VARCHAR2, --传入的物理表
     prm_AppCode         OUT VARCHAR2,
     prm_ErrorMsg        OUT VARCHAR2
    )IS 
    I_Count            Integer;
    V_AllTables        VARCHAR2(200);
    V_PrimaryKey       VARCHAR2(4000);
    V_ColumnNames      VARCHAR2(4000);
    V_RelateCon        VARCHAR2(1000);
    V_SelectSql        VARCHAR2(4000);
    V_CountSql         VARCHAR2(4000);   
    T_tables      dbms_sql.Varchar2_Table;
    r_page   P_D_Q_Page%rowType;
  BEGIN
     prm_AppCode:=def_OK;  
     if NVL(prm_Tables,'0')='0' then
        prm_ErrorMsg:='请输入要自动化生成页面逻辑的物理表!';
        prm_AppCode:='-1';     
        return ;
     end if;
     SELECT COUNT(1)
     INTO   I_COUNT  
     FROM P_D_Q_Field
     WHERE subpagecode=prm_subPageCode;
     IF I_COUNT>0 THEN
        prm_ErrorMsg:='系统中有子页面的配置信息，不能使用此功能重新初始化!';
        prm_AppCode:='-1';     
        return ;
     END IF;
    select *
      into r_page
      from P_D_Q_Page p
      where exists(select 1 
        from P_D_Q_Model  m 
        where   m.pagecode=p.pagecode and
            m.subpagecode=prm_subPageCode);
     ----------分割字符串-----------------
     select regexp_substr(Upper(prm_Tables),'[^|]+',1,rownum)
     BULK COLLECT INTO T_tables --实现数据装载
     from dual
     connect by rownum <= length(Upper(prm_Tables)) -length(replace(Upper(prm_Tables),'|')) + 1;
     
     -------------------验证输入表是否正确-----------------------
     V_AllTables:='';
     for i in 1.. T_tables.count loop
        SELECT COUNT(1)
        INTO I_Count
        FROM TAB 
        WHERE TNAME=T_tables(i);
        If ( I_Count=0 ) then
           prm_ErrorMsg:='输入表名不正确，请核实物理表名!';
           prm_AppCode:='-1';     
           return ;
        end if;
        if (i=1) then
           V_AllTables:=T_tables(i)||' '||chr(64+ i);
        else
           V_AllTables:=V_AllTables||','||T_tables(i)||' '||chr(64+ i);
        end if;     
     end loop;
     ---------------------得到主表的列信息-------------------------------
      SELECT  replace(wm_concat ('a.'||a.COLUMN_NAME||  ' as '||a.COLUMN_NAME),',',','||chr(10)) new_result
      INTO V_ColumnNames
        FROM  (select TABLE_NAME,COLUMN_NAME
             FROM  cols
             WHERE TABLE_NAME=T_tables(1)
             ORDER BY  COLUMN_ID
             ) a     
      group by   a.TABLE_NAME;
      --------------------得到关联条件--------------------------------
      V_RelateCon:=' WHERE 1=1 ';     
      BEGIN
        select t.COLUMN_NAME
        into V_PrimaryKey
        from user_ind_columns t, user_indexes i
        where t.index_name = i.index_name AND
             t.table_name = i.table_name and
             t.table_name =T_tables(1) and
             i.uniqueness='UNIQUE' and 
             rownum =1;  
             V_PrimaryKey:='A.'||V_PrimaryKey;       
      EXCEPTION
          WHEN NO_DATA_Found THEN
              V_PrimaryKey:='A.主键';  
     END ;
      ----------拼接其他的键
      for i in 2.. T_tables.count loop
          V_RelateCon:=V_RelateCon||chr(10)||' AND '||V_PrimaryKey||'='||chr(64+ i)||'.'||V_PrimaryKey;
      end loop;
  
      V_SelectSql:='SELECT '||chr(10)|| V_ColumnNames    ;
      V_SelectSql:=V_SelectSql||chr(10)||' FROM '||V_AllTables;
      V_SelectSql:=V_SelectSql||chr(10)||V_RelateCon;
      V_CountSql:='SELECT '||chr(10)|| ' count(1)'    ;
      V_CountSql:=V_CountSql||chr(10)||' FROM '||V_AllTables;
      V_CountSql:=V_CountSql||chr(10)||V_RelateCon;   
   
      --------------------更新子页面信息--------------------------------------
      update P_D_Q_Model
       set  querysql =lower( V_SelectSql),
            countsql =lower(V_CountSql),
            QUERYDESC=prm_fundesc,
            FUNCTIONDESC=prm_fundesc            
     where subpagecode = prm_subPageCode;     
     -------------------更新字段显示信息------------------------------------
     insert into p_d_q_field
       (fieldname,         subpagecode,       fielddisp,             fieldtype,           fieldlength, 
        fieldprecision,    dispwidth,         sortable,              visibleflag,         fieldorder, 
        fixed,             menudisabled,      tooltip,               renderer,            disptype, 
        align,             codeflag,          codename,              condtionflag,        conditonorder, 
        dateflag)
     select
        lower(c.COLUMN_NAME), prm_subPageCode, nvl(substr(cc.comments,1,20),c.COLUMN_NAME), c.DATA_TYPE,     c.DATA_LENGTH, 
        c.DATA_PRECISION,    decode(greatest(c.DATA_LENGTH*3,80),80,80,c.DATA_LENGTH*3),  '1', '1',c.COLUMN_ID, 
        '1',                 '1',              '1',                  null,                 decode(c.DATA_TYPE,'NUMBER','int','DATE','date','string'), 
        '0',                 '0',              null,                 '0',                  10, 
        '0'
     from user_tab_columns c,user_col_comments cc
      where c.TABLE_NAME=cc.table_name and
            c.COLUMN_NAME=cc.column_name and
            c.TABLE_NAME=T_tables(1)
      order by c.COLUMN_ID;       
     
   EXCEPTION
     WHEN OTHERS THEN      
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:='-1';           
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);        
    END; 
    
end PKG_D_Query;
/

prompt
prompt Creating package body PKG_P_RUN
prompt ===============================
prompt
CREATE OR REPLACE PACKAGE BODY DEV.PKG_P_RUN AS
    /*-------------------------------------------------------------------------
   || 过程名称 ：写入日志
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
   PROCEDURE Prc_Save_Log(
     prm_State          IN  VARCHAR2, --状态信息
     prm_Desc           IN  VARCHAR2 --描述信息
     )
   IS  
   -----------------------声明日志是自主事物---------------------------------------
   PRAGMA AUTONOMOUS_TRANSACTION;
   prm_AppCode         VARCHAR2(1);      --执行代码
   prm_ErrorMsg        VARCHAR2(2000);  
   r_OperLog p_l_operlog%rowtype;
   BEGIN       
       prm_AppCode:=def_OK;
       PKG_P_Utilty.Prc_get_Code('LOGID',r_OperLog.Onlycode,prm_AppCode,prm_ErrorMsg);--日志唯一号
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       r_OperLog.Operdatetime:=sysdate;--操作时间
       r_OperLog.Runstate:=prm_State;  --操作状态
       r_OperLog.Statedesc:=prm_Desc; --日志描述信息
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.IPAddr,r_OperLog.Ipaddr,prm_AppCode,prm_ErrorMsg) ;--操作IP
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.ProgramCode,r_OperLog.Programcode,prm_AppCode,prm_ErrorMsg);--操作程序模块
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.ProgramName,r_OperLog.ProgramName,prm_AppCode,prm_ErrorMsg);--操作程序名称
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.OperCode,r_OperLog.Opercode,prm_AppCode,prm_ErrorMsg); --操作员代码
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.FunctionCode,r_OperLog.FunctionCode,prm_AppCode,prm_ErrorMsg);--操作功能模块编码
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       PKG_P_UTILTY.Prc_Get_SessionENV(PKG_P_RUN.FunctionName,r_OperLog.FunctionName,prm_AppCode,prm_ErrorMsg);--操作功能模块名称
       IF prm_AppCode <> def_OK THEN
           RETURN;
       END IF;
       insert into p_l_operlog
      (onlycode,                 ipaddr,               programcode,              programname, 
       operdatetime,             runstate,             statedesc,                functioncode, 
       functionname,             opercode)
      values
      (r_OperLog.onlycode,       r_OperLog.ipaddr,     r_OperLog.programcode,    r_OperLog.programname, 
       r_OperLog.operdatetime,   r_OperLog.runstate,   r_OperLog.statedesc,      r_OperLog.functioncode, 
       r_OperLog.functionname,   r_OperLog.opercode);
       commit;
   EXCEPTION       
     WHEN OTHERS THEN     
        Rollback;         
   END;
END PKG_P_RUN;
/

prompt
prompt Creating package body PKG_P_UTILTY
prompt ==================================
prompt
CREATE OR REPLACE PACKAGE BODY DEV.PKG_P_Utilty AS
  /*-------------------------------------------------------------------------
   || 过程名称 ：加密
   || 功能描述 ：对字符串进行加密
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
PROCEDURE Prc_Encrypt_DES
 (prm_InputStr       IN VARCHAR2,     --输入字符串
  prm_EncryptStr     OUT VARCHAR2,    --输出字符串
  prm_AppCode        OUT VARCHAR2,      --执行代码
  prm_ErrorMsg       OUT VARCHAR2
 )
IS
  v_in varchar2(255);
  i_key varchar2(128);
  raw_input RAW(128) ;
  key_input RAW(128) ;
  decrypted_raw RAW(2048);
begin 
  prm_AppCode:=def_OK;
  --key，至少要8位
  i_key:= '&123456789';
  --要加密信息
  v_in := rpad(prm_InputStr,(trunc(length(prm_InputStr)/8)+1)*8,chr(0));
  --字符转成RAW   
  raw_input := UTL_RAW.CAST_TO_RAW(v_in);
  key_input := UTL_RAW.CAST_TO_RAW(i_key);
  dbms_obfuscation_toolkit.DESEncrypt(input => raw_input,key => key_input,encrypted_data => decrypted_raw);

--加密信息转成字符
  prm_EncryptStr:=rawtohex(decrypted_raw);
  prm_AppCode:=0;
  EXCEPTION       
     WHEN OTHERS THEN              
        prm_ErrorMsg:=SQLERRM;
        prm_AppCode:=-1;       
END;
 
/*-------------------------------------------------------------------------
   || 过程名称 ：解密
   || 功能描述 ：根据算法对数据进行解密
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/   
PROCEDURE Prc_DECRYPT_DES
 (prm_InputStr       IN VARCHAR2,     --输入字符串
  prm_DecryptStr     OUT VARCHAR2,    --输出字符串
  prm_AppCode        OUT VARCHAR2,      --执行代码
  prm_ErrorMsg       OUT VARCHAR2
 ) is                        
   i_key varchar2(2000);                                      

begin
  prm_AppCode:=def_OK;
  --key，至少要8位
  i_key:= '&123456789';
  --要加密信息 
    dbms_obfuscation_toolkit.DESDECRYPT(input_string => UTL_RAW.CAST_TO_varchar2(prm_InputStr),key_string => i_key,
    decrypted_string=> prm_DecryptStr);
    prm_DecryptStr := rtrim(prm_DecryptStr,chr(0));
    prm_AppCode:=0;
  EXCEPTION       
     WHEN OTHERS THEN              
        prm_ErrorMsg:=SQLERRM;
        PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);
        prm_AppCode:=-1;    
end;

/*-------------------------------------------------------------------------
|| 过程名称 ：得到系统唯一号
|| 功能描述 ：通过编码规则得到系统唯一号
|| 参数描述 ：参数标识        名称                输入/输出  类型
||            -------------------------------------------------------------
||
|| 作    者 ：杨章衡       完成日期 ：2014-11-05
||-------------------------------------------------------------------------
|| 修改记录 ：
||-------------------------------------------------------------------------*/  
PROCEDURE Prc_get_Code(
      prm_Code               IN  VARCHAR2, --唯一号编码
       prm_OnlyOne            OUT VARCHAR2,
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 )
IS
  pragma AUTONOMOUS_TRANSACTION;
  r_code            P_B_OnlyCode%Rowtype;
  s_year            VARCHAR2(4);
  s_month           VARCHAR2(2);
  s_day             VARCHAR2(2);
  s_updateYear      VARCHAR2(4);
  s_updateMonth     VARCHAR2(2);
  s_updateDay       VARCHAR2(2);  
BEGIN
   prm_AppCode:=def_OK;
   BEGIN
       SELECT * 
       INTO r_code
       FROM P_B_OnlyCode
       WHERE codeid =prm_Code;
       IF SQL%NOTFOUND THEN
           prm_AppCode  := '-1';
           prm_ErrorMsg := '没有维护系统唯一号.';  
           RETURN;
       END IF; 
   EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  :='-1';
         prm_ErrorMsg := '查询唯一号出错:'||SQLERRM;       
   END;
   IF r_code.prefix IS NULL THEN
      r_code.prefix :=' ';
   END IF;
   IF r_code.suffix IS NULL THEN
      r_code.suffix :=' ';
   END IF;
   SELECT TO_CHAR(sysdate,'YY'),TO_CHAR(SYSDATE,'MM'),TO_CHAR(SYSDATE,'DD')
    INTO s_year,s_month,s_day
   FROM DUAL;
   SELECT TO_CHAR(r_code.updatedate,'YY'),TO_CHAR(r_code.updatedate,'MM'),TO_CHAR(r_code.updatedate,'DD')
    INTO s_updateYear,s_updateMonth,s_updateDay
   FROM DUAL;
   IF   r_code.frequency =0  THEN  --不按频率更新
       r_code.current_code := r_code.current_code + r_code.increase_length;
       prm_OnlyOne:=r_code.prefix ||LPAD(to_char(r_code.current_code),r_code.codelength,'0')|| r_code.suffix;
      
   ELSIF (r_code.frequency = 1) THEN   --按年更新
      IF (s_year =s_updateYear) THEN
          r_code.current_code := r_code.current_code + r_code.increase_length ;          
      ELSE
          r_code.current_code := r_code.begincode;           
      END IF;
      prm_OnlyOne:=r_code.prefix ||s_year||LPAD(to_char(r_code.current_code),r_code.codelength,'0')||r_code.suffix;
     
   ELSIF (r_code.frequency = 2  ) THEN --按月更新
      IF (s_month = s_updateMonth) THEN
         r_code.current_code := r_code.current_code + r_code.increase_length ;    
      ELSE
         r_code.current_code := r_code.begincode;     
      END IF;
      prm_OnlyOne:= r_code.prefix||s_year||s_month||LPAD(to_char(r_code.current_code),r_code.codelength,'0')|| r_code.suffix;
     
   ELSIF  (r_code.frequency = 3  ) THEN  --按日更新     
      IF (s_day = s_updateDay ) THEN
         r_code.current_code := r_code.current_code + r_code.increase_length ;    
      ELSE
         r_code.current_code := r_code.begincode;   
      END IF;  
      prm_OnlyOne:=r_code.prefix||s_year||s_month||s_day||LPAD(to_char(r_code.current_code),r_code.codelength,'0')||r_code.suffix; 
   ELSE
     prm_AppCode  := -1;
     prm_ErrorMsg := '维护的唯一号参数有错误.';
     PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);
     RETURN; 
   END IF;
   
   BEGIN
      UPDATE  P_B_OnlyCode
      SET current_code = r_code.current_code,
          updatedate = SYSDATE             
      WHERE codeid =prm_Code;
   EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  := -1;
         prm_ErrorMsg := '更新唯一号出错:'||SQLERRM;     
         PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
   END;
   prm_OnlyOne :=Trim(prm_OnlyOne);
    COMMIT;

END Prc_get_Code;

    /*-------------------------------------------------------------------------
   || 过程名称 ：得到参数值
   || 功能描述 ：通过编码规则得到系统唯一号
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_get_Param(
      prm_paramCode          IN  VARCHAR2, --参数编码
      prm_paramValue         OUT VARCHAR2, --参数值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 )
  is
  begin
     prm_AppCode:=def_OK;
     SELECT P.PARAMVALUE
     INTO prm_paramValue
     FROM p_b_param P
     WHERE P.PARAMCODE=prm_paramCode;
  EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  := '-1';
         prm_ErrorMsg := '更新唯一号出错:'||SQLERRM;    
         PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
  end;
 /*-------------------------------------------------------------------------
   || 过程名称 ：设置参数值
   || 功能描述 ：设置参数值
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Set_ParamValue(
      prm_paramCode          IN  VARCHAR2, --参数编码
      prm_paramValue         IN VARCHAR2, --参数值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 )
    IS
    BEGIN
        prm_AppCode:=def_OK;
        UPDATE p_b_param SET PARAMVALUE=prm_paramValue WHERE PARAMCODE=prm_paramCode;
     EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  := '-1';
         prm_ErrorMsg := '设置参数'||prm_paramCode||'出错:'||SQLERRM;    
         PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);    
    END;

 /*-------------------------------------------------------------------------
   || 过程名称 ：设置会话级环境变量
   || 功能描述 ：设置数据库连接会话级环境变量
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：USERENV
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Set_SessionENV(
      prm_Code               IN  VARCHAR2, --环境变量键
      prm_Value               IN  VARCHAR2, --环境变量键值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 )
   IS
   BEGIN
      prm_AppCode:=def_OK;
      dbms_session.set_context(Env_Domain,prm_Code, prm_Value);
      EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  := -1;
         prm_ErrorMsg := '更新唯一号出错:'||SQLERRM;    
         PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
     
   END;
        /*-------------------------------------------------------------------------
   || 过程名称 ：得到会话级环境变量
   || 功能描述 ：得到数据库连接会话级环境变量
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：USERENV
   ||-------------------------------------------------------------------------*/  
     PROCEDURE Prc_Get_SessionENV(
      prm_Code               IN  VARCHAR2, --环境变量键
      prm_Value              OUT  VARCHAR2, --环境变量键值
      prm_AppCode            OUT VARCHAR2,
      prm_ErrorMsg           OUT VARCHAR2 )
   IS
   BEGIN
       prm_AppCode:=def_OK;
       select sys_context(Env_Domain,prm_Code) 
       INTO prm_Value
       from dual;
       IF SQL%NOTFOUND THEN
         prm_Value:='';
         RETURN;
       END IF; 
    EXCEPTION
       WHEN OTHERS THEN
         prm_AppCode  := '-1';
         prm_ErrorMsg := '得到环境变量出错:'||SQLERRM;    
         PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);  
   END;   
   
  /*-------------------------------------------------------------------------
   || 过程名称 ：对索引进行处理
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Create_TableKey(    
    prm_TableName    IN  VARCHAR2,        --物理表名
    prm_KeyType      IN  VARCHAR2,         --Key类型 P为主键, U唯一索引 ,N不般索引
    prm_ColName      IN  VARCHAR2,         --列名
    prm_DataSouceID  IN  VARCHAR2,         --数据源标识
    prm_AppCode      OUT VARCHAR2,      --执行代码
    prm_ErrorMsg     OUT VARCHAR2
   )IS
    i_KeyCount          Integer;
    V_CreateIndexSql    VARCHAR2(2000);
   BEGIN
     prm_AppCode :=def_OK;
     --------------------查找是否建了主键
     select Count(1)
      into i_KeyCount
      from user_ind_columns t, user_indexes i
     where t.index_name = i.index_name AND
           t.table_name = i.table_name  and
           t.table_name = UPPER(prm_TableName) and
           t.COLUMN_NAME =UPPER(prm_ColName);
     ---------------建了主键不需要重要建立-----------------
     IF i_KeyCount >=1 THEN
        RETURN;
     END IF;
     ---------------建立主键----------------------------
     IF prm_KeyType ='P' THEN
        V_CreateIndexSql:='alter table '||prm_TableName||'   add constraint '||
                          'PK'||prm_TableName||' primary key ('||prm_ColName||')';
     -------------建唯一索引-------------
     ELSIF prm_KeyType='U' THEN
        V_CreateIndexSql:='create unique  index IDX_'||SubStr(prm_TableName,Length(prm_TableName)-4,5)||prm_ColName;
        If prm_DataSouceID is null Then     
           V_CreateIndexSql:=V_CreateIndexSql||'  on '||prm_TableName||'('||prm_ColName||')';
        else
          V_CreateIndexSql:=V_CreateIndexSql||'  on '||prm_TableName||'(SOURCEID,'||prm_ColName||')';
          dbms_output.put(V_CreateIndexSql);
        END If;
     ------------建普通索引-------------------------
     ELSE
        V_CreateIndexSql:='create index IDX_'||prm_ColName;
        If prm_DataSouceID is null Then     
           V_CreateIndexSql:=V_CreateIndexSql|| '  on '||prm_TableName||'('||prm_ColName||')';
        else
          V_CreateIndexSql:=V_CreateIndexSql||'  on '||prm_TableName||'(SOURCEID,'||prm_ColName||')';
        END If;
                   
     END IF;  
     execute immediate V_CreateIndexSql; 
   EXCEPTION       
      WHEN OTHERS THEN        
          prm_ErrorMsg:=SQLERRM;
          prm_AppCode:='-1';      
          PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);   
   END Prc_Create_TableKey;  
   
   /*-------------------------------------------------------------------------
   || 过程名称 ：分割字符串
   || 功能描述 ：
   || 参数描述 ：参数标识        名称                输入/输出  类型
   ||            -------------------------------------------------------------
   ||
   || 作    者 ：杨章衡       完成日期 ：2014-11-05
   ||-------------------------------------------------------------------------
   || 修改记录 ：
   ||-------------------------------------------------------------------------*/
  PROCEDURE Prc_Split_Str(    
   prm_str           in   varchar2,  --分割字符串
   prm_separator     in   varchar2, --分割符
   prm_CharTable     OUT  dbms_sql.Varchar2_Table,      --执行代码
   prm_AppCode       OUT VARCHAR2,      --执行代码
   prm_ErrorMsg      OUT VARCHAR2
   )IS
    v_temp    varchar2(4000);
    v_element varchar2(4000);
    T_TableChar dbms_sql.Varchar2_Table;
   BEGIN
      prm_AppCode :=def_OK;      
      v_temp := prm_str;
    --  T_TableChar := Type_TableChar();
     while instr(v_temp, prm_separator) > 0
      loop
          v_element := substr(v_temp,1,instr(v_temp, prm_separator)-1);
          v_temp := substr(v_temp, instr(v_temp,prm_separator)+ length(prm_separator) , length(v_temp));
        --  T_TableChar.extend(1);
          T_TableChar(T_TableChar.count):=v_element;
     end loop;
    --T_TableChar.extend(1);
    T_TableChar(T_TableChar.count):=v_element;
    prm_CharTable:=T_TableChar;
   EXCEPTION       
      WHEN OTHERS THEN        
          prm_ErrorMsg:=SQLERRM;
          prm_AppCode:='-1';      
          PKG_P_RUN.Prc_Save_Log('2',prm_ErrorMsg);   
   END Prc_Split_Str;  
   
   
  
END PKG_P_Utilty;
/

prompt
prompt Creating trigger G4_TRIGGER_EAJDBCMONITOR
prompt =========================================
prompt
create or replace trigger "DEV"."G4_TRIGGER_EAJDBCMONITOR"
before insert on EAJDBCMONITOR
for each row
begin
select G4_SEQ_EAJDBCMONITOR_MONITORID.nextval into :new.monitorid from dual;
end ;
/


spool off
